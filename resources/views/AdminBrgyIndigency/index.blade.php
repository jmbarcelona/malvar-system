@extends('Layout.app_admin')
@section('title', 'Barangay Indigency Request')
@section('css')
<style type="text/css">
	
</style>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row pt-3">
		<div class="col-lg-12 col-md-12 col-12">
			<div class="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
				<div class="mb-3 mb-md-0">
					<h1 class="mb-1 h2 fw-bold">Barangay Indigency Request</h1>
				</div>
				<div class="d-flex">
					<button class="btn btn-info btn-shadow" onclick="add_user_accounts();"><i class="fa-solid fa-plus"></i>&nbsp;Add Barangay Indigency Request</button>
				</div>
			</div>
		</div>
	</div>
	<form class="needs-validation" id="add_edit_form" action="{{ route('admin.barangay_indigency.store') }}" novalidate>
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Record Filter</h5>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse">
						<i class="fas fa-minus"></i>
						</button>
					</div>
				</div>
				
				<div class="card-body">
					<div class="row">
						<div class="position-relative mb-2 col-md-4">
							<label>Date From</label>
							<input type="date" name="filter1" id="filter1" class="form-control" placeholder="Search equipment here">
						</div>

						<div class="mb-2 col-md-4">
							<label>Date To</label>
							<input type="date" name="filter2" id="filter2" class="form-control" placeholder="Search equipment here">
						</div>
					</div>
					
				</div>
				<div class="card-footer text-right">
					<!-- <button type="button" class="btn btn-secondary" data-card-widget="collapse">
					<i class="fa fa-minus"></i> Minimize Filter
					</button> -->
					<button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
					<button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
				</div>
				
			</div>
			<div class="card">
				<div class="card-body">
					<div class="table-responsive">
					<table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;"></table>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" role="dialog" id="modal_add_edit">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title h4">
						Add Barangay Indigency Request
					</div>		

					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">

                      <div class="col-sm-6">
              <input type="hidden" name="id" id="id">
              <label class="required">CTC No.</label><br>
              <input type="number" name="ctc_no" id="ctc_no" placeholder="Enter ctc number" class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_ctc_no"></div>
            </div>

            <div class="col-sm-6">
              <label class="required">Firstname</label><br>
              <input type="text" name="first_name" id="first_name" placeholder="Enter firstname" class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_first_name"></div>
            </div>

            <div class="col-sm-6">
              <label class="required">Middlename</label><br>
              <input type="text" name="middle_name" id="middle_name" placeholder="Enter middlename" class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_middle_name"></div>
            </div>

            <div class="col-sm-6">
              <label class="required">Lastname</label><br>
              <input type="text" name="last_name" id="last_name" placeholder="Enter lastname" class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_last_name"></div>
            </div>

            <div class="col-sm-6">
              <label class="required">Age</label><br>
              <input type="number" name="age" id="age" placeholder="Enter age" class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_age"></div>
            </div>
            <div class="col-sm-6">
              <label class="required">Civil Status</label><br>
              <select name="civil_status" id="civil_status" placeholder="Enter contact number" class="mb-2 form-control" autocomplete="off">
                <option value="" selected="" disabled="">Select Civil Status</option>
                <option value="Single">Single</option>
                <option value="Married">Married</option>
                <option value="Divorced">Divorced</option>
                <option value="Widowed">Widowed</option>
                <option value="Divorced">Divorced</option>
                <option value="Separated">Separated</option>
              </select>
              <div class="invalid-feedback text-left" id="err_civil_status"></div>
            </div>

            

            <div class="col-sm-12">
              <label class="required">Address</label><br>
              <textarea name="address" id="address" placeholder="Enter address" class="mb-2 form-control" autocomplete="off"></textarea>
              <div class="invalid-feedback text-left" id="err_address"></div>
            </div>

            <div class="col-sm-12">
              <label class="required">Purpose</label><br>
              <textarea name="purpose" id="purpose" placeholder="Enter contact number" class="mb-2 form-control" autocomplete="off"></textarea>
              <div class="invalid-feedback text-left" id="err_purpose"></div>
            </div>

                    </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success px-5" id="btn_submit">Submit</button>
				</div>
			</div>
		</div>
	</div>






	

</form>
</div>
@endsection


@section('script')
<!-- Javascript Function-->
<script type="text/javascript">


	$('#refresh-filter').on('click', function(){
		$('#filter').val('');
		submit_filter();
	});

	function submit_filter(){
		show_user_accounts();
	}

	function edit_clearance(id){
		$('#modal_add_edit').modal('show');
		$('.modal-title').html('Edit Barangay Indigency Request');
		$.ajax({
            type:"get",
            url:"{{ route('admin.barangay_indigency.find') }}"+'/'+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
             if (response.status == true) {
                
							$('#id').val(response.data.id);
							$('#ctc_no').val(response.data.ctc_no);
							$('#first_name').val(response.data.first_name);
							$('#middle_name').val(response.data.middle_name);
							$('#last_name').val(response.data.last_name);
							$('#age').val(response.data.age);
							$('#civil_status').val(response.data.civil_status);
							$('#date').val(response.data.date);
							$('#address').val(response.data.address);
							$('#purpose').val(response.data.purpose);
                $('#modal_add_edit').modal('show');
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          });   
	}


	show_user_accounts();
	var tbl_user_accounts;
	function show_user_accounts(){
		if (tbl_user_accounts) {
			tbl_user_accounts.destroy();
		}
		tbl_user_accounts = $('#tbl_user_accounts').DataTable({
		pageLength: 10,
		responsive: true,
		deferRender: true,
		aaSorting: [],
		language: {
		"emptyTable": "No data available",
		searchPlaceholder: "Requestor name",

	},
	ajax: {
        url: "{{ route('admin.barangay_indigency.list') }}",
        data: {filter1 : $('#filter1').val(), filter2 : $('#filter2').val()},
    },
		columns: [{
		className: 'text-center',
		"data": "id",
		"title": "Print PDF",
		"orderable": false,
		"render": function(data, type, row, meta){
      	return newdata =	'<a href="{{ route('admin.download.indigency') }}/'+data+'" target="_blank" class="btn btn-sm btn-danger text-sm" style="width: 100px"><i class="fas fa-file-pdf"></i> Print</a>'

				
    }
	},{
		className: '',
		"data": "id",
		"title": "Requestor's Name",
		"orderable": false,
		"render": function(data, type, row, meta){
      newdata = row.first_name +' '+ row.last_name
		return newdata;
    }
	},{
		className: '',
		"data": "date.",
		"title": "Date",
		"orderable": false,
	},{
		className: 'text-center',
		"data": "status.",
		"title": "Status",
		"orderable": false,
		"render": function(data, type, row, meta){
			if (data == 4) {
			   newdata = '<span class="badge badge-sm badge-warning text-sm" style="width: 100px">Pending</span>'
			}else if(data == 3){
      		   newdata = '<span class="badge badge-sm badge-danger text-sm" style="width: 100px">Disapproved</span>'
			}else{
      		   newdata = '<span class="badge badge-sm badge-success text-sm" style="width: 100px">Completed</span>'
			}

		return newdata;
    }
	},{
		className: 'text-center',
		"data": "id",
		"title": "Options",
		"orderable": false,
		"render": function(data, type, row, meta){
				

				newdata = '<div class="btn-group">\
		    <button type="button" class="btn btn-sm btn-info">Settings</button>\
		    <button type="button" class="btn btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">\
		    <span class="sr-only">Toggle Dropdown</span>\
		    </button>\
		    <div class="dropdown-menu p-2" role="menu">\
		    <li><a class="btn bg-info btn-block btn-sm mb-1" onclick="approve('+data+')"><i class="fa-solid fa-thumbs-up"></i> Approve</a></li>\
		    <li><a class="btn bg-danger btn-block btn-sm mb-1" onclick="disapprove('+data+')"><i class="fa-solid fa-thumbs-down"></i> Disapprove</a></li>\
		    <li><a class="btn bg-warning btn-block btn-sm mb-1" onclick="edit_clearance('+data+')"><i class="fa-solid fa-pen-to-square"></i></i> Edit</a></li>\
		    </div>\
		    </div>'
		
			
			return newdata;
      	
    }
	}
	]
	});
	}

	function view(id){
		$.ajax({
            type:"get",
            url:"{{ route('admin.barangay_indigency.find') }}"+'/'+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
             if (response.status == true) {
             	$('#ctc_no1').val(response.data.ctc_no);
             	$('#first_name1').val(response.data.first_name);
             	$('#middle_name1').val(response.data.middle_name);
             	$('#last_name1').val(response.data.last_name);
             	$('#age1').val(response.data.age);
             	$('#civil_status1').val(response.data.civil_status);
             	$('#date1').val(response.data.date);
             	$('#address1').val(response.data.address);
             	$('#purpose1').val(response.data.purpose);
            	$('#modal_view').modal('show');
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          });   
	}

	function approve(id){
		Swal.fire({
			  title: 'Are you sure?',
			  text: "This request will be approve!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, approve it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	
			  	$.ajax({
		    type:"post",
		    url:"{{ route('admin.barangay_indigency.approve') }}"+"/"+id,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     	Swal.fire(
			      'Success!',
			      'This request has been approve.',
			      'success'
			    )
		       
		     }else{
		     	Swal.fire({
		                position: 'center',
		                icon: 'error',
		                title: response.message,
		                showConfirmButton: false,
		                timer: 4500
		              })
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    
			  }
			})
	}


	
	function disapprove(id){
		Swal.fire({
			  title: 'Are you sure?',
			  text: "This request will be disapprove!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, disapprove it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	
			  	$.ajax({
		    type:"post",
		    url:"{{ route('admin.barangay_indigency.disapprove') }}"+"/"+id,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     	Swal.fire(
			      'Success!',
			      'This request has been disapprove.',
			      'success'
			    )
		       
		     }else{
		     	Swal.fire({
		                position: 'center',
		                icon: 'error',
		                title: response.message,
		                showConfirmButton: false,
		                timer: 4500
		              })
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    
			  }
			})
	}
	


	function delete_s_equipment(id){
		Swal.fire({
			  title: 'Are you sure?',
			  text: "You won't be able to revert this!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$.ajax({
		    type:"delete",
		    url:"{{ route('admin.contact.destroy') }}"+'/'+id,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     
		       
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    Swal.fire(
			      'Deleted!',
			      'Your record has been deleted.',
			      'success'
			    )
			  }
			})

		
	}

	$("#add_edit_form").on('submit', function(e){
		var mydata = $(this).serialize();
		let url = $(this).attr('action');
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"get",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit').prop('disabled', true);
					$('#btn_submit').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					Swal.fire({
		                position: 'center',
		                icon: 'success',
		                title: 'Barangay Indigency Requested Successfully!',
		                showConfirmButton: false,
		                timer: 1500
		              })
					
					showValidator(response.error,'add_edit_form');
					show_user_accounts();
					$('#modal_add_edit').modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'add_edit_form');
				}
				$('#btn_submit').prop('disabled', false);
				$('#btn_submit').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_user_accounts(_this){
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this user_accounts?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"delete",
				url:"",
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					
					show_user_accounts();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function add_user_accounts(){
		$('#add_edit_form').trigger("reset");
		$('.modal-title').html('Add Barangay Indigency Request');
		$('#modal_add_edit').modal('show');
	}

	
</script>
@endsection
