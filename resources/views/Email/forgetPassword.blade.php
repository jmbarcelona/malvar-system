@component('mail::message')
Hi {{ $data['name'] ?? ''}}!

We are here to help you recover your account
<br>
Visit the link below to reset your password!

@component('mail::button', ['url' => $data['link'].'/'.$data['id'], 'color' => 'primary'])
Change Password
@endcomponent

Thanks {{ $data['system'] ?? 'Malvar System'}}<br>
@endcomponent