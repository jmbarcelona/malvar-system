@extends('Layout.app_user')
@section('title', 'Students Management')
@section('css')
<style type="text/css">
	.dataTables_filter { display: none; }
</style>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row pt-3">
		<div class="col-lg-12 col-md-12 col-12">
			<div class="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
				<div class="mb-3 mb-md-0">
					<h1 class="mb-1 h2 fw-bold">Cedula Request</h1>
				</div>
				<div class="d-flex">
					<button class="btn btn-info btn-shadow" onclick="add_user_accounts();"><i class="fa-solid fa-handshake"></i>&nbsp; Request Cedula</button>
				</div>
			</div>
		</div>
	</div>
	<form class="needs-validation" id="cedula_form" action="{{ route('user.cedula.store') }}" novalidate>
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Record Filter</h5>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse">
						<i class="fas fa-minus"></i>
						</button>
					</div>
				</div>
				
				<div class="card-body">
					<div class="row">
						<div class="position-relative mb-2 col-md-4">
							<label>Fullname</label>
							<input type="text" name="filter" id="filter" class="form-control" placeholder="Search name here">
							
						</div>
					</div>
					
				</div>
				<div class="card-footer text-right">
					<button type="button" class="btn btn-secondary" data-card-widget="collapse">
					<i class="fa fa-minus"></i> Minimize Filter
					</button>
					<button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
					<button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
				</div>
				
			</div>
		</div>
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
				<table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;"></table>
			</div>
		</div>
	</div>
	<div class="modal fade" role="dialog" id="modal_user_accounts" data-bs-backdrop="static">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form action="#" id="Registration_form" class="needs-validation">
					<div class="card">
						<div class="card-header h2">Cedula Request
							<button class="close float-sm-right" data-dismiss="modal" id="onclose_modal">&times;</button>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-sm-4">
									<label class="required">Lastname</label><br>
									<input type="text" name="last_name" id="last_name" placeholder="Enter lastname" class="mb-2 form-control" autocomplete="off">
									<div class="invalid-feedback" id="err_last_name"></div>
								</div>
								<div class="col-sm-4">
									<label class="required">Firstname</label><br>
									<input type="text" name="first_name" id="first_name" placeholder="Enter firstname" class="mb-2 form-control" autocomplete="off">
									<div class="invalid-feedback" id="err_first_name"></div>
								</div>
								<div class="col-sm-4">
									<label class="required">Middlename</label><br>
									<input type="text" name="middle_name" id="middle_name" placeholder="Enter middlename" class="mb-2 form-control" autocomplete="off">
									<div class="invalid-feedback" id="err_middle_name"></div>
								</div>
								<div class="col-sm-12">
									<label class="required">Address</label>
									<textarea class="form-control mb-2" rows="2" id="address" name="address" placeholder="Enter address here"></textarea>
									<div class="invalid-feedback" id="err_address"></div>
								</div>
								<div class="col-sm-6">
									<label class="required">Citizenship</label>
									<input type="text" name="citizenship" id="citizenship" placeholder="Enter Citizenship here" class="mb-2 form-control" autocomplete="off" value="Filipino">
									<div class="invalid-feedback" id="err_citizenship"></div>
								</div>
								<div class="col-sm-6">
									<label class="required">Date of birth</label>
									<input type="date" name="bday" id="bday" placeholder="Enter date of birth here" class="mb-2 form-control" autocomplete="off">
									<div class="invalid-feedback" id="err_bday"></div>
								</div>
								<div class="col-sm-12">
									<label class="required">Place of birth</label>
									<textarea class="form-control mb-2" rows="2" id="bplace" name="bplace" placeholder="Enter place of birth here"></textarea>
									<div class="invalid-feedback" id="err_bplace"></div>
								</div>
								
								<div class="col-sm-6">
									<label class="required">Gender</label>
									<select name="gender" id="gender" class="form-control" autocomplete="off">
										<option selected="" disabled="">Please select gender</option>
										<option value="Male">Male</option>
										<option value="Female">Female</option>
									</select>
									<div class="invalid-feedback" id="err_gender"></div>
								</div>
								<div class="col-sm-6">
									<label class="required">Civil Status</label>
									<select name="civil_status" id="civil_status" class="form-control mb-2" autocomplete="off">
										<option selected="" disabled="">Please select status</option>
										<option value="Single">Single</option>
										<option value="married">married</option>
										<option value="Divorced">Divorced</option>
										<option value="Separated">Separated</option>
										<option value="Widowed">Widowed</option>
									</select>
									<div class="invalid-feedback" id="err_civil_status"></div>
								</div>
								<div class="col-sm-6">
									<label class="required">Height</label>
									<input type="text" name="height" id="height" class="form-control mb-2" placeholder="Enter height here">
									<div class="invalid-feedback" id="hrr_Height"></div>
								</div>
								<div class="col-sm-6">
									<label class="required">Weight</label>
									<input type="text" name="weight" id="weight" class="form-control mb-2" placeholder="Enter weight here">
									<div class="invalid-feedback" id="wrr_Weight"></div>
								</div>
							</div>
						</div>
						<div class="card-footer text-right">
							<button type="button" class="btn btn-danger px-5" onclick="clear_button()">Clear</button>
							<button type="submit" class="btn btn-success px-5">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</form>
</div>
@endsection


@section('script')
<!-- Javascript Function-->
<script type="text/javascript">


	$('#refresh-filter').on('click', function(){
		$('#filter').val('');
		submit_filter();
	});

	function submit_filter(){
		show_user_accounts();
	}


	show_user_accounts();
	var tbl_user_accounts;
	function show_user_accounts(){
		if (tbl_user_accounts) {
			tbl_user_accounts.destroy();
		}
		tbl_user_accounts = $('#tbl_user_accounts').DataTable({
		pageLength: 10,
		responsive: true,
		deferRender: true,
		aaSorting: [],
		language: {
		"emptyTable": "No data available"
	},
	ajax: {
        url: "{{ route('user.cedula.list') }}",
        data: {filter : $('#filter').val()},
    },
		columns: [{
		className: '',
		"data": "id",
		"title": "Fullname",
		"orderable": false,
		"render": function(data, type, row, meta){
      return row.last_name+', '+row.first_name+', '+row.middle_name
    }
	},{
		className: '',
		"data": "address",
		"title": "Address",
		"orderable": false,
	},{
		className: '',
		"data": "bday",
		"title": "Birth Day",
		"orderable": false,

			
	},{
		className: '',
		"data": "civil_status",
		"title": "Civil Status",
		"orderable": false,
	},{
		className: '',
		"data": "bplace",
		"title": "Birth Place",
		"orderable": false,
	},{
		className: '',
		"data": "date_requested",
		"title": "Date",
		"orderable": false,
	},{
		className: 'text-center',
		"data": "status",
		"title": "Status",
		"orderable": false,
		"render": function(data, type, row, meta){
			if (data == 1) {
				return '<span class="badge p-2 badge-success"><strong>Done</strong></i></span>'
			}else{
	      		return '<span class="badge p-2 badge-warning"><strong></strong>Pending</i></span>'

			}
	    }
		
	},{
		className: 'text-center',
		"data": "id",
		"title": "Delete",
		"orderable": false,
		"render": function(data, type, row, meta){
      return '<button type="button" class="btn" onclick="delete_user('+data+')"><i class="fa-solid fa-trash text-danger"></i></button>'


    }
	}
	]
	});
	}


	function delete_user(id){
		Swal.fire({
			  title: 'Are you sure?',
			  text: "You won't be able to revert this!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$.ajax({
		    type:"delete",
		    url:"{{ route('user.cedula.destroy') }}"+'/'+id,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     
		       
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    Swal.fire(
			      'Deleted!',
			      'Your file has been deleted.',
			      'success'
			    )
			  }
			})

		
	}

	$("#cedula_form").on('submit', function(e){
		var mydata = $(this).serialize();
		let url = $(this).attr('action');
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"get",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit_user_accounts').prop('disabled', true);
					$('#btn_submit_user_accounts').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					Swal.fire({
		                position: 'center',
		                icon: 'success',
		                title: 'Cedula Requested Successfully!',
		                showConfirmButton: false,
		                timer: 1500
		              })
					
					showValidator(response.error,'cedula_form');
					show_user_accounts();
					$('#modal_user_accounts').modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'cedula_form');
				}
				$('#btn_submit_user_accounts').prop('disabled', false);
				$('#btn_submit_user_accounts').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_user_accounts(_this){
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this user_accounts?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"delete",
				url:"",
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					
					show_user_accounts();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function add_user_accounts(){
		$('#cedula_form').trigger("reset");
		$('#modal_user_accounts').modal('show');
	}

	
</script>
@endsection
