@extends('Layout.app_admin')
@section('title', 'Admin Dashboard')
@section('content')
<!-- Content Wrapper. Contains page content -->
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

              <div class="col-lg-4 col-12">
                <!-- small box -->
                <div class="small-box bg-info">
                  <div class="inner">
                    <h3>{{ BarangayEquipmentRent() }}<span class="badge badge-sm badge-danger text-md float-sm-right">New</span></h3>
                    <p>Barangay Equipment Request  &nbsp;(Today)</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-chair"></i>
                  </div>
                  <a href="{{ route('admin.request-barangay.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-4 col-12">
                <!-- small box -->
                <div class="small-box bg-danger">
                  <div class="inner">
                    <h3>{{ SportEquipmentRent() }} <span class="badge badge-sm badge-danger text-md float-sm-right">New</span></h3>
                    <p>Sports Equipment Request  &nbsp;(Today)</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-baseball-ball"></i>
                  </div>
                  <a href="{{ route('admin.request-sport.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              


              <div class="col-lg-4 col-12">
                <!-- small box -->
                <div class="small-box bg-warning">
                  <div class="inner">
                    <h3>{{ BarangayIndigency() }} <span class="badge badge-sm badge-danger text-md float-sm-right">New</span></h3>
                    <p>Barangay Indigency Request &nbsp;(Today)</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-baseball-ball"></i>
                  </div>
                  <a href="{{ route('admin.barangay_indigency.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>

              <div class="col-lg-3 col-12">
                <!-- small box -->
                <div class="small-box bg-success">
                  <div class="inner">
                    <h3>{{ BarangayClearance() }} <span class="badge badge-sm badge-danger text-md float-sm-right">New</span></h3>
                    <p>Barangay Clearance Request &nbsp;(Today)</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-baseball-ball"></i>
                  </div>
                  <a href="{{ route('admin.barangay_clearance.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-12">
                <!-- small box -->
                <div class="small-box bg-warning">
                  <div class="inner">
                    <h3>{{ totalAnnouncement() }}</h3>
                    <p>Announcement</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-file-alt"></i>
                  </div>
                  <a href="{{ route('admin.announcement.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-12">
                <!-- small box -->
                <div class="small-box bg-info">
                  <div class="inner">
                    <h3>{{ totalUser() }}</h3>
                    <p>Total users</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-baseball-ball"></i>
                  </div>
                  <a href="{{ route('admin.users.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <div class="col-lg-3 col-12">
                <!-- small box -->
                <div class="small-box" style="background-color: tomato">
                  <div class="inner">
                    <h3>{{ Gymcount() }}<span class="badge badge-sm badge-danger text-md float-sm-right">New</span></h3>
                    <p>Gymnasium Scheduling Request &nbsp;(Today)</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-baseball-ball"></i>
                  </div>
                  <a href="{{ route('admin.Gym_scheduling.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
            </div>
@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection