@extends('Layout.app_user')
@section('title', 'Barangay Announcement')
@section('css')
<style type="text/css">
.dataTables_filter { display: none; }
.card-body { font-size: 30px; }
.text-area{
font-size: 20px;
}
</style>
@endsection
@section('content')
<div class="content-header" style="border-bottom: solid lightgrey 1px">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Barangay Announcement</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('user.announcement.index') }}" class="text-dark">Barangay Announcement</a></li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        

     <div class="container-fluid mt-2">
       <div class="row justify-content-center">
         <div class="col-sm-12">
            <div class="col-sm-12">
            <div class="row">

          @foreach($announcement as $announcement)
            <div class="col-sm-12 col-12">
            <div class="card mt-3">
             <div class="card-header bg-info">{{ $announcement->a_name }}</div>
             <div class="card-body">
              <div class="col-sm-12">
                <div class="row">

              <div class="col-sm-4">
              @if($announcement->img)
                      <img src="{{ asset('storage'.'/'."$announcement->img") }}" class="img-thumbnail" style="width: 500px; height: 500px;">
                      @else
                      <img src="{{ asset('img/default.png') }}" class="img-thumbnail" style="width: 500px; height: 500px;">
              @endif
              </div>

              <div class="col-sm-8">
                <textarea class="form-control bg-light text-area" style="height: 100%; border: none" disabled>{{ $announcement->description }}</textarea>
              </div>
</div>
</div>
             </div>
             <div class="card-footer text-info"><i>{{date('F j, Y', strtotime($announcement->date))}}</i></div>
           </div>
           </div>
          @endforeach

           </div>
           </div>
         </div>
       </div>
     </div> 



@endsection


@section('script')
<!-- Javascript Function-->
<script type="text/javascript">

</script>
@endsection
