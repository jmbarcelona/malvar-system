@extends('Layout.app_user')
@section('title', 'Requested Barangay Clearance')
@section('content')
<!-- Content Wrapper. Contains page content -->
<!-- Content Header (Page header) -->
<div class="content-header" style="border-bottom: solid lightgrey 1px">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Requested Barangay Clearance</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Home</a></li>
            <li class="breadcrumb-item active">Requested Barangay Clearance</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>

        <div class="table-responsive col-sm-12">
          <div class="text-right mb-3 mt-3">
            <button class="btn btn-danger btn-sm col-sm-1" onclick="back_button()"> <strong><i class="fa-solid fa-caret-left"></i> &nbsp;Back</strong></button>
          </div>
          <table class="table table-bordered table-hove" style="width: 100%;">
            <thead>
              <tr>
                <th>Full details</th>
                <th>Name</th>
                <th>Date</th>
                <th class="text-center">Status</th>
                <th class="text-center">Remarks</th>
              </tr>
            </thead>
            <tbody>
              @foreach($clearance as $clearance)
              <tr>
                <td><button type="button" class="btn btn-info btn-sm px-4" onclick="view_details('{{ $clearance->id }}')"><i class="fa-solid fa-eye"></i> View</button></td>
                <td>{{ $clearance->first_name.' '.$clearance->last_name }}</td>
                <td>{{ date('F j, Y', strtotime($clearance->date)) }}</td>
                @if($clearance->status == 4)
                <td class="text-center"><span class="badge badge-warning p-2">Pending</span></td>
                @elseif($clearance->status == 3)
                <td class="text-center"><span class="badge badge-danger p-2">Disapproved</span></td>
                @else
                <td class="text-center"><span class="badge badge-success p-2">Completed</span></td>
                @endif

                @if($clearance->status == 4)
                <td class="text-center"><span class="badge badge-warning p-2">Requested</span></td>
                @elseif($clearance->status == 3)
                <td class="text-center"><span class="badge badge-danger p-2">Request Declined</span></td>
                @else
                <td class="text-center"><span class="badge badge-success p-2">Ready to Pickup</span></td>
                @endif
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>

        <div class="modal fade" role="dialog" id="modal_view">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title h4">
            Request Details
          </div>    

          <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row">
            <!-- <div class="col-sm-6">
              <input type="hidden" name="id" id="id">
              <label class="required">CTC No.</label><br>
              <input type="number" name="ctc_no" id="ctc_no" placeholder="Enter ctc number" class="mb-2 form-control bg-light" disabled autocomplete="off">
              <div class="invalid-feedback text-left" id="err_ctc_no"></div>
            </div> -->

            <div class="col-sm-12">
              <label class="required">Firstname</label><br>
              <input type="text" name="first_name" id="first_name" placeholder="Enter firstname" class="mb-2 form-control bg-light" disabled autocomplete="off">
              <div class="invalid-feedback text-left" id="err_first_name"></div>
            </div>

            <div class="col-sm-12">
              <label class="required">Middlename</label><br>
              <input type="text" name="middle_name" id="middle_name" placeholder="Enter middlename" class="mb-2 form-control bg-light" disabled autocomplete="off">
              <div class="invalid-feedback text-left" id="err_middle_name"></div>
            </div>

            <div class="col-sm-12">
              <label class="required">Lastname</label><br>
              <input type="text" name="last_name" id="last_name" placeholder="Enter lastname" class="mb-2 form-control bg-light" disabled autocomplete="off">
              <div class="invalid-feedback text-left" id="err_last_name"></div>
            </div>

            <div class="col-sm-6">
              <label class="required">Year Of Residency</label><br>
              <input type="text" name="years_of_residency" id="years_of_residency" class="mb-2 form-control bg-light" disabled autocomplete="off">
              <div class="invalid-feedback text-left" id="err_years_of_residency"></div>
            </div>
            <div class="col-sm-6">
              <label class="required">Civil Status</label><br>
              <select name="civil_status" id="civil_status" placeholder="Enter contact number" class="mb-2 form-control bg-light" disabled autocomplete="off">
                <option value="" selected="" disabled="">Select Civil Status</option>
                <option value="Single">Single</option>
                <option value="Married">Married</option>
                <option value="Divorced">Divorced</option>
                <option value="Widowed">Widowed</option>
                <option value="Divorced">Divorced</option>
                <option value="Separated">Separated</option>
              </select>
              <div class="invalid-feedback text-left" id="err_civil_status"></div>
            </div>

            <div class="col-sm-6">
              <label class="required">Request Date</label><br>
              <input type="text" name="date" id="date" placeholder="Enter date" class="mb-2 form-control bg-light" disabled autocomplete="off">
              <div class="invalid-feedback text-left" id="err_date"></div>
            </div>

            <div class="col-sm-6">
              <label class="required">Purok</label><br>
              <input type="text" name="purok" id="purok" placeholder="Enter purok" class="mb-2 form-control bg-light" disabled autocomplete="off">
              <div class="invalid-feedback text-left" id="err_purok"></div>
            </div>

            <div class="col-sm-12">
              <label class="required">Purpose</label><br>
              <textarea name="purpose" id="purpose" placeholder="Enter Purpose" class="mb-2 form-control bg-light" disabled autocomplete="off"></textarea>
              <div class="invalid-feedback text-left" id="err_purpose"></div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger px-5" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

       
@endsection
@section('script')
<script type="text/javascript">
function view_details(id){
  $.ajax({
            type:"get",
            url:"{{ route('user.barangay_clearance.find') }}"+'/'+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
             if (response.status == true) {
              $('#ctc_no').val(response.data.ctc_no);
              $('#first_name').val(response.data.first_name);
              $('#middle_name').val(response.data.middle_name);
              $('#last_name').val(response.data.last_name);
              $('#purok').val(response.data.address);
              $('#civil_status').val(response.data.civil_status);
              $('#date').val(response.data.date);
              $('#years_of_residency').val(response.data.resident_from);
              $('#purpose').val(response.data.purpose);
              $('#modal_view').modal('show');
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          }); 
}

function back_button(){
  window.history.go(-1);
}
</script>
@endsection

