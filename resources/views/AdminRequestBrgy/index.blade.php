@extends('Layout.app_admin')
@section('title', 'Barangay Equipments Request')
@section('css')
<style type="text/css">
	/*.dataTables_filter { display: none; }*/

	.balance-div{
  bottom: 0.25rem;
  position: fixed;
  right: 0.5rem;
  z-index: 1032;
  background-color: grey 0.1;
  border-radius: 5px
}


</style>
@endsection
@section('content')
<div class="container-fluid">

<div class="row pt-3">
	<div class="col-lg-12 col-md-12 col-12">
		<div class="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
			<div class="mb-3 mb-md-0">
				<h1 class="mb-1 h2 fw-bold">Barangay Equipment Request</h1>
			</div>
			<div class="d-flex">
				<!-- <button class="btn btn-success btn-shadow" onclick="stock_in();"><i class="fas fa-download"></i>&nbsp;Stock in</button>
				  <button class="btn btn-danger btn-shadow  mx-2" onclick="stock_out();"><i class="fas fa-upload"></i></i>&nbsp;Stock out</button> -->
				  <a href="{{ route('admin.request-barangay.add') }}">
				  <button class="btn btn-info btn-shadow"><i class="fa-solid fa-plus"></i>&nbsp;Add Barangay Equipment Request</button>
				  </a>
			</div>
		</div>
	</div>
</div>


<form class="needs-validation" id="spots_equipment_form" action="{{ route('admin.barangay.store') }}" novalidate>	
<div class="col-12">
            <div class="card">
              <div class="card-header" onclick="$('#minimize_filter').click()">
                <h5 class="card-title">Record Filter</h5>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
             
              <div class="card-body">

                  <div class="row">

                  		<div class="position-relative mb-2 col-md-4">
							<label>Status Filter</label>
							<select class="form-control" id="status_filter" name="status_filter">
								<option value="" selected="" disabled="">Select Status</option>
								<option value="4">Pending</option>
								<option value="1">Approved</option>
								<option value="2">Declined</option>
								<option value="3">Returned</option>
							</select>
						</div>
						
                        <div class="position-relative mb-2 col-md-4">
							<label>Date From</label>
							<input type="date" name="filter1" id="filter1" class="form-control">
						</div>

						<div class="position-relative mb-2 col-md-4">
							<label>Date To</label>
							<input type="date" name="filter2" id="filter2" class="form-control">
						</div>

						

                  </div>
      
              </div>

              <div class="card-footer text-right">
                <!-- <button type="button" class="btn btn-secondary" data-card-widget="collapse" id="minimize_filter">
                  <i class="fa fa-minus"></i> Minimize Filter
                </button> -->
                <button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
                <button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
              </div>
           
            </div>

          



<div class="card">
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;"></table>
		</div>
	</div>
</div>
</div>
<!-- 

<div class="card-footer text-right balance-div">
<button type="button" class="btn btn-info">
Main Report Ending Balance: <span class="badge badge-light ending-balance">₱24 pcs</span>
</button>
</div>

 -->



 <div class="modal fade" role="dialog" id="modal_add_edit">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title h4">
            Add barangay Equipment
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="row">
          <div class="col-sm-12">
          	<input type="hidden" name="id" id="id">
            <label class="required">Equipment Name</label><br>
            <input type="text" name="equipment_name" id="equipment_name" placeholder="Enter equipment name" class="mb-2 form-control" autocomplete="off">
            <div class="invalid-feedback text-left" id="err_equipment_name"></div>
          </div>
          <div class="col-sm-12">
            <label class="required">Quantity</label><br>
            <input type="number" name="quantity" id="quantity" placeholder="Enter quantity" class="mb-2 form-control" autocomplete="off">
            <div class="invalid-feedback text-left" id="err_quantity"></div>
          </div>
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-success px-5" id="btn_submit">Submit</button>
          </div>
        </div>
      </div>
    </div>
</form>
</div>


<!-- //stock in -->

 <div class="modal fade" role="dialog" id="stock_in_modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title h4">
            Stock in equipment
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
          	<form action="{{ route('admin.barangay.stockIn') }}" id="stock_in_form" class="needs-validation">
            <div class="row">
          <div class="col-sm-8">
            <label class="required">Equipment Name</label><br>
            <select id="selection_for_stock_in" name="selection_for_stock_in" class="form-control" onchange="selector_stock_in(this.value)">
            </select>
            <div class="invalid-feedback text-left" id="err_selection_for_stock_in"></div>

            <!-- <div class="invalid-feedback text-left" id="err_equipment_name"></div> -->
          </div>
          <div class="col-sm-4">
            <label class="required">Current Quantity</label><br>
            <input type="number" name="current_quantity" id="current_quantity" class="mb-2 form-control" autocomplete="off" disabled="" style="background-color: white">
          </div>

          <div class="col-sm-12">
            <label class="required">Add Quantity</label><br>
            <input type="number" name="add_for_stock_in" id="add_for_stock_in" placeholder="Enter quantity" class="mb-2 form-control" autocomplete="off">
            <div class="invalid-feedback text-left" id="err_add_for_stock_in"></div>
          </div>
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-success px-5" id="btn_submit3">Submit</button>
          </div>
        </div>
      </div>
    </div>
</form>
</div>


<!-- //stock out -->
<div class="modal fade" role="dialog" id="stock_out_modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title h4">
           Stock out equipment
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
          	<form action="{{ route('admin.barangay.stockOut') }}" id="stock_out_form" class="needs-validation">
            <div class="row">
          <div class="col-sm-8">
            <label class="required">Equipment Name</label><br>
            <select id="selection_for_stock_out" name="selection_for_stock_out" class="form-control" onchange="selector_stock_out(this.value)">
            </select>
            <div class="invalid-feedback text-left" id="err_selection_for_stock_in"></div>

            <!-- <div class="invalid-feedback text-left" id="err_equipment_name"></div> -->
          </div>
          <div class="col-sm-4">
            <label class="required">Current Quantity</label><br>
            <input type="number" name="current_quantity1" id="current_quantity1" class="mb-2 form-control" autocomplete="off" disabled="" style="background-color: white">
          </div>

          <div class="col-sm-12">
            <label class="required">Stock Out Quantity</label><br>
            <input type="number" name="minus_for_stock_in" id="minus_for_stock_in" placeholder="Enter quantity" class="mb-2 form-control" autocomplete="off">
            <div class="invalid-feedback text-left" id="err_add_for_stock_in"></div>
          </div>
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-success px-5" id="btn_submit3">Submit</button>
          </div>
        </div>
      </div>
    </div>
</form>
</div>

@endsection


@section('script')
<!-- Javascript Function-->
<script type="text/javascript">


$('#stock_in_form').on('submit', function(e){
	e.preventDefault();
	let formData = $(this).serialize();
	let url = $(this).attr('action');
	$.ajax({
			type:"post",
			url:url,
			data:formData,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit3').prop('disabled', true);
					$('#btn_submit3').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					Swal.fire({
		                position: 'center',
		                icon: 'success',
		                title: 'Equipment Added Successfully!',
		                showConfirmButton: false,
		                timer: 1500
		              })
					
					showValidator(response.error,'spots_equipment_form');
					show_user_accounts();
					$('#stock_in_modal').modal('hide');
					 $('#stock_in_form')[0].reset();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'spots_equipment_form');
				}
				$('#btn_submit3').prop('disabled', false);
				$('#btn_submit3').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});

});





	$('#refresh-filter').on('click', function(){
		$('#filter1').val('');
		$('#filter2').val('');
		submit_filter();
	});

	function submit_filter(){
		show_user_accounts();
	}


	show_user_accounts();
	var tbl_user_accounts;
	function show_user_accounts(){
		if (tbl_user_accounts) {
			tbl_user_accounts.destroy();
		}
		tbl_user_accounts = $('#tbl_user_accounts').DataTable({
		pageLength: 10,
		responsive: true,
		deferRender: true,
		aaSorting: [],
		language: {
		"emptyTable": "No data available",
		searchPlaceholder: "Search name here"
	},
	ajax: {
        url: "{{ route('admin.request-barangay.list') }}",
        data: {filter1 : $('#filter1').val(), filter2 : $('#filter2').val(), status_filter : $('#status_filter').val() },
    },
		columns: [{
		className: '',
		"data": "renter",
		"title": "Borrower's Name",
		"orderable": false,
	},{
		className: '',
		"data": "e_name",
		"title": "Equipment",
		"orderable": false,
		"searchable": false,
	},{
		className: '',
		"data": "qnty",
		"title": "Request Quantity",
		"orderable": false,
		"searchable": false,
	},{
		className: '',
		"data": "barangay_equipment.qnty",
		"title": "Current Stock",
		"orderable": false,
		"searchable": false,
	},{
		"width": "20%",
		"data": "reason",
		"title": "Reason",
		"orderable": false,
		"searchable": false,
	},{
		className: 'text-center',
		"data": "date",
		"title": "Date",
		"orderable": false,
		"searchable": false,
	},{
		className: 'text-center',
		"data": "status",
		"title": "Status",
		"orderable": false,
		"searchable": false,
		"render": function(data, type, row, meta){
				if (data == 1) {
					newdata = '<td><span class="badge p-2 badge-sm badge-info">Approved</span></td>'
				}else if(data == 2)
				{
					newdata = '<td><span class="badge p-2 badge-sm badge-danger">Dispproved</span></td>'
				}else if(data == 3)
				{
						newdata = '<td><span class="badge p-2 badge-sm badge-success">Returned</span></td>'
				}else
				{
					newdata = '<td><span class="badge p-2 badge-sm badge-warning">Pending</span></td>'
				}
		return newdata;
    }
	},{
		className: 'text-center',
		"data": "id",
		"title": "Options",
		"orderable": false,
		"searchable": false,
		"render": function(data, type, row, meta){
			if (row.status == 1) {
					newdata = '<button type="button" class="px-4 btn btn-sm btn-success" onclick="return_stock('+data+','+row.barangay_equipment_id+')"><i class="fa-solid fa-rotate-left"></i> Return</button>'
				}else if(row.status == 2)
				{
					newdata = '<button type="button" class="px-4 btn btn-sm btn-info" onclick="approve('+data+','+row.barangay_equipment_id+')"><i class="fa-solid fa-thumbs-up"></i> Approve</button>'
				}else if(row.status == 3)
				{
						newdata = '<span class="badge badge-info p-2 text-sm">This request is already done</span>'
				}else
				{
					newdata = '<button type="button" class="px-4 btn btn-sm btn-info" onclick="approve('+data+','+row.barangay_equipment_id+')"><i class="fa-solid fa-thumbs-up"></i> Approve</button>\
      				   <button type="button" class="px-4 btn btn-sm btn-danger" onclick="declined('+data+','+row.barangay_equipment_id+')"><i class="fa-solid fa-thumbs-down"></i> Dispprove</button>'
				}

      		
		return newdata;
    }
	}
	]
	});
	}


	function return_stock(rent_id, equipment_id){
			Swal.fire({
			  title: 'Are you sure?',
			  text: "This request equipment quantity will add to our current stock!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, return it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$.ajax({
		    type:"post",
		    url:"{{ route('admin.request-barangay.return') }}",
		    data:{ rent_id : rent_id, equipment_id : equipment_id},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     
		       
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    Swal.fire(
			      'Success!',
			      'Equipment has been returned successfully.',
			      'success'
			    )
			  }
			})	
	}


	function approve(rent_id, equipment_id){
		Swal.fire({
			  title: 'Are you sure?',
			  text: "This request will be approve!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, approve it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$.ajax({
		    type:"post",
		    url:"{{ route('admin.request-barangay.approave') }}",
		    data:{ rent_id : rent_id, equipment_id : equipment_id},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     	Swal.fire(
			      'Success!',
			      'This request has been approve.',
			      'success'
			    )
		       
		     }else{
		     	Swal.fire({
		                position: 'center',
		                icon: 'error',
		                title: response.message,
		                showConfirmButton: false,
		                timer: 4500
		              })
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    
			  }
			})
		
	}


	function declined(rent_id, equipment_id){
			Swal.fire({
			  title: 'Are you sure?',
			  text: "This request will be declined!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, decline it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$.ajax({
		    type:"post",
		    url:"{{ route('admin.request-barangay.decline') }}",
		    data:{ rent_id : rent_id, equipment_id : equipment_id},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     
		       
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    Swal.fire(
			      'Success!',
			      'This request has been declined.',
			      'success'
			    )
			  }
			})	
		}


	$("#spots_equipment_form").on('submit', function(e){
		var mydata = $(this).serialize();
		let url = $(this).attr('action');
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"get",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit').prop('disabled', true);
					$('#btn_submit').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					Swal.fire({
		                position: 'center',
		                icon: 'success',
		                title: 'Equipment Inserted Successfully!',
		                showConfirmButton: false,
		                timer: 1500
		              })
					
					showValidator(response.error,'spots_equipment_form');
					show_user_accounts();
					$('#modal_add_edit').modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'spots_equipment_form');
				}
				$('#btn_submit').prop('disabled', false);
				$('#btn_submit').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	

	function add_user_accounts(){
		$('#spots_equipment_form').trigger("reset");
		$('.modal-title').html('Add barangay Equipment');
		$('#modal_add_edit').modal('show');
	}

	
</script>
@endsection
