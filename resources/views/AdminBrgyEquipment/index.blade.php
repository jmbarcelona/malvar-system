@extends('Layout.app_admin')
@section('title', 'Barangay Equipments')
@section('css')
<style type="text/css">
.dataTables_filter { display: none; }
</style>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row pt-3">
    <div class="col-lg-12 col-md-12 col-12">
      <div class="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
        <div class="mb-3 mb-md-0">
          <h1 class="mb-1 h2 fw-bold">Barangay Equipment</h1>
        </div>
        <div class="d-flex">
          <button class="btn btn-success btn-shadow" onclick="stock_in();"><i class="fas fa-download"></i>&nbsp;Stock in</button>
          <button class="btn btn-danger btn-shadow  mx-2" onclick="stock_out();"><i class="fas fa-upload"></i></i>&nbsp;Stock out</button>
          <button class="btn btn-info btn-shadow" onclick="add_user_accounts();"><i class="fa-solid fa-plus"></i>&nbsp;Add Barangay Equipment</button>
        </div>
      </div>
    </div>
  </div>
  <form class="needs-validation" id="spots_equipment_form" action="{{ route('admin.barangay.store') }}" novalidate>
    <div class="container-fluid">
      <div class="card">
        <div class="card-header" onclick="$('#minimize_filter').click()">
          <h5 class="card-title">Record Filter</h5>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
            </button>
          </div>
        </div>
        
        <div class="card-body">
          <div class="row">
            <div class="position-relative mb-2 col-md-4">
              <label>Equipment</label>
              <input type="text" name="filter" id="filter" class="form-control" placeholder="Search equipment here">
              
            </div>
          </div>
          
        </div>
        <div class="card-footer text-right">
          <!-- <button type="button" class="btn btn-secondary" data-card-widget="collapse" id="minimize_filter">
          <i class="fa fa-minus"></i> Minimize Filter
          </button> -->
          <button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
          <button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
        </div>
        
      </div>
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
          <table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;"></table>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" role="dialog" id="modal_add_edit">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title for_add_edit h4">
            Add Barangay Equipment
          </div>
          <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <input type="hidden" name="id" id="id">
              <label class="required">Equipment Name</label><br>
              <input type="text" name="equipment_name" id="equipment_name" placeholder="Enter equipment name" class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_equipment_name"></div>
            </div>
            <div class="col-sm-12">
              <label class="required">Quantity</label><br>
              <input type="number" name="quantity" id="quantity" placeholder="Enter quantity" class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_quantity"></div>
            </div>
            <div class="col-sm-12">
              <label class="required">Description</label><br>
              <textarea name="description" id="description" placeholder="Enter description" class="mb-2 form-control" autocomplete="off"></textarea>
              <div class="invalid-feedback text-left" id="err_description"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-success px-5" id="btn_submit">Submit</button>
        </div>
      </div>
    </div>
  </div>
</form>
</div>
<!-- //stock in -->
<div class="modal fade" role="dialog" id="stock_in_modal">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <div class="modal-title h4">
        Stock in equipment
      </div>
      <button class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
      <form action="{{ route('admin.barangay.stockIn') }}" id="stock_in_form" class="needs-validation">
        <div class="row">
          <div class="col-sm-8">
            <label class="required">Equipment Name</label><br>
            <select id="selection_for_stock_in" name="selection_for_stock_in" class="form-control" onchange="selector_stock_in(this.value)">
            </select>
            <div class="invalid-feedback text-left" id="err_selection_for_stock_in"></div>
            <!-- <div class="invalid-feedback text-left" id="err_equipment_name"></div> -->
          </div>
          <div class="col-sm-4">
            <label class="required">Current Quantity</label><br>
            <input type="number" name="current_quantity" id="current_quantity" class="mb-2 form-control" autocomplete="off" disabled="" style="background-color: white">
          </div>
          <div class="col-sm-12">
            <label class="required">Add Quantity</label><br>
            <input type="number" name="add_for_stock_in" id="add_for_stock_in" placeholder="Enter quantity" class="mb-2 form-control" autocomplete="off">
            <div class="invalid-feedback text-left" id="err_add_for_stock_in"></div>
          </div>
          <div class="col-sm-12">
            <label class="required">Reason</label><br>
            <textarea name="reason" id="reason" placeholder="Enter reason" class="mb-2 form-control" autocomplete="off"></textarea>
            <div class="invalid-feedback text-left" id="err_reason"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-success px-5" id="btn_submit3">Submit</button>
      </div>
    </div>
  </div>
</div>
</form>
</div>
<!-- //stock out -->
<div class="modal fade" role="dialog" id="stock_out_modal">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <div class="modal-title h4">
      Stock out equipment
    </div>
    <button class="close" data-dismiss="modal">&times;</button>
  </div>
  <div class="modal-body">
    <form action="{{ route('admin.barangay.stockOut') }}" id="stock_out_form" class="needs-validation">
      <div class="row">
        <div class="col-sm-8">
          <label class="required">Equipment Name</label><br>
          <select id="selection_for_stock_out" name="selection_for_stock_out" class="form-control" onchange="selector_stock_out(this.value)">
          </select>
          <div class="invalid-feedback text-left" id="err_selection_for_stock_out"></div>
          <!-- <div class="invalid-feedback text-left" id="err_equipment_name"></div> -->
        </div>
        <div class="col-sm-4">
          <label class="required">Current Quantity</label><br>
          <input type="number" name="current_quantity1" id="current_quantity1" class="mb-2 form-control" autocomplete="off" disabled="" style="background-color: white">
        </div>
        <div class="col-sm-12">
          <label class="required">Stock Out Quantity</label><br>
          <input type="number" name="minus_for_stock_in" id="minus_for_stock_in" placeholder="Enter quantity" class="mb-2 form-control" autocomplete="off">
          <div class="invalid-feedback text-left" id="err_minus_for_stock_in"></div>
        </div>
        <div class="col-sm-12">
            <label class="required">Reason</label><br>
            <textarea name="reason" id="reason" placeholder="Enter reason" class="mb-2 form-control" autocomplete="off"></textarea>
            <div class="invalid-feedback text-left" id="err_reason"></div>
          </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
      <button type="submit" class="btn btn-success px-5" id="btn_submit3">Submit</button>
    </div>
  </div>
</div>
</div>
</form>
</div>
@endsection


@section('script')
<!-- Javascript Function-->
<script type="text/javascript">


$('#stock_out_form').on('submit', function(e){
	e.preventDefault();
	let formData = $(this).serialize();
	let url = $(this).attr('action');
	$.ajax({
			type:"post",
			url:url,
			data:formData,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit3').prop('disabled', true);
					$('#btn_submit3').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					Swal.fire({
		                position: 'center',
		                icon: 'success',
		                title: response.message,
		                showConfirmButton: false,
		                timer: 1500
		              })
					
					showValidator(response.error,'spots_equipment_form');
					show_user_accounts();
					$('#stock_out_modal').modal('hide');
					 $('#stock_out_form')[0].reset();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'spots_equipment_form');
					if (response.message) {
						Swal.fire({
		                position: 'center',
		                icon: 'error',
		                title: response.message,
		                showConfirmButton: false,
		                timer: 5500
		              })
					}
				}
				$('#btn_submit3').prop('disabled', false);
				$('#btn_submit3').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});

});




	function stock_out(){
	$('#stock_out_modal').modal('show');
	$('#stock_out_form').trigger("reset");
		$.ajax({
            url: "{{ route('admin.barangay.select') }}",
            method: 'get',
            type: 'json',
            success: function(response) {
              responseData = response.data;
              let table_output = responseData.map(function(usr){
                let out = '';
                    out += '<option value="'+usr.id+'">'+usr.e_name+'</option>';
                    return out;
              }).join('');
              var option1 = '<option value="" selected disabled>Please select equipment</option>'
            $("#selection_for_stock_out").html(option1+table_output);
            }  
                
        }).fail(function() {
            
        });
	}


	function selector_stock_out(id){
	$.ajax({
            url: "{{ route('admin.barangay.qnty') }}"+'/'+id,
            method: 'get',
            type: 'json',
            success: function(response) {
              $('#current_quantity1').val(response.data.qnty);
            }  
                
        }).fail(function() {
            
        });
}



$('#stock_in_form').on('submit', function(e){
	e.preventDefault();
	let formData = $(this).serialize();
	let url = $(this).attr('action');
	$.ajax({
			type:"post",
			url:url,
			data:formData,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit3').prop('disabled', true);
					$('#btn_submit3').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					Swal.fire({
		                position: 'center',
		                icon: 'success',
		                title: 'Equipment Added Successfully!',
		                showConfirmButton: false,
		                timer: 1500
		              })
					
					showValidator(response.error,'spots_equipment_form');
					show_user_accounts();
					$('#stock_in_modal').modal('hide');
					 $('#stock_in_form')[0].reset();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'spots_equipment_form');
				}
				$('#btn_submit3').prop('disabled', false);
				$('#btn_submit3').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});

});



function selector_stock_in(id){
	$.ajax({
            url: "{{ route('admin.barangay.qnty') }}"+'/'+id,
            method: 'get',
            type: 'json',
            success: function(response) {
              $('#current_quantity').val(response.data.qnty);
            }  
                
        }).fail(function() {
            
        });
}

	function stock_in(){
		$('#stock_in_form').trigger("reset");
	$('#stock_in_modal').modal('show');
		$.ajax({
            url: "{{ route('admin.barangay.select') }}",
            method: 'get',
            type: 'json',
            success: function(response) {
              responseData = response.data;
              let table_output = responseData.map(function(usr){
                let out = '';
                    out += '<option value="'+usr.id+'">'+usr.e_name+'</option>';
                    return out;
              }).join('');
              var option = '<option value="" selected disabled>Please select equipment</option>'
            $("#selection_for_stock_in").html(option+table_output);
            }  
                
        }).fail(function() {
            
        });
	}






	$('#refresh-filter').on('click', function(){
		$('#filter').val('');
		submit_filter();
	});

	function submit_filter(){
		show_user_accounts();
	}


	show_user_accounts();
	var tbl_user_accounts;
	function show_user_accounts(){
		if (tbl_user_accounts) {
			tbl_user_accounts.destroy();
		}
		tbl_user_accounts = $('#tbl_user_accounts').DataTable({
		pageLength: 10,
		responsive: true,
		deferRender: true,
		aaSorting: [],
		language: {
		"emptyTable": "No data available"
	},
	ajax: {
        url: "{{ route('admin.barangay.list') }}",
        data: {filter : $('#filter').val()},
    },
		columns: [{
		className: '',
		"data": "e_name",
		"title": "Equipment name",
		"orderable": false,
	},{
		className: '',
		"data": "qnty",
		"title": "Quantity",
		"orderable": false,
	},{
		className: '',
		"data": "description",
		"title": "Description",
		"orderable": false,
	},{
		className: 'text-center',
		"data": "id",
    "width": "200px",
		"title": "options",
		"orderable": false,
		"render": function(data, type, row, meta){
      newdata = '<button type="button" class="btn btn-sm btn-info" onclick="edit_s_equipment('+data+')"><i class="fa-solid fa-pen-to-square"></i> Edit</button>\
				 <button type="button" class="btn btn-sm btn-danger" onclick="delete_s_equipment('+data+')"><i class="fa-solid fa-trash-can"></i> Delete</button>'
		return newdata;
    }
	}
	]
	});
	}


	function edit_s_equipment(id){
		$.ajax({
            type:"get",
            url:"{{ route('admin.barangay.find') }}"+'/'+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
             if (response.status == true) {
                $('#quantity').val(response.data.qnty);
                $('#equipment_name').val(response.data.e_name);
                $('#id').val(response.data.id);
                $('#description').val(response.data.description);
                $('.for_add_edit').html('Edit Barangay Equipment')
                $('#modal_add_edit').modal('show');
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          });   
	}


	function delete_s_equipment(id){
		Swal.fire({
			  title: 'Are you sure?',
			  text: "You won't be able to revert this!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$.ajax({
		    type:"post",
		    url:"{{ route('admin.barangay.destroy') }}"+'/'+id,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     
		       
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    Swal.fire(
			      'Deleted!',
			      'Your record has been deleted.',
			      'success'
			    )
			  }
			})

		
	}

	$("#spots_equipment_form").on('submit', function(e){
		var mydata = $(this).serialize();
		let url = $(this).attr('action');
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"get",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit').prop('disabled', true);
					$('#btn_submit').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					Swal.fire({
		                position: 'center',
		                icon: 'success',
		                title: 'Equipment Inserted Successfully!',
		                showConfirmButton: false,
		                timer: 1500
		              })
					
					showValidator(response.error,'spots_equipment_form');
					show_user_accounts();
					$('#modal_add_edit').modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'spots_equipment_form');
				}
				$('#btn_submit').prop('disabled', false);
				$('#btn_submit').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_user_accounts(_this){
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this user_accounts?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"delete",
				url:"",
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					
					show_user_accounts();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function add_user_accounts(){
		$('#spots_equipment_form').trigger("reset");
		$('.for_add_edit').html('Add Barangay Equipment');
		$('#modal_add_edit').modal('show');
	}

	
</script>
@endsection
