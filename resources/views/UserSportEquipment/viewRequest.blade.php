@extends('Layout.app_user')
@section('title', 'Request View')
@section('content')
<!-- Content Wrapper. Contains page content -->
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Request View</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Home</a></li>
            <li class="breadcrumb-item active">Request View</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <div class="table-responsive col-sm-12">
          <div class="text-right mb-2">
            <button class="btn btn-danger btn-sm col-sm-1" onclick="back_button()"> <strong><i class="fa-solid fa-caret-left"></i> &nbsp;Back</strong></button>
          </div>
          <table class="table table-bordered table-hove" style="width: 100%;">
            <thead>
              <tr>
                <th>Equipment Name</th>
                <th>Quantity</th>
                <th>Date</th>
                <th class="text-center">Status</th>
                <th class="text-center">Remarks</th>
              </tr>
            </thead>
            <tbody>
              @foreach($request as $request)
              <tr>
                <td>{{ $request->e_name }}</td>
                <td>{{ $request->qnty }}</td>
                <td>{{ date('F j, Y', strtotime($request->date)) }}</td>
                
                @if($request->status == 1)
                <td class="text-center"><span class="badge p-2 badge-sm badge-info">Approved</span></td>
                @elseif($request->status == 2)
                <td class="text-center"><span class="badge p-2 badge-sm badge-danger">Declined</span></td>
                @elseif($request->status == 3)
                <td class="text-center"><span class="badge p-2 badge-sm badge-success">Returned</span></td>
                @else
                <td class="text-center"><span class="badge p-2 badge-sm badge-warning">Pending</span></td>
                @endif

                @if($request->status == 1)
                <td class="text-center"><span class="badge p-2 badge-sm badge-info">Ready to Pickup</span></td>
                @elseif($request->status == 2)
                <td class="text-center"><span class="badge p-2 badge-sm badge-danger">Request Declined</span></td>
                @elseif($request->status == 3)
                <td class="text-center"><span class="badge p-2 badge-sm badge-success">Equipment/s Returned</span></td>
                @else
                <td class="text-center"><span class="badge p-2 badge-sm badge-warning">Equipment/s Requested</span></td>
                @endif
                
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
@endsection
@section('script')
<script type="text/javascript">
function back_button(){
  window.history.go(-1);
}
</script>
@endsection

