@extends('Layout.app_user')
@section('title', 'Request Sports Equipment')
@section('content')
<!-- Content Wrapper. Contains page content -->
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Request Sports Equipment</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Home</a></li>
            <li class="breadcrumb-item active">Request Sports Equipment</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="col-sm-12 col-md-12">
              <form action="{{ route('user.sports.store') }}" class="needs-validation" id="request_form">
                <div class="card">
                  <div class="card-header h3">
                    <a href="{{ route('user.sports.requestView') }}">
                      <button type="button" class="btn btn-info">
                      <i class="fa-solid fa-eye"></i>
                      View Requested Equipments</button>
                    </a>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-8  mb-3">
                        <label>Sports Equipments</label>
                        <select class="form-control" id="sports_equipments" name="sports_equipments" onchange="sports_equipments_select(this.value)">
                          <option value="" selected="" disabled="">Select Sports Equipment</option>
                          @foreach($SportEquipment as $SportEquipment)
                          <option value="{{ $SportEquipment->id }}">{{ $SportEquipment->e_name }}</option>
                          @endforeach
                        </select>
                        <div class="invalid-feedback text-left" id="err_sports_equipments"></div>
                      </div>
                      <div class="col-sm-4 mb-3">
                        <input type="hidden" name="e_name" id="e_name">
                        <label>Current Quantity</label>
                        <input type="number" class="form-control" id="current_quantity" name="current_quantity" readonly="" style="background-color: white">
                        <div class="invalid-feedback text-left" id="err_current_quantity"></div>
                      </div>
                      <div class="col-sm-6 mb-3">
                        <label>Request Quantity</label>
                        <input type="number" class="form-control" id="request_quantity" name="request_quantity" placeholder="Enter request quantity">
                        <div class="invalid-feedback text-left" id="err_request_quantity"></div>
                      </div>
                      <div class="col-sm-6 mb-3">
                        <label>Date</label>
                        <input type="date" class="form-control" id="date" name="date" placeholder="Enter request quantity">
                        <div class="invalid-feedback text-left" id="err_date"></div>
                      </div>
                      <div class="col-sm-12">
                        <label>Reason</label>
                        <textarea class="form-control" id="reason" name="reason" rows="1" placeholder="Enter reason for request"></textarea>
                        <div class="invalid-feedback text-left" id="err_reason"></div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer text-right">
                    <button type="button" class="btn btn-danger" onclick="cancel_button()">Cancel</button>
                    <button type="submit" class="btn btn-success" id="btn_submit">Submit Request</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script type="text/javascript">
function cancel_button(){
   $('#request_form')[0].reset();
}


$('#request_form').on('submit', function(e){
  e.preventDefault();
  let url = $(this).attr('action');
  let formData = $(this).serialize();
  $.ajax({
      type:"post",
      url:url,
      data:formData,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
          $('#btn_submit').prop('disabled', true);
          $('#btn_submit').text('Please wait...');
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
        $('#btn_submit').prop('disabled', false);
        $('#btn_submit').text('Submit Request');
          $('#request_form')[0].reset();
          Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                  })
          
          showValidator(response.error,'request_form');
          show_user_accounts();
          $('#modal_add_edit').modal('hide');
        }else{
        $('#btn_submit').prop('disabled', false);
        $('#btn_submit').text('Submit Request');
          if (response.message) {
            Swal.fire({
                    position: 'center',
                    icon: 'info',
                    title: response.message,
                    showConfirmButton: true,
                    timer: 10000
                  })
          }
          //<!-- your error message or action here! -->
          showValidator(response.error,'request_form');
        }
      },
      error:function(error){
        console.log(error)
      }
    });

});




  function sports_equipments_select(id){
      $.ajax({
      url: "{{ route('user.sports.qnty') }}"+'/'+id,
      method: 'get',
      type: 'json',
      success: function(response) {
        $('#current_quantity').val(response.data.qnty);
        $('#e_name').val(response.data.e_name);
      }  
          
  }).fail(function() {
      
  });
}


</script>
@endsection

