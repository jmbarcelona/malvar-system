@extends('Layout.app_user')
@section('title', 'Contacts')
@section('css')
<style type="text/css">
.dataTables_filter { display: none; }

</style>
@endsection
@section('content')
<div class="content-header" style="border-bottom: solid lightgrey 1px">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Contacts</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-dark">Contacts</a></li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>

        
         <div class="container-fluid mt-4">
         <div class="row">
         
          @foreach($contact as $contact)
            <div class="col-sm-4">
            <div class="card" style="border: solid lightgrey 2px">
              <div class="card-body">
                <div class="col-sm-12">
                  <div class="row">

                    <div class="col-sm-4">
                      @if($contact->img)
                      <img src="{{ asset('storage'.'/'."$contact->img") }}" class="img-thumbnail" style="width: 100px; height: 100px;">
                      @else
                      <img src="{{ asset('img/default.png') }}" class="img-thumbnail" style="width: 100px; height: 100px;">
                      @endif
                    </div>
                    <div class="col-sm-8">  
                      <div class="col-sm-6">
                      <label class="mt-2">{{$contact->c_name}}</label>
                      </div>
                      <div class="col-sm-6">
                      <input type="text" name="" class="form-control font-weight-bold" value="{{$contact->c_no}}" readonly="">
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </div>
            @endforeach
          
            

        </div>
        </div>
          
       
      

@endsection


@section('script')
<!-- Javascript Function-->
<script type="text/javascript">

</script>
@endsection
