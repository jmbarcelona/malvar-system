@extends('Layout.app_user')
@section('title', 'Verify Account')
@section('content')

<!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Verify Account</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">

              <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Home</a></li>
              <li class="breadcrumb-item active">Verify Account</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
          <!-- /.content-header -->
    

@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection

