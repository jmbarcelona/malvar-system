@extends('Layout.app_admin')
@section('title', 'Verify users')
@section('css')
<style type="text/css">
</style>
@endsection
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Verify users</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Home</a></li>
            <li class="breadcrumb-item active">Verify users</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
	<form class="needs-validation" id="all_data_form" action="{{ route('admin.users.store') }}" novalidate>
		<div class="container-fluid">
			<div class="card">
				<div class="card-header" onclick="$('#minimize_filter').click()" >
					<h5 class="card-title">Record Filter</h5>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse">
						<i class="fas fa-minus"></i>
						</button>
					</div>
				</div>
				
				<div class="card-body">
					<div class="row">
						<div class="mb-2 col-md-3">
							<label>Status Filter</label>
							<select class="form-control" id="status_filter" name="status_filter">
								<option value="">Select Status</option>
								<option value="2">Pending</option>
								<option value="3">Approved</option>
								<option value="4">Disapproved</option>
							</select>
						</div>

					
					</div>
					
				</div>
				<div class="card-footer text-right">
					<!-- <button type="button" class="btn btn-secondary" data-card-widget="collapse" id="minimize_filter">
					<i class="fa fa-minus"></i> Minimize Filter
					</button> -->
					<button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
					<button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
				</div>
				
			</div>
			<div class="card">
				<div class="card-body">
					<div class="table-responsive">
					<table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;"></table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" role="dialog" id="modal_add_edit">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title h4">
						Add User Account
					</div>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-4">
							<input type="hidden" name="id" id="id">
							<label class="required">Firstname</label>
							<input type="text" name="first_name" id="first_name" placeholder="Enter firstname here" class="mb-2 form-control" autocomplete="off">
							<div class="invalid-feedback text-left" id="err_first_name"></div>
						</div>
						<div class="col-sm-4">
							<label>Middlename</label>
							<input type="text" name="middle_name" id="middle_name" placeholder="Enter middlename here" class="mb-2 form-control" autocomplete="off">
							<div class="invalid-feedback text-left" id="err_middle_name"></div>
						</div>
						<div class="col-sm-4">
							<label class="required">Lastname</label>
							<input type="text" name="last_name" id="last_name" placeholder="Enter lastname here" class="mb-2 form-control" autocomplete="off">
							<div class="invalid-feedback text-left" id="err_last_name"></div>
						</div>
						<div class="col-sm-12">
							<label class="required">Date of Birth</label>
							<input type="date" name="bday" id="bday" class="mb-2 form-control" autocomplete="off">
							<div class="invalid-feedback text-left" id="err_bday"></div>
						</div>
						<div class="col-sm-6">
							<label class="required">Gender</label>
							<select name="gender" id="gender" class="form-control" autocomplete="off">
								<option selected="" disabled="">Please select gender</option>
								<option value="Male">Male</option>
								<option value="Female">Female</option>
							</select>
							<div class="invalid-feedback text-left" id="err_gender"></div>
						</div>
						<div class="col-sm-6">
							<label class="required">Contact</label>
							<input type="text" name="contact" id="contact" placeholder="Enter contact here" class="mb-2 form-control" autocomplete="off">
							<div class="invalid-feedback text-left" id="err_contact"></div>
						</div>
						<div class="col-sm-12">
							<label class="required">Address</label>
							<textarea class="form-control mb-2" rows="3" id="address" name="address" placeholder="Enter address here"></textarea>
							<div class="invalid-feedback text-left" id="err_address"></div>
						</div>
						<div id="hide_on_edit" class="col-sm-12">
							<div class="col-sm-12">
								<label class="required">Email</label>
								<input type="text" name="email_address" id="email_address" class="form-control mb-2" placeholder="Enter email here">
								<div class="invalid-feedback text-left" id="err_email_address"></div>
							</div>
							<div class="col-sm-12">
								<label class="required">Password</label>
								<input type="password" name="password" id="password" class="form-control mb-2" placeholder="Enter password here">
								<div class="invalid-feedback text-left" id="err_password"></div>
							</div>
							<div class="col-sm-12">
								<label class="required">Confirm Password</label>
								<input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Verify password here">
								<div class="invalid-feedback text-left" id="err_confirm_password"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success px-5" id="btn_submit">Submit</button>
				</div>
			</div>
		</div>
	</div>
</form>
</div>

 <div class="modal fade" role="dialog" id="view_img_modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title h4">
            
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
          	<div class="text-center">
            <img src="" alt="" class="img-fluid" id="view_img_shower">
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>


@endsection


@section('script')
<script type="text/javascript">


	$('#refresh-filter').on('click', function(){
		$('#filter').val('');
		submit_filter();
	});

	function submit_filter(){
		show_user_accounts();
		$('#minimize_filter').trigger('click');
	}


	show_user_accounts();
	var tbl_user_accounts;
	function show_user_accounts(){
		if (tbl_user_accounts) {
			tbl_user_accounts.destroy();
		}
		tbl_user_accounts = $('#tbl_user_accounts').DataTable({
		pageLength: 10,
		responsive: true,
		deferRender: true,
		aaSorting: [],
		language: {
		"emptyTable": "No data available"
	},
	ajax: {
        url: "{{ route('admin.verify.list') }}",
        data: {status_filter : $('#status_filter').val()},
    },
		columns: [{
		className: '',
		"data": "id",
		"title": "Verification image",
		"orderable": false,
		"width": 150,
    "render": function(data, type, row, meta){
       return newdata = '<button type="button" class="btn btn-info btn-sm col-sm-11" onclick="view_img('+data+')" >View</button>'
    }
	},{
		className: '',
		"data": "id",
		"title": "Fullname",
		"orderable": false,
		"render": function(data, type, row, meta){
		 return newdata = row.first_name+' '+row.last_name
		}
	},{
		className: '',
		"data": "contact",
		"title": "Contact #",
		"orderable": false,
	},{
		className: '',
		"data": "email_address",
		"title": "Email",
		"orderable": false,
	},{
		className: 'text-center',
		"data": "status",
		"title": "Request Status",
		"render": function(data, type, row, meta){
			if (data == 3) {
				newdata = '<td><span class="badge p-2 badge-sm badge-success">Approved</span></td>'
			}else if(data == 2){
				newdata = '<td><span class="badge p-2 badge-sm badge-warning">Pending</span></td>'
			}else{
				newdata = '<td><span class="badge p-2 badge-sm badge-danger">Disapproved</span></td>'
			}
		 return newdata
		}
	},{
		className: 'text-center',
		"data": "id",
		"title": "Options",
		"orderable": false,
		"render": function(data, type, row, meta){
      newdata = '<button type="button" class="btn btn-sm btn-info" onclick="approve('+data+')"><i class="fas fa-thumbs-up"></i> Approve</button>\
				 <button type="button" class="btn btn-sm btn-danger" onclick="disapprove('+data+')"><i class="fas fa-thumbs-down"></i> Disapprove</button>'
		return newdata;
    }
	}
	]
	});
	}

	function view_img(id){
		$.ajax({
            type:"get",
            url:"{{ route('admin.verify.find') }}"+'/'+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
             if (response.status == true) {               
                $('#view_img_modal').modal('show');
                
                $("#view_img_shower").attr("src", "http://localhost/malvar-system/public/storage/"+response.data.proof_img+"")
                $('.modal-title').html('Verification Image: '+response.data.first_name+' '+response.data.last_name)
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          }); 
				}

		function approve(id){
		Swal.fire({
			  title: 'Are you sure?',
			  text: "This request will be approve!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, approve it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$.ajax({
		    type:"post",
		    url:"{{ route('admin.verify.approve') }}"+"/"+id,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     	Swal.fire(
			      'Success!',
			      'Account Verified Successfully',
			      'success'
			    )
		       
		     }else{
		     	Swal.fire({
		                position: 'center',
		                icon: 'error',
		                title: response.message,
		                showConfirmButton: false,
		                timer: 4500
		              })
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    
			  }
			})
	}

	function disapprove(id){
		Swal.fire({
			  title: 'Are you sure?',
			  text: "This request will be disapprove!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, disapprove it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$.ajax({
		    type:"post",
		    url:"{{ route('admin.verify.disapprove') }}"+"/"+id,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     	Swal.fire(
			      'Success!',
			      'Account Verification Declined!',
			      'success'
			    )
		       
		     }else{
		     	Swal.fire({
		                position: 'center',
		                icon: 'error',
		                title: response.message,
		                showConfirmButton: false,
		                timer: 4500
		              })
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    
			  }
			})
	}

	
</script>
@endsection
