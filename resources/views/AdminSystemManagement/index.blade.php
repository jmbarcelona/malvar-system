@extends('Layout.app_admin')
@section('title', 'System Management')
@section('css')
<style type="text/css">
.form-control:focus {
box-shadow: none;
border-color: #BA68C8
}
.profile-button {
background: rgb(99, 39, 120);
box-shadow: none;
border: none
}
.profile-button:hover {
background: #682773
}
.profile-button:focus {
background: #682773;
box-shadow: none
}
.profile-button:active {
background: #682773;
box-shadow: none
}
.back:hover {
color: #682773;
cursor: pointer
}
.labels {
font-size: 11px
}
.add-experience:hover {
background: #BA68C8;
color: #fff;
cursor: pointer;
border: solid 1px #BA68C8
}
.container-fluid{
height: 100vh;
}
</style>
@endsection
@section('content')
<div class="container-fluid form-group">
  <div class="row justify-content-center">
    <div class="col-md-5">
      <div class="d-flex flex-column align-items-center text-center p-3 py-5">
        <div>
          <h4>System Name</h4>
        </div>
        <div class="mb-4">
          <form action="{{ route('admin.system.edit') }}" id="system_form" class="needs-validation">
            <input type="hidden" id="system_id" name="system_id" value="{{ $system->id ?? ''}}">
            <input type="text" name="system_name" class="form-control text-center" placeholder="Enter system name here" value="{{ $system->system_name ?? '' }}">
          </div>
          <h4>System Logo</h4>
          @if(!empty($system->logo))
          <img class="rounded-circle img-thumbnail" id="img_shower" src="{{ asset('storage/'.$system->logo) }}" style="width: 260px; height:260px; ">
          @else
          <img class="rounded-circle img-thumbnail" id="img_shower" src="{{ asset('img/default.png') }}" style="width: 260px; height:260px; ">
          @endif
          
          <div class="col-sm-8 mt-2 img_buttons">
            <button type="button" class="btn btn-sm btn-info px-5" onclick="upload_img_btn()">browse photo</button>
          </div>
          <input type="file" class="form-control" name="img" id="img" hidden="" />
        </div>
      </div>
      <div class="col-md-5">
        <div class="p-3 py-5">
          <div class="d-flex justify-content-between align-items-center mb-3">
            <h4 class="text-right">Barangay Captain Info</h4>
          </div>
          <div class="row mt-2">
            <div class="col-md-12 form-group">
              <input type="hidden" name="id" id="id" class="form-control bg-light" value="{{ $user->id ?? '' }}">
              
              <label class="labels">Fullname</label>
              <input type="text" name="full_name" id="full_name" class="form-control bg-light" placeholder="Enter Fullname"
              value="{{ $user->first_name ?? ''}}">
              <div class="invalid-feedback text-left" id="err_full_name"></div>
            </div>
            <div class="col-md-12 form-group">
              <label class="labels">Address</label>
              <input type="text" class="form-control bg-light" placeholder="Enter Address" id="address" name="address" value="{{ $user->address ?? '' }}">
              <div class="invalid-feedback text-left" id="err_address"></div>
            </div>
            <div class="col-md-12 form-group">
              <label class="labels">Gender</label>
              <select class="form-control bg-light" id="gender" name="gender">
                <option disabled="" selected="">Please select gender</option>
                @if(!empty($user->gender))
                <option value="Male" {{ $user->gender == "Male" ? "selected" : '' }}>Male</option>
                <option value="Female" {{ $user->gender == "Female" ? "selected" : '' }}>Female</option>
                @else
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                @endif
                
              </select>
              <div class="invalid-feedback text-left" id="err_gender"></div>
            </div>
            <div class="col-md-12 form-group">
              <label class="labels">Contact Number</label>
              <input type="text" class="form-control bg-light" placeholder="Enter phone number" id="contact" name="contact" value="{{ $user->contact ?? ''}}">
              <div class="invalid-feedback text-left" id="err_contact"></div>
            </div>
            <div class="col-md-12 form-group">
              <label class="labels">Email</label>
              <input type="text" class="form-control bg-light" placeholder="Enter email address" id="email" name="email" value="{{ $user->email_address ?? '' }}">
              <div class="invalid-feedback text-left" id="err_email"></div>
            </div>
            <div class="col-md-12 form-group mt-3">
              <div class="row justify-content-center">
                <div class="col-sm-12">
                  <button type="submit" class="btn col-sm-12 btn-success">
                  <strong>Save</strong>
                  </button>
                </form>
              </div>
            </div>
          </div>
@endsection
@section('script')
<script type="text/javascript">

  $('#system_form').on('submit', function(e){
    e.preventDefault();
    let form = $("#system_form")[0];
    // Create an FormData object 
    var data = new FormData(form);
    let url = $(this).attr('action');
    $.ajax({
          type:"POST",
          url:url,
          data:data,
          enctype: 'multipart/form-data',
          processData: false,  // Important!
          contentType: false,
          dataType:'json',
          beforeSend:function(){
          },
          success:function(response){
            // console.log(response);
           if (response.status == true) {
              Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: response.message,
                  showConfirmButton: false,
                  timer: 1500,
                })

                setTimeout(function(){
                  location.reload();
                }, 2000);
              
           }else{
            showValidator(response.error,'system_form');
            if (response.message) {
              Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: response.message,
                  showConfirmButton: false,
                  timer: 4500,
                })
            }
              
            console.log(response);
           }
           
           
          },
          error: function(error){
            console.log(error);
          }
        });

  });

function upload_img_btn(){
  $('#img').trigger('click');
}

</script>
    <script>
      var selDiv = "";
      var storedFiles = [];
      $(document).ready(function () {
        $("#img").on("change", handleFileSelect);
        selDiv = $("#img_shower");
      });

      function handleFileSelect(e) {
        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        filesArr.forEach(function (f) {
          if (!f.type.match("image.*")) {
            return;
          }
          storedFiles.push(f);

          var reader = new FileReader();
          reader.onload = function (e) {
             $("#img_shower").attr('src', e.target.result);
             $("#img_shower").attr('data-file', f.name);
          };
          reader.readAsDataURL(f);
        });
      }
    </script>
@endsection




