@extends('Layout.app_user')
@section('title', 'Request Barangay Indigency')
@section('content')
<!-- Content Wrapper. Contains page content -->
<!-- Content Header (Page header) -->
<div class="content-header" style="border-bottom: solid lightgrey 1px">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Request Barangay Indigency</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Home</a></li>
            <li class="breadcrumb-item active">Request Barangay Indigency</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>

        <div class="container-fluid mt-2">
          <div class="row justify-content-center">
            <div class="col-sm-12 col-md-12">
              <form action="{{ route('user.barangay_indigency.store') }}" class="needs-validation" id="add_indigency">
                <div class="card">
                  <div class="card-header h3">
                    <a href="{{ route('user.barangay_indigency.request') }}">
                      <button type="button" class="btn btn-info">
                      <i class="fa-solid fa-eye"></i>
                      View All Request</button>
                    </a>
                  </div>
                  <div class="card-body">
                    <div class="row">

                      <div class="col-sm-6">
              <input type="hidden" name="id" id="id">
              <label class="required">CTC No.</label><br>
              <input type="number" name="ctc_no" id="ctc_no" placeholder="Enter ctc number" class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_ctc_no"></div>
            </div>

            <div class="col-sm-6">
              <label class="required">Firstname</label><br>
              <input type="text" name="first_name" id="first_name" placeholder="Enter firstname" class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_first_name"></div>
            </div>

            <div class="col-sm-6">
              <label class="required">Middlename</label><br>
              <input type="text" name="middle_name" id="middle_name" placeholder="Enter middlename" class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_middle_name"></div>
            </div>

            <div class="col-sm-6">
              <label class="required">Lastname</label><br>
              <input type="text" name="last_name" id="last_name" placeholder="Enter lastname" class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_last_name"></div>
            </div>

            <div class="col-sm-6">
              <label class="required">Age</label><br>
              <input type="number" name="age" id="age" placeholder="Enter age" class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_age"></div>
            </div>
            <div class="col-sm-6">
              <label class="required">Civil Status</label><br>
              <select name="civil_status" id="civil_status" placeholder="Enter contact number" class="mb-2 form-control" autocomplete="off">
                <option value="" selected="" disabled="">Select Civil Status</option>
                <option value="Single">Single</option>
                <option value="Married">Married</option>
                <option value="Divorced">Divorced</option>
                <option value="Widowed">Widowed</option>
                <option value="Divorced">Divorced</option>
                <option value="Separated">Separated</option>
              </select>
              <div class="invalid-feedback text-left" id="err_civil_status"></div>
            </div>

            

            <div class="col-sm-12">
              <label class="required">Address</label><br>
              <textarea name="address" id="address" rows="1" placeholder="Enter address" class="mb-2 form-control" autocomplete="off"></textarea>
              <div class="invalid-feedback text-left" id="err_address"></div>
            </div>

            <div class="col-sm-12">
              <label class="required">Purpose</label><br>
              <textarea name="purpose" id="purpose" rows="1" placeholder="Enter purpose" class="mb-2 form-control" autocomplete="off"></textarea>
              <div class="invalid-feedback text-left" id="err_purpose"></div>
            </div>
                </div>
                  </div>
                  <div class="card-footer text-right">
                    <button type="button" class="btn btn-danger" onclick="cancel_button()">Cancel</button>
                    <button type="submit" class="btn btn-success" id="btn_submit">Submit Request</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
       
@endsection
@section('script')
<script type="text/javascript">

    $("#add_indigency").on('submit', function(e){
        var mydata = $(this).serialize();
        let url = $(this).attr('action');
        e.stopPropagation();
        e.preventDefault(e);

        $.ajax({
          type:"post",
          url:url,
          data:mydata,
          cache:false,
          beforeSend:function(){
              //<!-- your before success function -->
              $('#btn_submit').prop('disabled', true);
              $('#btn_submit').text('Please wait...');
          },
          success:function(response){
              //console.log(response)
            if(response.status == true){
            $('#btn_submit').prop('disabled', false);
            $('#btn_submit').text('Save');
               $('#add_indigency')[0].reset();
              Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Barangay Indigency Requested Successfully!',
                        showConfirmButton: false,
                        timer: 1500
                      })
              
              showValidator(response.error,'add_indigency');
              show_user_accounts();

            }else{
              //<!-- your error message or action here! -->
              showValidator(response.error,'add_indigency');

              if (response.message) {
                Swal.fire({
                        position: 'center',
                        icon: 'info',
                        title: response.message,
                        showConfirmButton: true,
                        timer: 10000
                      })
              }
              
            }
            $('#btn_submit').prop('disabled', false);
            $('#btn_submit').text('Save');
          },
          error:function(error){
            console.log(error)
          }
        });
      });

</script>
@endsection

