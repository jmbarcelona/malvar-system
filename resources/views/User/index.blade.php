@extends('Layout.app_user')
@if(Auth::user()->status == 3)
@section('title', 'Dashboard')
@else
@section('title', 'Verify Account')
@endif
@section('content')

@if(Auth::user()->status == 3)
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
               <div class="col-lg-4 col-12">
                <!-- small box -->
                <div class="small-box bg-warning">
                  <div class="inner">
                    <h3>{{ totalAnnouncement() }}</h3>
                    <p>Announcement</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-file-alt"></i>
                  </div>
                  <a href="{{ route('user.announcement.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <div class="col-lg-4 col-12">
                <!-- small box -->
                <div class="small-box bg-info">
                  <div class="inner">
                    <h3>{{ BarangayEquipmentRent() }}</h3>
                    <p>Barangay Equipment Request &nbsp;(Today)</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-chair"></i>
                  </div>
                  <a href="{{ route('user.barangay.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-4 col-12">
                <!-- small box -->
                <div class="small-box bg-danger">
                  <div class="inner">
                    <h3>{{ UserSportRent() }} </h3>
                    <p>Sports Equipment Request &nbsp;(Today)</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-baseball-ball"></i>
                  </div>
                  <a href="{{ route('user.sports.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>

              <div class="col-lg-4 col-12">
                <!-- small box -->
                <div class="small-box bg-info">
                  <div class="inner">
                    <h3>{{ UserBarangayIndigency() }} </h3>
                    <p>Barangay Indigency Request &nbsp;(Today)</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-file"></i>
                  </div>
                  <a href="{{ route('user.barangay_indigency.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>

              <div class="col-lg-4 col-12">
                <!-- small box -->
                <div class="small-box bg-success">
                  <div class="inner">
                    <h3>{{ UserBarangayClearance() }} </h3>
                    <p>Barangay Clearance Request &nbsp;(Today)</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-file-alt"></i>
                  </div>
                  <a href="{{ route('user.barangay_clearance.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
             
             
              <div class="col-lg-4 col-12">
                <!-- small box -->
                <div class="small-box" style="background-color: tomato">
                  <div class="inner">
                    <h3>{{ UserGymcount() }}</h3>
                    <p>Gymnasium Scheduling Request&nbsp;(Today)</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-basketball-ball"></i>
                  </div>
                  <a href="{{ route('user.court.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <!-- ./col -->
            </div>
            @elseif(Auth::user()->status == 2)
            <div class="content-header">
              <div class="container-fluid">
                <div class="row mb-2">
                  <div class="col-sm-6">
                    <h1 class="m-0">Verify Account</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Verify Account</li>
                      </ol>
                      </div><!-- /.col -->
                      </div><!-- /.row -->
                      </div><!-- /.container-fluid -->
                    </div>
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-sm-5">
                          <form action="{{ route('user.user_verify_account.store') }}" id="verify_form" class="needs-validation">
                            <div class="card">
                              <div class="card-body" style="border-top: solid green;">
                                <div class="row">
                                  <div class="col-sm-12">
                                    <h3>Verification request sent!</h3>
                                    <p>Please wait for the administrator's confimation</p>
                                  </div>
                                </div>
                              </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
            @else
            <div class="content-header">
              <div class="container-fluid">
                <div class="row mb-2">
                  <div class="col-sm-6">
                    <h1 class="m-0">Verify Account</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Verify Account</li>
                      </ol>
                      </div><!-- /.col -->
                      </div><!-- /.row -->
                      </div><!-- /.container-fluid -->
                    </div>

                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-sm-5">
                          <form action="{{ route('user.user_verify_account.store') }}" id="verify_form" class="needs-validation">
                            <div class="card">
                              <div class="card-body" style="border-top: solid green;">
                                <div class="row">
                                  <div class="col-sm-12">
                                <div class="col-sm-12">
                                  <p>To access the full website you must verify your account</p>
                                </div>
                                  <div class="col-sm-12">
                                    <label>Submit any proof that you are a citizen of this barangay</label>
                                    <input type="file" class="form-control pl-1 pt-1" name="proof_img" id="proof_img">
                                    <div class="invalid-feedback" id="err_proof_img"></div>
                                  </div>
                                </div>
                                </div>
                              </div>
                              <div class="card-footer text-right">
                                <button type="submit" class="btn btn-success btn-sm" id="btn_submit">Submit</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
            @endif
            @endsection
            @section('script')
<script type="text/javascript">

  @if(Auth::user()->status === 1)
  Swal.fire({
      position: 'center',
      icon: 'info',
      title: 'To access the full website you must verify your account',
      showConfirmButton: false,
      timer: 6000
    })
  @elseif(Auth::user()->status === 4)
    Swal.fire({
          position: 'center',
          icon: 'info',
          title: 'Previous request declined, try uploading a clearer image',
          showConfirmButton: false,
          timer: 6000
        })
  @endif


$("#verify_form").on('submit', function(e){
        let form = $("#verify_form")[0];
          // Create an FormData object 
        var data = new FormData(form);
        let url = $(this).attr('action');
        e.preventDefault(e);

        $.ajax({
          type:"post",
          url:url,
          data:data,
          cache:false,
          enctype: 'multipart/form-data',
          processData: false,  // Important!
          contentType: false,
          beforeSend:function(){
              //<!-- your before success function -->
              $('#btn_submit').prop('disabled', true);
              $('#btn_submit').text('Please wait...');
          },
          success:function(response){
              //console.log(response)
            if(response.status == true){
            $('#btn_submit').prop('disabled', false);
            $('#btn_submit').text('Save');
            $('#verify_form')[0].reset();
              Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'success!',
                        showConfirmButton: false,
                        timer: 1500
                      })

              setTimeout(function(){
                location.reload();
              }, 2000);
              
              showValidator(response.error,'verify_form');
            }else{
              
              showValidator(response.error,'verify_form');
              if (response.message) {
                  Swal.fire({
                        position: 'center',
                        icon: 'info',
                        title: response.message,
                        showConfirmButton: true,
                        timer: 10000
                      })
              }
              
            }
            $('#btn_submit').prop('disabled', false);
            $('#btn_submit').text('Save');
          },
          error:function(error){
            console.log(error)
            $('#btn_submit').prop('disabled', false);
            $('#btn_submit').text('Save');
          }
        });
      });

</script>
@endsection
