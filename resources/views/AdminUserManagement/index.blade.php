@extends('Layout.app_admin')
@section('title', 'User Management')
@section('css')
<style type="text/css">
	.dataTables_filter { display: none; }
</style>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row pt-3">
		<div class="col-lg-12 col-md-12 col-12">
			<div class="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
				<div class="mb-3 mb-md-0">
					<h1 class="mb-1 h2 fw-bold">User Management</h1>
				</div>
				<div class="d-flex">
					<button class="btn btn-info btn-shadow" onclick="add_user_accounts();"><i class="fa-solid fa-user-plus"></i></i></i>&nbsp;Add User</button>
				</div>
			</div>
		</div>
	</div>
	<form class="needs-validation" id="all_data_form" action="{{ route('admin.users.store') }}" novalidate>
		<div class="container-fluid">
			<div class="card">
				<div class="card-header" onclick="$('#minimize_filter').click()" >
					<h5 class="card-title">Record Filter</h5>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse">
						<i class="fas fa-minus"></i>
						</button>
					</div>
				</div>
				
				<div class="card-body">
					<div class="row">
						<div class="position-relative mb-2 col-md-4">
							<label>Fullname or Address</label>
							<input type="text" name="filter" id="filter" class="form-control" placeholder="Search here">
							
						</div>
					</div>
					
				</div>
				<div class="card-footer text-right">
					<!-- <button type="button" class="btn btn-secondary" data-card-widget="collapse" id="minimize_filter">
					<i class="fa fa-minus"></i> Minimize Filter
					</button> -->
					<button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
					<button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
				</div>
				
			</div>
			<div class="card">
				<div class="card-body">
					<div class="table-responsive">
					<table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;"></table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" role="dialog" id="modal_add_edit">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title h4">
						Add User Account
					</div>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-4">
							<input type="hidden" name="id" id="id">
							<label class="required">Firstname</label>
							<input type="text" name="first_name" id="first_name" placeholder="Enter firstname here" class="mb-2 form-control" autocomplete="off">
							<div class="invalid-feedback text-left" id="err_first_name"></div>
						</div>
						<div class="col-sm-4">
							<label>Middlename</label>
							<input type="text" name="middle_name" id="middle_name" placeholder="Enter middlename here" class="mb-2 form-control" autocomplete="off">
							<div class="invalid-feedback text-left" id="err_middle_name"></div>
						</div>
						<div class="col-sm-4">
							<label class="required">Lastname</label>
							<input type="text" name="last_name" id="last_name" placeholder="Enter lastname here" class="mb-2 form-control" autocomplete="off">
							<div class="invalid-feedback text-left" id="err_last_name"></div>
						</div>
						<div class="col-sm-12">
							<label class="required">Date of Birth</label>
							<input type="date" name="bday" id="bday" class="mb-2 form-control" autocomplete="off">
							<div class="invalid-feedback text-left" id="err_bday"></div>
						</div>
						<div class="col-sm-6">
							<label class="required">Gender</label>
							<select name="gender" id="gender" class="form-control" autocomplete="off">
								<option selected="" disabled="">Please select gender</option>
								<option value="Male">Male</option>
								<option value="Female">Female</option>
							</select>
							<div class="invalid-feedback text-left" id="err_gender"></div>
						</div>
						<div class="col-sm-6">
							<label class="required">Contact</label>
							<input type="text" name="contact" id="contact" placeholder="Enter contact here" class="mb-2 form-control" autocomplete="off">
							<div class="invalid-feedback text-left" id="err_contact"></div>
						</div>
						<div class="col-sm-12">
							<label class="required">Address</label>
							<textarea class="form-control mb-2" rows="2" id="address" name="address" placeholder="Enter address here"></textarea>
							<div class="invalid-feedback text-left" id="err_address"></div>
						</div>
						<div id="hide_on_edit" class="col-sm-12">
							<div class="col-sm-12">
								<label class="required">Email</label>
								<input type="text" name="email_address" id="email_address" class="form-control mb-2" placeholder="Enter email here">
								<div class="invalid-feedback text-left" id="err_email_address"></div>
							</div>
							<div class="col-sm-12">
								<label class="required">Password</label>
								<input type="password" name="password" id="password" class="form-control mb-2" placeholder="Enter password here">
								<div class="invalid-feedback text-left" id="err_password"></div>
							</div>
							<div class="col-sm-12">
								<label class="required">Confirm Password</label>
								<input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Verify password here">
								<div class="invalid-feedback text-left" id="err_confirm_password"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success px-5" id="btn_submit">Submit</button>
				</div>
			</div>
		</div>
	</div>
</form>
</div>
@endsection


@section('script')
<!-- Javascript Function-->
<script type="text/javascript">


	$('#refresh-filter').on('click', function(){
		$('#filter').val('');
		submit_filter();
	});

	function submit_filter(){
		show_user_accounts();
		$('#minimize_filter').trigger('click');
	}


	show_user_accounts();
	var tbl_user_accounts;
	function show_user_accounts(){
		if (tbl_user_accounts) {
			tbl_user_accounts.destroy();
		}
		tbl_user_accounts = $('#tbl_user_accounts').DataTable({
		pageLength: 10,
		responsive: true,
		deferRender: true,
		aaSorting: [],
		language: {
		"emptyTable": "No data available"
	},
	ajax: {
        url: "{{ route('admin.users.list') }}",
        data: {filter : $('#filter').val()},
    },
		columns: [{
		className: '',
		"data": "id",
		"title": "Fullname",
		"orderable": false,
		"render": function(data, type, row, meta){
		 return newdata = row.first_name+' '+row.last_name
		}
	},{
		className: '',
		"data": "contact",
		"title": "Contact #",
		"orderable": false,
	},{
		className: '',
		"data": "address",
		"title": "Address",
		"orderable": false,
	},{
		className: '',
		"data": "email_address",
		"title": "Email",
		"orderable": false,
	},{
		className: 'text-center',
		"data": "id",
		"title": "options",
		"orderable": false,
		"render": function(data, type, row, meta){
      newdata = '<button type="button" class="btn btn-sm btn-info" onclick="edit_user('+data+')"><i class="fa-solid fa-pen-to-square"></i> Edit</button>\
				 <button type="button" class="btn btn-sm btn-danger" onclick="delete_user('+data+')"><i class="fa-solid fa-trash-can"></i> Delete</button>'
		return newdata;
    }
	}
	]
	});
	}


	function edit_user(id){
		$.ajax({
            type:"get",
            url:"{{ route('admin.users.find') }}"+'/'+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
             if (response.status == true) {
             	$('#hide_on_edit').hide();
                $('#first_name').val(response.data.first_name);
                $('#middle_name').val(response.data.middle_name);
                $('#last_name').val(response.data.last_name);
                $('#gender').val(response.data.gender);
                $('#contact').val(response.data.contact);
                $('#address').val(response.data.address);
                $('#bday').val(response.data.bday);
                $('#id').val(response.data.id);
                $('.modal-title').html('Edit User');
                $('#modal_add_edit').modal('show');
                
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          });   
	}


	function delete_user(id){
		Swal.fire({
			  title: 'Are you sure?',
			  text: "This user will be deleted!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$.ajax({
		    type:"delete",
		    url:"{{ route('admin.users.destroy') }}"+'/'+id,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     
		       
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    Swal.fire(
			      'Deleted!',
			      'User has been deleted.',
			      'success'
			    )
			  }
			})

		
	}

	$("#all_data_form").on('submit', function(e){
		var mydata = $(this).serialize();
		let url = $(this).attr('action');
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"get",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit').prop('disabled', true);
					$('#btn_submit').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					Swal.fire({
		                position: 'center',
		                icon: 'success',
		                title: response.message,
		                showConfirmButton: false,
		                timer: 1500
		              })
					
					showValidator(response.error,'all_data_form');
					show_user_accounts();
					$('#modal_add_edit').modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'all_data_form');
				}
				$('#btn_submit').prop('disabled', false);
				$('#btn_submit').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function add_user_accounts(){
		$('#hide_on_edit').show();
		$('#all_data_form').trigger("reset");
		$('.modal-title').html('Add User');
		$('#modal_add_edit').modal('show');
	}

	
</script>
@endsection
