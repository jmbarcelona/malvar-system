<!doctype html>

    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Barangay Clearance pdf view</title>
    </head>
    <body>
        

    <style>
            h3 {
          font-size: 20px;
        }

        p {
          font-size: 14px;
        }
        u {
          font-size: 18px;
        }

        .square {
          height: 130px;
          width: 120px;
          border: solid SKYBLUE 2px;

        }
    </style>
    <center>
    @if(empty(getSystemDetails()->logo))
          <img src="{{ asset('img/default.png') }}" class="mt-4" style="width: 120px;"><br>
          @else
          <img src="{{ asset('storage/'.getSystemDetails()->logo)}}" class="mt-4" style="width: 120px;"><br>
          @endif
    </center>
    <center>
        <h3>OFFICE OF THE SANGGUNIANG BARANGAY</h3>
        <h3>B A R A N G A Y &nbsp;&nbsp; C L E A R A N C E</h3>
    </center>
    <div>
        <p>TO ALL OFFICERS OF THE LAW <span style="margin-left: 300px;">Date:<u> {{ $clearance->date }}</u></span></p>
        <p>And To Whom It May Concern</p>
    </div>
    <center>
        <p><b>This is to certify that</b></p>
                                            <!-- name data here -->                                                                     
        <p><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $clearance->first_name}} 
            @if(!empty($clearance->middle_name))
            {{substr($clearance->middle_name, 0, 1)}}.
            @endif
        {{$clearance->last_name }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></p>
        <p>of</p>
        <p>Purok <span><U>&nbsp;&nbsp; {{ $clearance->address }} &nbsp;&nbsp;</U></span>General Malvar, Santiago City</p>
        </center>
        <br>
        <div style="margin-left: 20px;">
                                                                                                                                                        <!-- date dataa here -->
        whose Community Tax Certificate number appears below, is a bonafide resident of this barangay <br>from <span><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $clearance->resident_from }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span> up to the present. Furthermore, upon verification of the records field <br>in this office, subject individual was found to have.
        </div>
        <center>
        <h3>NO DEROGATORY RECORDS</h3>
        <p>This being used for</p>
        <!-- purpose data -->
        <p><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $clearance->purpose }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></p>
        <p>(purpose)</p>
        <p>IMPORTANT REMINDERS</p>
        </center>
        <div style="width: 45%; float:left">
        <p>SPECIMEN SIGNATURE:</p>
        <P>__________________________</P>
        <P>__________________________</P>
        <P>Community Tax No.: <span>__________________________</span></P>
        <P>Issued at:<span style="margin-left: 52;">__________________________</span></P>
        <P>Issued on:<span style="margin-left: 50;">__________________________</span></P>
        <P>Ammount Paid:<span style="margin-left: 27;">__________________________</span></P>
        <P>Office Receipt No.:<span style="margin-left: 10;">__________________________</span></P>
        <P>Control No.:<span style="margin-left: 40;">__________________________</span></P>
        </div>

        <div style="width: 45%; float:right">
      <center>
        <br>
        <P>RIGHT THUMB</P>
        <div class="square" style="margin-left: 95px;"></div>
        <br>
        <p><B>HON. <span>{{  strtoupper($captain->first_name)}}</span></B></p>
        <p>Punong Barangay</p>
      </center>
        </div>
     </body>
    </html>