<!doctype html>

    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Barangay Indigency pdf view</title>
    </head>
    <body>
        

    <style>
            h3 {
          font-size: 20px;
        }

        p {
          font-size: 14px;
        }
        u {
          font-size: 18px;
        }

        .square {
          height: 130px;
          width: 120px;
          border: solid SKYBLUE 2px;

        }
    </style>
    <center>
    @if(empty(getSystemDetails()->logo))
          <img src="{{ asset('img/default.png') }}" class="mt-4" style="width: 120px;"><br>
          @else
          <img src="{{ asset('storage/'.getSystemDetails()->logo)}}" class="mt-4" style="width: 120px;"><br>
          @endif
    </center>
    <center>
        <h3>OFFICE OF THE SANGGUNIANG BARANGAY</h3>
        <br>
        <h3 style="color: red"><u>CERTIFICATE OF INDIGENCY</u></h3>
    </center>
    <br>
    <div>
        <p>TO WHOM IT MAY CONCERN</p>
    </div>
    <br>
        <P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B>&nbsp;&nbsp;&nbsp;THIS IS TO CERTIFY </B>that <span><u>&nbsp;&nbsp;&nbsp; 
            {{ $indigency->first_name}}
            @if(!empty($indigency->middle_name))
            {{substr($indigency->middle_name, 0, 1)}}.
            
            @endif
            {{$indigency->last_name }}





             &nbsp;&nbsp;&nbsp;</u></span>, <span><u>&nbsp;&nbsp;{{ $indigency->age }}&nbsp;&nbsp;</u></span> years old, Filipino and bonafide resident of purok 
        <span><u>&nbsp;&nbsp;{{ $indigency->address }}&nbsp;&nbsp;</u></span> Barangay General Malvar, Santiago City, belongs to a family with low income and considered as indigent. Any assistance extended to him/her will be great help to the present situation of his/her family.</P>
        <br>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CERTIFIED FURTHER that he/she is of good moral character and has no negative record field in this office as of this date.</p>
        <br>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This certification is issued to the above-mentioned person upon his/her request for whatever legal purpose it may serve.</p>

        <p><hr></p>
        <center>
        <p>Issued this <span><u>&nbsp;&nbsp;&nbsp;&nbsp;{{ $indigency->date }}&nbsp;&nbsp;&nbsp;&nbsp;</u></span> in Barangay general Malvar, City of Santiago.</p>
        </center>

        <div style="width: 45%; float:left">
       <br>
        <br><br>
        <br><br>
        <P>CTC No.: <span style="margin-left: 30;">__________________________</span></P>
        <P>Issued on:<span style="margin-left: 30;">__________________________</span></P>
        <P>Issued at:<span style="margin-left: 32;">__________________________</span></P>
        <P>Control No.:<span style="margin-left: 20;">__________________________</span></P>
        </div>

        <div style="width: 45%; float:right">
            <br><br>
        <br><br>
        <br><br>
        <br><br><br>
      <center>
        <br>
        <br><br>
        <p>______________________________________</p>
        <p><B>HON. <span>{{  strtoupper($captain->first_name)}}</span></B></p>
        <p>Punong Barangay</p>
      </center>
        </div>
     </body>
    </html>