@extends('Layout.app_user')
@section('title', 'Profile')
@section('css')
<style type="text/css">
.form-control:focus {
box-shadow: none;
border-color: #BA68C8
}
.profile-button {
background: rgb(99, 39, 120);
box-shadow: none;
border: none
}
.profile-button:hover {
background: #682773
}
.profile-button:focus {
background: #682773;
box-shadow: none
}
.profile-button:active {
background: #682773;
box-shadow: none
}
.back:hover {
color: #682773;
cursor: pointer
}
.labels {
font-size: 11px
}
.add-experience:hover {
background: #BA68C8;
color: #fff;
cursor: pointer;
border: solid 1px #BA68C8
}
.container-fluid{
height: 100vh;
}
</style>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-5">
      <div class="d-flex flex-column align-items-center text-center p-3 py-5">
        @if(!empty(Auth::user()->user_img))
        <img class="rounded-circle mt-5 img-thumbnail" id="img_shower" src="{{ asset('storage/'.Auth::user()->user_img) }}" style="width: 260px; height:260px;">
        @else
        <img class="rounded-circle mt-5 img-thumbnail" id="img_shower" src="{{ asset('img/default.png') }}" style="width: 260px; height:260px; ">
        @endif
        <div class="col-sm-8 mt-2 img_buttons">
          <form id="upload_img_form" action="{{ route('user.profile.upload') }}">
            <button type="button" class="btn btn-sm btn-info" onclick="upload_img_btn()">Browse</button>
            <button type="submit" class="btn btn-sm btn-success">Save</button>
          </div>
          <input type="file" class="form-control" name="img" id="img" hidden="" />
          
        </form>
        <span class="font-weight-bold">
          {{ Auth::user()->first_name.' '.Auth::user()->middle_name.' '.Auth::user()->last_name }}
        </span>
        <span>
          {{ Auth::user()->email_address }}
        </span>
        <span>
          <button type="button" class="btn btn-success col-sm-12" onclick="change_password_btn('{{ Auth::user()->id }}')">Change Password</button>
        </span>
      </div>
    </div>
    <div class="col-md-5">
      <div class="p-3 py-5">
        <div class="d-flex justify-content-between align-items-center mb-3">
          <h4 class="text-right">Profile Settings</h4>
        </div>
        <div class="row mt-2">
          <div class="col-md-12"><label class="labels">Address</label><input type="text" class="form-control bg-light" placeholder="first name" disabled="" value="{{ Auth::user()->address }}"></div>
          <div class="col-md-12"><label class="labels">Gender</label><input type="text" class="form-control bg-light" placeholder="first name" disabled="" value="{{ Auth::user()->gender }}"></div>
          <div class="col-md-12"><label class="labels">Date of Birth</label><input type="text" class="form-control bg-light" disabled="" value="{{date('F j, Y', strtotime(Auth::user()->bday))}}"></div>
        </div>
        <div class="row">
          <div class="col-md-12"><label class="labels">Mobile Number</label><input type="text" class="form-control bg-light" placeholder="enter phone number" disabled="" value="{{ Auth::user()->contact }}"></div>
          <div class="col-md-12"><label class="labels">Email</label><input type="text" class="form-control bg-light" placeholder="enter email id" disabled="" value="{{ Auth::user()->email_address }}"></div>
          <div class="col-md-12 mt-3">
            <button type="button" class="btn btn-info col-sm-12" onclick="edit_modal_btn('{{ Auth::user()->id }}')"><strong>Edit Profile</strong></button>
          </div>
        </div>
        
      </div>
    </div>
    
  </div>
</div>
</div>
</div>
<div class="modal fade" role="dialog" id="edit_modal">
<div class="modal-dialog modal-lg">
<div class="modal-content">
  <div class="modal-header">
    <div class="modal-title h4">
      Edit Profile
    </div>
    <button class="close" data-dismiss="modal">&times;</button>
  </div>
  <div class="modal-body">
    <form action="{{ route('user.profile.edit') }}" id="edit_profile_form" class="needs-validation">
      <div class="row">
        <div class="col-sm-4">
          <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">
          <label>Firstname</label>
          <input type="text" name="first_name" id="first_name" placeholder="Enter firstname here" class="mb-2 form-control" autocomplete="off">
          <div class="invalid-feedback" id="err_first_name"></div>
        </div>
        <div class="col-sm-4">
          <label>Middlename</label>
          <input type="text" name="middle_name" id="middle_name" placeholder="Enter middlename here" class="mb-2 form-control" autocomplete="off">
          <div class="invalid-feedback" id="err_middle_name"></div>
        </div>
        <div class="col-sm-4">
          <label class="required">Lastname</label>
          <input type="text" name="last_name" id="last_name" placeholder="Enter lastname here" class="mb-2 form-control" autocomplete="off">
          <div class="invalid-feedback" id="err_last_name"></div>
        </div>
        <div class="col-sm-12">
          <label class="required">Date of Birth</label>
          <input type="date" name="bday" id="bday" class="mb-2 form-control" autocomplete="off">
          <div class="invalid-feedback" id="err_bday"></div>
        </div>
        <div class="col-sm-6">
          <label class="required">Gender</label>
          <select name="gender" id="gender" class="form-control" autocomplete="off">
            <option selected="" disabled="">Please select gender</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
          <div class="invalid-feedback" id="err_gender"></div>
        </div>
        <div class="col-sm-6">
          <label class="required">Contact</label>
          <input type="text" name="contact" id="contact" placeholder="Enter contact here" class="mb-2 form-control" autocomplete="off">
          <div class="invalid-feedback" id="err_contact"></div>
        </div>
        <div class="col-sm-12">
          <label class="required">Address</label>
          <textarea class="form-control mb-2" rows="1" id="address" name="address" placeholder="Enter address here"></textarea>
          <div class="invalid-feedback" id="err_address"></div>
        </div>
        <div class="col-sm-12">
          <label class="required">Email</label>
          <input type="text" name="email_address" id="email_address" class="form-control mb-2" placeholder="Enter email here">
          <div class="invalid-feedback" id="err_email_address"></div>
        </div>
        
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn-md btn btn-danger" data-dismiss="modal">Cancel</button>
      <button type="submit" class="btn-md btn btn-success" id="btn_submit1">Submit</button>
    </div>
  </form>
</div>
</div>
</div>
<div class="modal fade" role="dialog" id="change_password_modal">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <div class="modal-title h4">
      Change Password
    </div>
    <button class="close" data-dismiss="modal">&times;</button>
  </div>
  <form action="{{ route('user.profile.change.password') }}" id="change_password_form" class="needs-validation">
    <div class="modal-body">
      <div class="input-group mb-3">
        <input type="hidden" name="user_id2" id="user_id2">
        <input type="password" id="old_pass" name="old_pass" class="form-control" placeholder="Enter old password " aria-label="Amount (to the nearest dollar)">
        <div class="input-group-append">
          <span class="input-group-text " style="cursor: pointer" onclick=" password_visibile('eye1', 'old_pass');"><i class="fa-solid fa-eye" id="eye1"></i></span>
        </div>
        <div class="invalid-feedback text-left" id="err_old_pass"></div>
      </div>
      <div class="input-group mb-3">
        <input type="password" id="new_pass" name="new_pass" class="form-control" placeholder="Enter new password " aria-label="Amount (to the nearest dollar)">
        <div class="input-group-append">
          <span class="input-group-text " style="cursor: pointer" onclick=" password_visibile('eye2', 'new_pass');"><i class="fa-solid fa-eye" id="eye2"></i></span>
        </div>
        <div class="invalid-feedback text-left" id="err_new_pass"></div>
      </div>
      <div class="input-group mb-3">
        <div class="input-group-prepend">
        </div>
        <input type="password" id="confirm_pass" name="confirm_pass" class="form-control" placeholder="Confirm password " aria-label="Amount (to the nearest dollar)">
        <div class="input-group-append">
          <span class="input-group-text " style="cursor: pointer" onclick=" password_visibile('eye3', 'confirm_pass');"><i class="fa-solid fa-eye" id="eye3"></i></span>
        </div>
        <div class="invalid-feedback text-left" id="err_confirm_pass"></div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn-md btn btn-danger" data-dismiss="modal">Cancel</button>
      <button type="submit" class="btn-md btn btn-success" id="btn_submit">Submit</button>
    </div>
  </form>
</div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">

$('#upload_img_form').on('submit', function(e){
e.preventDefault();
let form = $("#upload_img_form")[0];
    // Create an FormData object 
  var data = new FormData(form);
  let url = $(this).attr('action');
  $.ajax({
        type:"post",
        url:url,
        data:data,
        enctype: 'multipart/form-data',
        processData: false,  // Important!
        contentType: false,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 1500,
              })
            setTimeout(function(){
                   location.reload();
                }, 1000);
            showValidator(response.error,'change_password_form');
            $('#change_password_modal').modal('hide');
         }else{
          if (response.message) {
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: response.message,
                showConfirmButton: false,
                timer: 1500,
              })
          }
            
          console.log(response);
         }
         showValidator(response.error,'change_password_form');
         
        },
        error: function(error){
          console.log(error);
        }
      });
});


function upload_img_btn(){
  $('#img').trigger('click');
}


$('#change_password_form').on('submit', function(e){
    e.preventDefault();
    let url = $(this).attr('action');
    let formData = $(this).serialize();
    $.ajax({
        type:"get",
        url:url,
        data:formData,
        dataType:'json',
        beforeSend:function(){
            $('#btn_submit').prop('disabled', true);
            $('#btn_submit').text('Please wait...');
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.message,
                showConfirmButton: false,
                timer: 1500,
              })

            showValidator(response.error,'change_password_form');
            $('#change_password_modal').modal('hide');
         }else{
          if (response.message) {
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: response.message,
                showConfirmButton: false,
                timer: 1500,
              })
          }
            
          console.log(response);
         }
         showValidator(response.error,'change_password_form');
         $('#btn_submit').prop('disabled', false);
         $('#btn_submit').text('Save');
        },
        error: function(error){
          console.log(error);
        }
      });
    });

$('#edit_profile_form').on('submit', function(e){
    e.preventDefault();
    let formData = $(this).serialize();
    let url = $(this).attr('action');
    $.ajax({
        type:"POST",
        url:url,
        data:formData,
        dataType:'json',
        beforeSend:function(){
            $('#btn_submit1').prop('disabled', true);
            $('#btn_submit1').text('Please wait...');
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Profile Updated Successfully!',
                showConfirmButton: false,
                timer: 1500,
              }) 
                setTimeout(function(){
                   location.reload();
                }, 1000);
                
                showValidator(response.error,'edit_profile_form');
                $('#edit_modal').modal('hide');
         }else{
            showValidator(response.error,'edit_profile_form');
            console.log(response);
         }
         $('#btn_submit1').prop('disabled', false);
         $('#btn_submit1').text('Save');
        },
        error: function(error){
          console.log(error);
          Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Something went wrong!',
                showConfirmButton: false,
                timer: 1500,
              }) 
        }
      });
});

    function password_visibile(icon, input){
        $("#"+icon).toggleClass('fa-eye fa-eye-slash');
        $("#"+input).attr('type', function(index, attr){
        return (attr == 'text')? 'password' : 'text';
        });
    }

    function edit_modal_btn(id){
        $.ajax({
            type:"get",
            url:"{{ route('user.profile.find') }}"+'/'+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
             if (response.status == true) {
                $('#first_name').val(response.data.first_name);
                $('#middle_name').val(response.data.middle_name);
                $('#last_name').val(response.data.last_name);
                $('#gender').val(response.data.gender);
                $('#bday').val(response.data.bday);
                $('#contact').val(response.data.contact);
                $('#email_address').val(response.data.email_address);
                $('#address').val(response.data.address);
                $('#edit_modal').modal('show');
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          });   
    }

    function change_password_btn(id){
        $('#change_password_modal').modal('show');
        $('#change_password_form')[0].reset();
        $('#user_id2').val(id);
    }
</script>
    <script>
      var selDiv = "";
      var storedFiles = [];
      $(document).ready(function () {
        $("#img").on("change", handleFileSelect);
        selDiv = $("#img_shower");
      });

      function handleFileSelect(e) {
        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        filesArr.forEach(function (f) {
          if (!f.type.match("image.*")) {
            return;
          }
          storedFiles.push(f);

          var reader = new FileReader();
          reader.onload = function (e) {
             $("#img_shower").attr('src', e.target.result);
             $("#img_shower").attr('data-file', f.name);
          };
          reader.readAsDataURL(f);
        });
      }
    </script>
@endsection
