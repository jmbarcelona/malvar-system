@extends('Layout.app_user')
@section('title', 'Gymnasium Request')
@section('content')
<!-- Content Wrapper. Contains page content -->
<!-- Content Header (Page header) -->
<div class="content-header" style="border-bottom: solid lightgrey 1px">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Gymnasium Request</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Home</a></li>
            <li class="breadcrumb-item active">Gymnasium Request</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>

        <div class="table-responsive col-sm-12">
          <div class="text-right mb-3 mt-3">
            <button class="btn btn-danger btn-sm col-sm-1" onclick="back_button()"> <strong><i class="fa-solid fa-caret-left"></i> &nbsp;Back</strong></button>
          </div>
          <table class="table table-bordered table-hove" style="width: 100%;">
            <thead>
              <tr>
                <th>ID</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>Rent Fee</th>
                <th>Date</th>
                <th class="text-center">Status</th>
              </tr>
            </thead>
            <tbody>
              @foreach($gymRecord as $gymRecord)
              <tr>
                <td>{{ $gymRecord->users->id }}</td>
                <td>{{ $gymRecord->start_time.':00 PM' }}</td>
                <td>{{ $gymRecord->end_time.':00 PM' }}</td>
                <td>{{ '₱'.$gymRecord->amount}}</td>
                <td>{{ date('F j, Y', strtotime($gymRecord->date)) }}</td>

                @if($gymRecord->deleted_at)
                <td class="text-center"><span class="badge badge-danger p-2">Disapproved</span></td>
                @elseif($gymRecord->status == 1)
                <td class="text-center"><span class="badge badge-warning p-2">Pending</span></td>
                @elseif($gymRecord->status == 2)
                <td class="text-center"><span class="badge badge-success p-2">Approved</span></td>
                @endif
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>

       
@endsection
@section('script')
<script type="text/javascript">
function view_details(id){
  $.ajax({
            type:"get",
            url:"{{ route('user.barangay_clearance.find') }}"+'/'+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
             if (response.status == true) {
              $('#ctc_no1').val(response.data.ctc_no);
              $('#first_name1').val(response.data.first_name);
              $('#middle_name1').val(response.data.middle_name);
              $('#last_name1').val(response.data.last_name);
              $('#age1').val(response.data.age);
              $('#civil_status1').val(response.data.civil_status);
              $('#date1').val(response.data.date);
              $('#address1').val(response.data.address);
              $('#purpose1').val(response.data.purpose);
              $('#modal_view').modal('show');
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          }); 
}

function back_button(){
  window.history.go(-1);
}
</script>
@endsection

