@extends('Layout.app_user')
@section('title', 'Gymnasium Reservation')
@section('content')

<!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Gymnasium Reservation</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">

              <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Home</a></li>
              <li class="breadcrumb-item active">Gymnasium Reservation</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
          <!-- /.content-header -->
      <div class="container-fluid">

         <div class="card">
        <div class="card-header"><strong>Today's reseved schedule</strong></div>
        <div class="card-body">
          <div class="table-responsive">
          <table class="table table-bordered table-hove" id="" style="width: 100%;">
            <thead>
                
                  <tr>
                    <th>ID</th>
                    <th>Time</th>
                    <th>Date</th>
                  </tr>
              </thead>

              <tbody>
                @foreach($gym_today as $gym_today)
                <tr>
                  <td>{{ $gym_today->users->id }}</td>
                  <td>{{ $gym_today->start_time.':00 PM' .' - '.$gym_today->end_time.':00 PM'}}</td>
                  <td>{{date('F j, Y', strtotime($gym_today->date))}}</td>
                </tr>
                @endforeach
              </tbody>
          </table>
        </div>
      </div>
    </div>

        <div class="row justify-content-center">
          <div class="col-sm-12 col-md-12">
          <form action="{{ route('user.Gym_scheduling.store') }}" class="needs-validation" id="add_edit_form">
            <div class="card">
              <div class="card-header h3">
                <a href="{{ route('user.Gym_scheduling.request_view') }}" class="btn btn-info text-center px-4"><i class="fa-solid fa-eye"></i>&nbsp; View your request</a>
              </div>
              <div class="card-body">
                <div class="row">
              <div class="col-sm-12">
              <input type="hidden" name="id" id="id">
              <label class="required">Start time</label><br>
              <select name="start_time" id="start_time"  class="mb-2 form-control" autocomplete="off" onchange="time_to_validate(this.value)">
                <option value="" selected="" disabled="">Select start time</option>
                <option value="6" id="6pm1">6:00 PM</option>
                <option value="7" id="7pm1">7:00 PM</option>
                <option value="8" id="8pm1">8:00 PM</option>
                <option value="9" id="9pm1">9:00 PM</option>
              </select>
              <div class="invalid-feedback text-left" id="err_start_time"></div>
            </div>
            <div class="col-sm-12">
              <label class="required">End time</label><br>
              <select name="end_time" id="end_time"  class="mb-2 form-control" autocomplete="off" onchange="amount_to_paid(this.value)">
                <option value="" selected="" disabled="">Select end time</option>
                <option value="7" id="7pm">7:00 PM</option>
                <option value="8" id="8pm">8:00 PM</option>
                <option value="9" id="9pm">9:00 PM</option>
                <option value="10" id="10pm">10:00 PM</option>
              </select>
              <div class="invalid-feedback text-left" id="err_end_time"></div>
            </div>

            <div class="col-sm-12 mb-2">
              <label class="required">Date</label><br>
              <input type="date" name="date" id="date" class="form-control">
              <div class="invalid-feedback text-left" id="err_date"></div>
            </div>

            <div class="col-sm-12 mb-2">
              <label class="required">Reason</label><br>
              <textarea name="reason" id="reason" class="form-control" rows="2" placeholder="Enter reason"></textarea>
              <div class="invalid-feedback text-left" id="err_reason"></div>
            </div>

            <div class="col-sm-4">
              <label class="required">Amount to paid</label><br>
              <input type="text" name="to_paid" id="to_paid" class="form-control bg-light" disabled="">
            </div>

                </div>
              </div>
              <div class="card-footer text-right">
                <button type="button" class="btn btn-danger">Cancel</button>
                <button type="submit" class="btn btn-success" id="btn_submit">Submit Request</button>
              </div>
            </div>
          </form>
          </div>
        </div>

        </div>


@endsection
@section('script')
<script type="text/javascript">

  function amount_to_paid(_this){
    if (_this == 7) {
      $('#6pm1').removeClass("d-none", "d-none");
      $('#7pm1').addClass("d-none", "d-none");
      $('#8pm1').addClass("d-none", "d-none");
      $('#9pm1').addClass("d-none", "d-none");
    }else if(_this == 8) {
      $('#6pm1').removeClass("d-none", "d-none");
      $('#7pm1').removeClass("d-none", "d-none");
      $('#8pm1').addClass("d-none", "d-none");
      $('#9pm1').addClass("d-none", "d-none");
    }else if(_this == 9){
      $('#6pm1').removeClass("d-none", "d-none");
      $('#7pm1').removeClass("d-none", "d-none");
      $('#9pm1').addClass("d-none", "d-none");
      $('#8pm1').removeClass("d-none", "d-none");
    }else{
      $('#6pm1').removeClass("d-none", "d-none");
      $('#7pm1').removeClass("d-none", "d-none");
      $('#9pm1').removeClass("d-none", "d-none");
      $('#8pm1').removeClass("d-none", "d-none");
    }
    
    var total = $('#end_time').val() - $('#start_time').val()

    $('#to_paid').val("₱"+total+","+"000")    
    
  }



  function time_to_validate(_this){
    $('#end_time').removeAttr('disabled', 'disabled');

    if ($('#end_time').val() == 10  ) {
      $('#end_time').val();
    }else if($('#end_time').val() <= $('#start_time').val()){
      $('#end_time').val('');
      $('#to_paid').val("");
    }

    if (_this == 7) {
    $('#7pm').addClass("d-none", "d-none");

    $('#8pm').removeClass("d-none", "d-none");
    $('#9pm').removeClass("d-none", "d-none");
    }else if(_this == 8) {

    $('#7pm').addClass("d-none", "d-none");
    $('#8pm').addClass("d-none", "d-none");

    $('#9pm').removeClass("d-none", "d-none");
    }else if(_this == 9) {

    $('#7pm').addClass("d-none", "d-none");
    $('#8pm').addClass("d-none", "d-none");
    $('#9pm').addClass("d-none", "d-none");

    }else if (_this == 6){
    $('#7pm').removeClass("d-none", "d-none");
    $('#8pm').removeClass("d-none", "d-none");
    $('#9pm').removeClass("d-none", "d-none");
    }else{
      $('#end_time').attr('disabled', 'disabled');
    }


    var total = $('#end_time').val() - $('#start_time').val()

    if (total >= 0) {
      $('#to_paid').val("₱"+total+","+"000")
    }
    


  }


  $("#add_edit_form").on('submit', function(e){
    var mydata = $(this).serialize();
    let url = $(this).attr('action');
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"post",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
          $('#btn_submit').prop('disabled', true);
          $('#btn_submit').text('Please wait...');
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          setTimeout(function(){
            location.reload();
          }, 1500);
          Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Request submitted Successfully!',
                    showConfirmButton: false,
                    timer: 1500
                  })
          
          $('#add_edit_form')[0].reset();
          showValidator(response.error,'add_edit_form');
          show_user_accounts();
          $('#modal_add_edit').modal('hide');
        }else{
          if (response.message) {
            Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 5500
                  })
          }
          
          //<!-- your error message or action here! -->
          showValidator(response.error,'add_edit_form');
        }
        $('#btn_submit').prop('disabled', false);
        $('#btn_submit').text('Save');
      },
      error:function(error){
        console.log(error)
      }
    });
  });

  function add_user_accounts(){
    $('#add_edit_form').trigger("reset");
    $('.modal-title').html('Add Contact');
    $('#modal_add_edit').modal('show');
  }

  

</script>
@endsection

