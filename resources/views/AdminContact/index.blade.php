@extends('Layout.app_admin')
@section('title', 'Contacts')
@section('css')
<style type="text/css">
	.dataTables_filter { display: none; }

	
</style>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row pt-3">
		<div class="col-lg-12 col-md-12 col-12">
			<div class="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
				<div class="mb-3 mb-md-0">
					<h1 class="mb-1 h2 fw-bold">Contacts</h1>
				</div>
				<div class="d-flex">
					<button class="btn btn-info btn-shadow" onclick="add_user_accounts();"><i class="fa-solid fa-address-book"></i>&nbsp;Add Contact</button>
				</div>
			</div>
		</div>
	</div>
	<form class="needs-validation" id="contact_add_edit_form" action="{{ route('admin.contact.store') }}" novalidate>
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Record Filter</h5>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse">
						<i class="fas fa-minus"></i>
						</button>
					</div>
				</div>
				
				<div class="card-body">
					<div class="row">
						<div class="position-relative mb-2 col-md-4">
							<label>Contact name</label>
							<input type="text" name="filter" id="filter" class="form-control" placeholder="Search contact name here">
							
						</div>
					</div>
					
				</div>
				<div class="card-footer text-right">
					<!-- <button type="button" class="btn btn-secondary" data-card-widget="collapse">
					<i class="fa fa-minus"></i> Minimize Filter
					</button> -->
					<button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
					<button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
				</div>
				
			</div>
			<div class="card">
				<div class="card-body">
					<div class="table-responsive">
					<table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;"></table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" role="dialog" id="modal_add_edit">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title h4">
						Add Contact
					</div>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<input type="hidden" name="id" id="id">
							<label class="required">Contact Name</label><br>
							<input type="text" name="contact_name" id="contact_name" placeholder="Enter equipment name" class="mb-2 form-control" autocomplete="off">
							<div class="invalid-feedback text-left" id="err_contact_name"></div>
						</div>
						<div class="col-sm-12">
							<label class="required">Contact Number</label><br>
							<input type="text" name="contact_number" id="contact_number" placeholder="Enter contact number" class="mb-2 form-control" autocomplete="off">
							<div class="invalid-feedback text-left" id="err_contact_number"></div>
						</div>
						<div class="col-sm-12 mt-2 text-center">
				      <img src="{{ asset('img/default.png') }}" class="mt-4" style="width: 150px; height: 150px" id="img_shower"><br>
						</div>
						<div class="col-sm-12 mt-2 text-center">
							<input type="file" name="img" id="img" placeholder="Enter contact number" class="mb-2" autocomplete="off" hidden>
							<button type="button" class="btn btn-info col-sm-12" onclick='$("#img").trigger("click")'>+ Upload Image</button>
						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success px-5" id="btn_submit">Submit</button>
				</div>
			</div>
		</div>
	</div>
</form>
</div>
@endsection


@section('script')
<!-- Javascript Function-->
<script type="text/javascript">


	$('#refresh-filter').on('click', function(){
		$('#filter').val('');
		submit_filter();
	});

	function submit_filter(){
		show_user_accounts();
	}


	show_user_accounts();
	var tbl_user_accounts;
	function show_user_accounts(){
		if (tbl_user_accounts) {
			tbl_user_accounts.destroy();
		}
		tbl_user_accounts = $('#tbl_user_accounts').DataTable({
			"aoColumnDefs": [
      { "sClass": "my_class", "aTargets": [ 0 ] }
    ],
		pageLength: 10,
		responsive: true,
		deferRender: true,
		aaSorting: [],
		language: {
		"emptyTable": "No data available",

	},
	ajax: {
        url: "{{ route('admin.contact.list') }}",
        data: {filter : $('#filter').val()},
    },
		columns: [{
		className: '',
		"data": "img",
		"title": "",
		"orderable": false,
		"width": 1,
		"render": function(data, type, row, meta){
			if (data) {
				newdata = '<img src="http://localhost/malvar-system/public/storage/'+data+'" class="" style="width: 60px; height: 60px">'
			}else{
				newdata = '<img src="{{ asset('img/default.png') }}" class="" style="width: 60px; height: 60px">'
			}
      
		return newdata;
    }
	},{
		className: '',
		"data": "c_name",
		"title": "Contact name",
		"orderable": false,
	},{
		className: '',
		"data": "c_no.",
		"title": "Contact #",
		"orderable": false,
	},{
		className: 'text-center',
		"data": "id",
		"title": "Options",
		"orderable": false,
		"render": function(data, type, row, meta){
      newdata = '<button type="button" class="btn btn-sm btn-info" onclick="edit_s_equipment('+data+')"><i class="fa-solid fa-pen-to-square"></i> Edit</button>\
				 <button type="button" class="btn btn-sm btn-danger" onclick="delete_s_equipment('+data+')"><i class="fa-solid fa-trash-can"></i> Delete</button>'
		return newdata;
    }
	}
	]
	});
	}


	function edit_s_equipment(id){
		$.ajax({
            type:"get",
            url:"{{ route('admin.contact.find') }}"+'/'+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
             if (response.status == true) {
                $('#contact_number').val(response.data.c_no);
                $('#contact_name').val(response.data.c_name);
                $('#id').val(response.data.id);
                if (response.data.img) {
                $("#img_shower").attr("src", "http://localhost/malvar-system/public/storage/"+response.data.img+"")
                }
                $('.modal-title').html('Edit Contact')
                $('#modal_add_edit').modal('show');
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          });   
	}


	function delete_s_equipment(id){
		Swal.fire({
			  title: 'Are you sure?',
			  text: "You won't be able to revert this!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$.ajax({
		    type:"delete",
		    url:"{{ route('admin.contact.destroy') }}"+'/'+id,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     
		       
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    Swal.fire(
			      'Deleted!',
			      'Your record has been deleted.',
			      'success'
			    )
			  }
			})

		
	}

	$("#contact_add_edit_form").on('submit', function(e){
		e.preventDefault(e);
		let form = $("#contact_add_edit_form")[0];
    // Create an FormData object 
  	var data = new FormData(form);
		let url = $(this).attr('action');
		$.ajax({
			type:"post",
			url:url,
			data:data,
			cache:false,
			enctype: 'multipart/form-data',
      processData: false,  // Important!
      contentType: false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit').prop('disabled', true);
					$('#btn_submit').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					Swal.fire({
		                position: 'center',
		                icon: 'success',
		                title: 'Contact Inserted Successfully!',
		                showConfirmButton: false,
		                timer: 1500
		              })
					
					showValidator(response.error,'contact_add_edit_form');
					show_user_accounts();
					$('#modal_add_edit').modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'contact_add_edit_form');
				}
				$('#btn_submit').prop('disabled', false);
				$('#btn_submit').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_user_accounts(_this){
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this user_accounts?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"delete",
				url:"",
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					
					show_user_accounts();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function add_user_accounts(){
		$('#contact_add_edit_form').trigger("reset");
		$('.modal-title').html('Add Contact');
		$('#modal_add_edit').modal('show');
		$('#img').val('');
		$("#img_shower").attr("src","{{ asset('img/default.png') }}" )
	}

	
</script>

<script>
      var selDiv = "";
      var storedFiles = [];
      $(document).ready(function () {
        $("#img").on("change", handleFileSelect);
        selDiv = $("#img_shower");
      });

      function handleFileSelect(e) {
        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        filesArr.forEach(function (f) {
          if (!f.type.match("image.*")) {
            return;
          }
          storedFiles.push(f);

          var reader = new FileReader();
          reader.onload = function (e) {
             $("#img_shower").attr('src', e.target.result);
             $("#img_shower").attr('data-file', f.name);
          };
          reader.readAsDataURL(f);
        });
      }
    </script>
@endsection
