@extends('Layout.app_admin')
@section('title', 'Barangay Indigency Reports')
@section('css')
<style type="text/css">
	
</style>
@endsection
@section('content')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Barangay Indigency Reports</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Barangay Indigency Reports</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>

	<form class="needs-validation" id="add_edit_form" action="{{ route('admin.barangay_indigency.store') }}" novalidate>
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Record Filter</h5>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse">
						<i class="fas fa-minus"></i>
						</button>
					</div>
				</div>
				
				<div class="card-body">
					<div class="row">
						<div class="position-relative mb-2 col-md-4">
							<label>Date From</label>
							<input type="date" name="filter1" id="filter1" class="form-control" placeholder="Search equipment here" value="2023-15-02">
						</div>

						<div class="mb-2 col-md-4">
							<label>Date To</label>
							<input type="date" name="filter2" id="filter2" class="form-control" placeholder="Search equipment here">
						</div>
					</div>
					
				</div>
				<div class="card-footer text-right">
					<!-- <button type="button" class="btn btn-secondary" data-card-widget="collapse">
					<i class="fa fa-minus"></i> Minimize Filter
					</button> -->
					<button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
					<button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
				</div>
				
			</div>
			<div class="card">
				<div class="card-body">
					<div class="table-responsive">
					<table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;"></table>
				</div>
			</div>
		</div>
	</div>
</form>




@endsection


@section('script')
<!-- Javascript Function-->
<script type="text/javascript">


	$('#refresh-filter').on('click', function(){
		$('#filter1').val('');
		$('#filter2').val('');
		submit_filter();
	});

	function submit_filter(){
		show_user_accounts();
	}


	show_user_accounts();
	var tbl_user_accounts;
	function show_user_accounts(){
		if (tbl_user_accounts) {
			tbl_user_accounts.destroy();
		}
		tbl_user_accounts = $('#tbl_user_accounts').DataTable({
		pageLength: 10,
		responsive: true,
		deferRender: true,
		aaSorting: [],
		language: {
		"emptyTable": "No data available",
		searchPlaceholder: "Requestor name",

	},
	ajax: {
        url: "{{ route('admin.barangay_indigency.report.list') }}",
        data: {filter1 : $('#filter1').val(), filter2 : $('#filter2').val()},
    },
		columns: [{
		className: 'text-center',
		"data": "id",
		"title": "Print PDF",
		"orderable": false,
		"render": function(data, type, row, meta){
      	return newdata =	'<a href="{{ route('admin.download.indigency') }}/'+data+'" target="_blank" class="btn btn-sm btn-danger text-sm" style="width: 100px"><i class="fas fa-file-pdf"></i> Print</a>'

				
    }
	},{
		className: '',
		"data": "id",
		"title": "Requestor's Name",
		"orderable": false,
		"render": function(data, type, row, meta){
      newdata = row.first_name +' '+ row.last_name
		return newdata;
    }
	},{
		className: '',
		"data": "date.",
		"title": "Date",
		"orderable": false,
	},{
		className: 'text-center',
		"data": "status.",
		"title": "status",
		"orderable": false,
		"render": function(data, type, row, meta){
			if (data == 4) {
			   newdata = '<span class="badge badge-sm badge-warning text-sm" style="width: 100px">Pending</span>'
			}else{
      		   newdata = '<span class="badge badge-sm badge-success text-sm" style="width: 100px">Done</span>'
			}

		return newdata;
    }
	}
	]
	});
	}

</script>
@endsection
