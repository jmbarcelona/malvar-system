@extends('Layout.app_admin')
@section('title', 'Sports Equipments Logs')
@section('css')
<style type="text/css">
	/*.dataTables_filter { display: none; }*/

	.balance-div{
  bottom: 0.25rem;
  position: fixed;
  right: 0.5rem;
  z-index: 1032;
  background-color: grey 0.1;
  border-radius: 5px
}


</style>
@endsection
@section('content')
<div class="container-fluid">
<div class="content-header">
  <div class="container-fluid" style="border-bottom: solid lightgrey 1px">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Sports Equipment Logs</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Sports Equipment Logs</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>


<form class="needs-validation" id="spots_equipment_form" action="{{ route('admin.barangay.store') }}" novalidate>	
<div class="col-12">
            <div class="card">
              <div class="card-header" onclick="$('#minimize_filter').click()">
                <h5 class="card-title">Record Filter</h5>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
             
              <div class="card-body">

                  <div class="row">

                  		
						
                        <div class="position-relative mb-2 col-md-4">
							<label>Date From</label>
							<input type="date" name="filter1" id="filter1" class="form-control">
						</div>

						<div class="position-relative mb-2 col-md-4">
							<label>Date To</label>
							<input type="date" name="filter2" id="filter2" class="form-control">
						</div>

						

                  </div>
      
              </div>

              <div class="card-footer text-right">
                <!-- <button type="button" class="btn btn-secondary" data-card-widget="collapse" id="minimize_filter">
                  <i class="fa fa-minus"></i> Minimize Filter
                </button> -->
                <button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
                <button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
              </div>
           
            </div>

          



<div class="card">
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;"></table>
		</div>
	</div>
</div>
</div>
<!-- 

<div class="card-footer text-right balance-div">
<button type="button" class="btn btn-info">
Main Report Ending Balance: <span class="badge badge-light ending-balance">₱24 pcs</span>
</button>
</div>

 -->



 <div class="modal fade" role="dialog" id="modal_add_edit">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title h4">
            Add barangay Equipment
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="row">
          <div class="col-sm-12">
          	<input type="hidden" name="id" id="id">
            <label class="required">Equipment Name</label><br>
            <input type="text" name="equipment_name" id="equipment_name" placeholder="Enter equipment name" class="mb-2 form-control" autocomplete="off">
            <div class="invalid-feedback text-left" id="err_equipment_name"></div>
          </div>
          <div class="col-sm-12">
            <label class="required">Quantity</label><br>
            <input type="number" name="quantity" id="quantity" placeholder="Enter quantity" class="mb-2 form-control" autocomplete="off">
            <div class="invalid-feedback text-left" id="err_quantity"></div>
          </div>
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-success px-5" id="btn_submit">Submit</button>
          </div>
        </div>
      </div>
    </div>
</form>
</div>

@endsection


@section('script')
<!-- Javascript Function-->
<script type="text/javascript">

	$('#refresh-filter').on('click', function(){
		$('#filter1').val('');
		$('#filter2').val('');
		submit_filter();
	});

	function submit_filter(){
		show_user_accounts();
	}


	show_user_accounts();
	var tbl_user_accounts;
	function show_user_accounts(){
		if (tbl_user_accounts) {
			tbl_user_accounts.destroy();
		}
		tbl_user_accounts = $('#tbl_user_accounts').DataTable({
		pageLength: 10,
		responsive: true,
		deferRender: true,
		aaSorting: [],
		language: {
		"emptyTable": "No data available",
		searchPlaceholder: "Search equipment here"
	},
	ajax: {
        url: "{{ route('admin.sports_equpment.report.list') }}",
        data: {filter1 : $('#filter1').val(), filter2 : $('#filter2').val() },
    },
		columns: [{
		className: '',
		"data": "e_name",
		"title": "Equipment",
		"orderable": false,
	},{
		className: '',
		"data": "report_name",
		"title": "Report Type",
		"orderable": false,
		"searchable": false,
	},{
		className: '',
		"data": "quantity",
		"title": "Stock in/out Quantity",
		"orderable": false,
		"searchable": false,
	},{
		className: '',
		"data": "total_qty",
		"title": "Current Stock",
		"orderable": false,
		"searchable": false,
	},{
    className: '',
    "data": "reason",
    "title": "Reason",
    "orderable": false,
    "searchable": false,
  },{
		className: 'text-center',
		"data": "datetime",
    "orderable": false,
    "searchable": false,
		"title": "Date"
	}
	]
	});
	}


	
</script>
@endsection
