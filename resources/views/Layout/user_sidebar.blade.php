<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  
  <a href="{{ route('user.index') }}" class="brand-link">
    @if(empty(getSystemDetails()->logo))
    <img src="{{ asset('img/default.png') }}" alt="" class="" style="margin-left: 12px width: 50px; height: 50px">
    @else
    <img src="{{ asset('storage/'.getSystemDetails()->logo)}}" alt="" class="" style="margin-left: 12px width: 50px; height: 50px">
    @endif
    
    <span class="brand-text text-warning ">{{ getSystemDetails()->system_name ?? "Malvar System" }}</span>
  </a>
  
  <a href="{{ route('user.profile.index') }}" class="brand-link m-0">
    @if(!empty(Auth::user()->user_img))
    <img class="rounded-circle" src="{{ asset('storage/'.Auth::user()->user_img) }}" style="width: 50px; height: 50px; ">
    @else
    <img class="rounded-circle" src="{{ asset('img/default.png') }}" style="width: 50px; height: 50px; ">
    @endif
    <span class="brand-text ml-1">{{ Auth::user()->first_name }}</span>
  </a>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <!-- SidebarSearch Form -->
    <div class="form-inline mt-4">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
          <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
        with font-awesome or any other icon font library -->
         @if(Auth::user()->status == 3)
        <li class="nav-item">
          <a href="{{ route('user.index') }}" class="nav-link {{ (request()->is('user/dashboard*') || request()->is('user/dashboard/'))? 'active': '' }}">
            <i class="fas fa-tachometer-alt"></i>
            <p style="margin-left: 5px">
              Dashboard
            </p>
          </a>
        </li>
        @else
        <li class="nav-item">
          <a href="{{ route('user.index') }}" class="nav-link {{ (request()->is('user/dashboard*') || request()->is('user/dashboard/'))? 'active': '' }}">
            <i class="fas fa-user-check"></i>
            <p style="margin-left: 5px">
              Verify Account
            </p>
          </a>
        </li>
        
        @endif
        <!-- <li class="nav-item">
          <a href="{{ route('user.cedula.index') }}" class="nav-link {{ (request()->is('admin/dashboard/*') || request()->is('admin/dashboard'))? 'active': '' }}">
            <i class="fas fa-file-alt"></i>
            <p style="margin-left: 5px">
              Cedula Request
            </p>
          </a>
        </li>  -->
       @if(Auth::user()->status == 3)
        <li class="nav-item {{ (request()->is('user/sports/*') || request()->is('user/sports')) || (request()->is('user/barangay/*') || request()->is('user/barangay')) || (request()->is('user/barangay-indigency/*') || request()->is('user/barangay-indigency')) || (request()->is('user/barangay-clearance/*') || request()->is('user/barangay-clearance'))? 'menu-is-opening menu-open': ''}}">

          <a href="#" class="nav-link {{ (request()->is('user/sports/*') || request()->is('user/sports')) || (request()->is('user/barangay/*') || request()->is('user/barangay')) || (request()->is('user/barangay-indigency/*') || request()->is('user/barangay-indigency')) || (request()->is('user/barangay-clearance/*') || request()->is('user/barangay-clearance')) ? 'active': '' }}">
            <i class="fa-solid fa-handshake"></i>
            <p>
              Requests
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('user.sports.index') }}" class="nav-link {{ (request()->is('user/sports/*') || request()->is('user/sports'))? 'active': '' }}">
                <i class="fa-solid fa-volleyball ml-2"></i>
                <p>Sports Equipments</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('user.barangay.index') }}" class="nav-link {{ (request()->is('user/barangay/*') || request()->is('user/barangay'))? 'active': '' }}">
                <i class="fa-solid fa-chair ml-2"></i>
                <p>Barangay Equipments</p>
              </a>
            </li>

            <li class="nav-item">
          <a href="{{ route('user.barangay_clearance.index') }}" class="nav-link {{ (request()->is('user/barangay-clearance/*') || request()->is('user/barangay-clearance'))? 'active': '' }}">
            <i class="fa-regular fa-file-lines ml-2"></i>
            <p style="margin-left: 5px">
              Barangay Clearance
            </p>
          </a>
        </li>


        <li class="nav-item">
          <a href="{{ route('user.barangay_indigency.index') }}" class="nav-link {{ (request()->is('user/barangay-indigency/*') || request()->is('user/barangay-indigency'))? 'active': '' }}">
            <i class="fa-solid fa-indent ml-2"></i>
            <p style="margin-left: 5px">
              Barangay Indigency
            </p>
          </a>
        </li>
          </ul>
        </li>
         <li class="nav-item">
          <a href="{{ route('user.court.index') }}" class="nav-link  {{ (request()->is('user/court/*') || request()->is('user/court'))? 'active': '' }}">
            <i class="fas fa-baseball-ball"></i>
            <p style="margin-left: 5px">
              Gymnasium Reservation
            </p>
          </a>
        </li>

       @else

       @endif




         <li class="nav-item">
          <a href="{{ route('user.announcement.index') }}" class="nav-link {{ (request()->is('user/announcement/*') || request()->is('user/announcement'))? 'active': '' }}">
            <i class="fa-solid fa-bullhorn"></i>
            <p style="margin-left: 5px">
              Announcement
            </p>
          </a>
        </li>
        

        <li class="nav-item">
          <a href="{{ route('user.contacts.index') }}" class="nav-link {{ (request()->is('user/contacts/*') || request()->is('user/contacts'))? 'active': '' }}">
            <i class="fa-solid fa-address-book"></i>
            <p style="margin-left: 5px">
              Contacts
            </p>
          </a>
        </li>
       
        <li class="nav-item">
          <a href="{{ route('user.profile.index') }}" class="nav-link {{ (request()->is('user/profile/*') || request()->is('user/profile'))? 'active': '' }}">
            <i class="fas fa-user-cog"></i>
            <p style="margin-left: 5px">
              Profile
            </p>
          </a>
        </li>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>