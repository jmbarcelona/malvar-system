<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{route('admin.index') }}" class="brand-link">
    @if(empty(getSystemDetails()->logo))
    <img src="{{ asset('img/default.png') }}" alt="" style="margin-left: 12px width: 50px; height: 50px">
    @else
    <img src="{{ asset('storage/'.getSystemDetails()->logo)}}" alt="" style="margin-left: 12px width: 50px; height: 50px">
    @endif
    
    <span class="brand-text text-warning ">{{ getSystemDetails()->system_name ?? "Malvar System" }}</span>
  </a>
  <a href="{{ route('admin.profile.index') }}" class="brand-link m-0">
    @if(!empty(Auth::user()->user_img))
    <img class="rounded-circle" src="{{ asset('storage/'.Auth::user()->user_img) }}" style="width: 50px; height: 50px;">
    @else
    <img class="rounded-circle" src="{{ asset('img/default.png') }}" style="width: 50px; height: 50px;">
    @endif
    <span class="brand-text ml-1">{{ Auth::user()->first_name }}</span>
  </a>
  <!-- Sidebar -->
  <div class="sidebar ">
    <!-- Sidebar user panel (optional) -->
    <!-- SidebarSearch Form -->
    <!-- <div class="form-inline mt-4">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
          <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div> -->
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
        with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="{{route('admin.index') }}" class="nav-link {{ (request()->is('admin/dashboard/*') || request()->is('admin/dashboard'))? 'active': '' }}">
            <i class="fas fa-tachometer-alt"></i>
            <p style="margin-left: 5px">
              Dashboard
            </p>
          </a>
        </li>
         
        <li class="nav-item {{ (request()->is('admin/barangay-clearance/*') || request()->is('admin/barangay-clearance')) ||  (request()->is('admin/barangay-indigency/*') || request()->is('admin/barangay-indigency')) || (request()->is('admin/request-sport/*') || request()->is('admin/request-sport')) || (request()->is('admin/request-barangay/*') || request()->is('admin/request-barangay'))? 'menu-is-opening menu-open': ''}} ">

          <a href="#" class="nav-link {{ (request()->is('admin/barangay-clearance/*') || request()->is('admin/barangay-clearance')) ||  (request()->is('admin/barangay-indigency/*') || request()->is('admin/barangay-indigency')) || (request()->is('admin/request-sport/*') || request()->is('admin/request-sport')) || (request()->is('admin/request-barangay/*') || request()->is('admin/request-barangay')) ? 'active': '' }}">

            <i class="fa-solid fa-handshake"></i>
            <p>
              Requests
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">

            <li class="nav-item">
              <a href="{{ route('admin.request-barangay.index') }}" class="nav-link {{ (request()->is('admin/request-barangay/*') || request()->is('admin/request-barangay'))? 'active': '' }}">
                <i class="fa-solid fa-chair ml-2"></i>
                <p>Barangay Equipments</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('admin.request-sport.index') }}" class="nav-link {{ (request()->is('admin/request-sport/*') || request()->is('admin/request-sport'))? 'active': '' }}">
                <i class="fa-solid fa-volleyball ml-2"></i>
                <p>Sports Equipments</p>
              </a>
            </li>


            <li class="nav-item">
              <a href="{{ route('admin.barangay_clearance.index') }}" class="nav-link {{ (request()->is('admin/barangay-clearance/*') || request()->is('admin/barangay-clearance'))? 'active': '' }}">
                <i class="fa-regular fa-file-lines ml-2"></i>
                <p>Barangay Clearance</p>
              </a>
            </li>

            <li class="nav-item">
              <a href="{{ route('admin.barangay_indigency.index') }}" class="nav-link {{ (request()->is('admin/barangay-indigency/*') || request()->is('admin/barangay-indigency'))? 'active': '' }}">
                <i class="fa-solid fa-indent ml-2"></i>
                <p>Barangay Indigency</p>
              </a>
            </li>

          </ul>
        </li>
        <!-- <li class="nav-item">
          <a href="" class="nav-link {{ (request()->is('admin/roastery/*') || request()->is('admin/roastery'))? 'active': '' }}">
            <i class="fas fa-file-alt"></i>
            <p style="margin-left: 5px">
              Cedula Request
            </p>
          </a>
        </li> -->
        <li class="nav-item">
          <a href="{{ route('admin.Gym_scheduling.index') }}" class="nav-link {{ (request()->is('admin/Gym-scheduling/*') || request()->is('admin/Gym-scheduling'))? 'active': '' }}">
            <i class="fa-solid fa-basketball"></i>
            <p style="margin-left: 5px">
              Gymnasium Reservation
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.sports.index') }}" class="nav-link {{ (request()->is('admin/sports/equipments/*') || request()->is('admin/sports/equipments'))? 'active': '' }}">
            <i class="fa-solid fa-volleyball"></i>
            <p style="margin-left: 5px">
              Sports Equipment Stock
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.barangay.index') }}" class="nav-link {{ (request()->is('admin/barangay/equipments/*') || request()->is('admin/barangay/equipments'))? 'active': '' }}">
            <i class="fas fa-chair"></i>
            <p style="margin-left: 5px">
              Brgy Equipment Stock
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('admin.announcement.index') }}" class="nav-link {{ (request()->is('admin/announcement/*') || request()->is('admin/announcement'))? 'active': '' }}">
            <i class="fa-solid fa-bullhorn"></i>
            <p style="margin-left: 5px">
              Announcement
            </p>
          </a>
        </li>

        <li class="nav-item {{ (request()->is('admin/reports/barangay-equpment/*') || request()->is('admin/reports/barangay-equpment')) ||  (request()->is('admin/reports/sports-equpment/*') || request()->is('admin/reports/sports-equpment')) || (request()->is('admin/reports/barangay-clearance/*') || request()->is('admin/reports/barangay-clearance')) || (request()->is('admin/reports/barangay-indigency/*') || request()->is('admin/reports/barangay-indigency'))? 'menu-is-opening menu-open': ''}}">
          
          <a href="#" class="nav-link {{ (request()->is('admin/reports/barangay-equpment/*') || request()->is('admin/reports/barangay-equpment')) ||  (request()->is('admin/reports/sports-equpment/*') || request()->is('admin/reports/sports-equpment')) || (request()->is('admin/reports/barangay-clearance/*') || request()->is('admin/reports/barangay-clearance')) || (request()->is('admin/reports/barangay-indigency/*') || request()->is('admin/reports/barangay-indigency'))? 'active': '' }}">
            <i class="fas fa-copy"></i>
            <p style="margin-left: 5px">
              Reports / Logs
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">

            <li class="nav-item">
              <a href="{{ route('admin.barangay_equpment.report') }}" class="nav-link text-sm {{ (request()->is('admin/reports/barangay-equpment/*') || request()->is('admin/reports/barangay-equpment'))? 'active': '' }}">
                <i class="fa-solid fa-chair ml-2"></i>
                <p>Barangay Equipments logs</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('admin.sports_equpment.report') }}" class="nav-link text-sm {{ (request()->is('admin/reports/sports-equpment/*') || request()->is('admin/reports/sports-equpment'))? 'active': '' }}">
                <i class="fa-solid fa-volleyball ml-2"></i>
                <p>Sports Equipments logs</p>
              </a>
            </li>


            <li class="nav-item">
              <a href="{{ route('admin.barangay_clearance.report') }}" class="nav-link text-sm {{ (request()->is('admin/reports/barangay-clearance/*') || request()->is('admin/reports/barangay-clearance'))? 'active': '' }}">
                <i class="fa-regular fa-file-lines ml-2"></i>
                <p>Barangay Clearance reports</p>
              </a>
            </li>

            <li class="nav-item">
              <a href="{{ route('admin.barangay_indigency.report') }}" class="nav-link text-sm {{ (request()->is('admin/reports/barangay-indigency/*') || request()->is('admin/reports/barangay-indigency'))? 'active': '' }}">
                <i class="fa-solid fa-indent ml-2"></i>
                <p>Barangay Indigency reports</p>
              </a>
            </li>

          </ul>
        </li>



        
        
        
        <!-- <li class="nav-item">
          <a href="" class="nav-link {{ (request()->is('admin/roastery/*') || request()->is('admin/roastery'))? 'active': '' }}">
            <i class="fas fa-chair"></i>
            <p style="margin-left: 5px">
              Barangay Clearance
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="" class="nav-link {{ (request()->is('admin/roastery/*') || request()->is('admin/roastery'))? 'active': '' }}">
            <i class="fas fa-chair"></i>
            <p style="margin-left: 5px">
              Certificate of indigency
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="" class="nav-link {{ (request()->is('admin/roastery/*') || request()->is('admin/roastery'))? 'active': '' }}">
            <i class="fas fa-baseball-ball"></i>
            <p style="margin-left: 5px">
              Court Rent
            </p>
          </a>
        </li> -->
        <li class="nav-item">
          <a href="{{ route('admin.contact.index') }}" class="nav-link {{ (request()->is('admin/contact/*') || request()->is('admin/contact'))? 'active': '' }}">
            <i class="fa-solid fa-address-book"></i>
            <p style="margin-left: 5px">
              Contacts
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('admin.verify.index') }}" class="nav-link {{ (request()->is('admin/admin-verify/*') || request()->is('admin/admin-verify'))? 'active': '' }}">
            <i class="fas fa-user-check"></i>
            <p style="margin-left: 5px">
              Verify Users
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('admin.users.index') }}" class="nav-link {{ (request()->is('admin/user-management/*') || request()->is('admin/user-management'))? 'active': '' }}">
            <i class="fas fa-users"></i>
            <p style="margin-left: 5px">
              Users management
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.profile.index') }}" class="nav-link {{ (request()->is('admin/profile/*') || request()->is('admin/profile'))? 'active': '' }}">
            <i class="fas fa-user-cog"></i>
            <p style="margin-left: 5px">
              Profile
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('admin.system.index') }}" class="nav-link {{ (request()->is('admin/system/*') || request()->is('admin/system'))? 'active': '' }}">
            <i class="fa-solid fa-gear"></i>
            <p style="margin-left: 5px">
              System Management
            </p>
          </a>
        </li>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>