<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		@include('Layout.header')
		@yield('css')
	</head>
	<body class="hold-transition sidebar-mini layout-fixed">
		@include('Layout.user_sidebar')
		@include('Layout.user_navbar')
		<div class="content-wrapper">
			@yield('content')
			
		</body>
		@include('Layout.footer')
		@yield('script')
	</html>