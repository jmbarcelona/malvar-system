<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		@include('Layout.header')
		@yield('css')
	</head>
	<body style="background-image: url('public/img/schoolbook.jpg');background-position: center; background-repeat: no-repeat; background-size: cover;">
		@include('Layout.auth_navbar')
		@include('Layout.home_navbar')
		
		@yield('content')
		
	</body>
	@include('Layout.footer')
	@yield('script')
</html>