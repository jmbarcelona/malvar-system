<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		@include('Layout.header')
		@yield('css')
	</head>
	<body>
		@include('Layout.auth_navbar')
		
		@yield('content')
		
	</body>
	@include('Layout.footer')
	@yield('script')
</html>