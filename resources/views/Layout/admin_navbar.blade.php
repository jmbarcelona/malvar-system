<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-dark navbar-dark">
  <!-- Preloader -->
  
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button" data-auto-collapse-size="15000"><i class="fas fa-bars"></i></a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <!-- <a href="index3.html" class="nav-link">Home</a> -->
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <!-- <a href="#" class="nav-link">Contact</a> -->
    </li>
  </ul>
  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    
    <li class="nav-item">
      <a class="nav-link" data-widget="fullscreen" href="#" role="button">
        <i class="fas fa-expand-arrows-alt"></i>
      </a>
    </li>
    <li class="nav-item">
      
      <button type="button" class="btn btn-secondary" onclick="logout()">Logout</button>
      
    </li>
    
  </ul>
</nav>
<!-- /.navbar -->
<script type="text/javascript">
function logout(){
Swal.fire({
title: 'Are you sure?',
text: "You will be logged out!",
icon: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
confirmButtonText: 'Yes, proceed!'
}).then((result) => {
if (result.isConfirmed) {
window.location.href = "{{ route('auth.logout') }}";
Swal.fire(
'Success!',
'You logged out!',
'success'
)
}
})
}
</script>