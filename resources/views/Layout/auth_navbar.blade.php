<!-- Navbar -->
<nav class="navbar navbar-expand navbar-dark navbar-dark">
  <!-- Left navbar links -->
  <!-- Preloader -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link h4 pb-0 text-warning" href="#" role="button">{{ getSystemDetails()->system_name ?? "Malvar System" }}
      </a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <!-- <a href="index3.html" class="nav-link">Home</a> -->
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <!-- <a href="#" class="nav-link">Contact</a> -->
    </li>
  </ul>
  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <li class="nav-item">
      <a href="{{ route('auth.registration') }}" class="btn btn-success px-3">Register</a>
    </li>
    <li class="nav-item">
      <a href="{{ route('login') }}" class="btn btn-info px-4 mx-2">Login</a>
    </li>
    
  </ul>
</nav>
<!-- /.navbar -->