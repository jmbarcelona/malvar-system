@extends('Layout.app_auth')
@section('title', 'Change Password')
@section('content')

  <div class="login-logo">
    @if(empty(getSystemDetails()->logo))
    <img src="{{ asset('img/default.png') }}" class="mt-4" style="width: 150px"><br>
    @else
    <img src="{{ asset('storage/'.getSystemDetails()->logo)}}" class="mt-4" style="width: 150px"><br>
    @endif
    <a href=""><strong>{{ getSystemDetails()->system_name ?? "MalvarSystem"}}</strong></a>
  </div>


  <div class="container-fluid mt-5">
      <div class="row justify-content-center">
        <div class="col-sm-6">
          <form action="{{ route('auth.reset_password_save') }}/{{ request()->id }}" id="change_password_form" novalidate="" class="needs-validation">
          <div class="card">
            <div class="card-header h5">Reset Password</div>
            <div class="card-body">
              
      <div class="input-group mb-3">
        <input type="password" id="new_pass" name="new_pass" class="form-control" placeholder="Enter new password " aria-label="Amount (to the nearest dollar)">
        <div class="input-group-append">
          <span class="input-group-text " style="cursor: pointer" onclick=" password_visibile('eye2', 'new_pass');"><i class="fa-solid fa-eye" id="eye2"></i></span>
        </div>
        <div class="invalid-feedback text-left" id="err_new_pass"></div>
      </div>
      <div class="input-group mb-3">
        <div class="input-group-prepend">
        </div>
        <input type="password" id="confirm_pass" name="confirm_pass" class="form-control" placeholder="Confirm password " aria-label="Amount (to the nearest dollar)">
        <div class="input-group-append">
          <span class="input-group-text " style="cursor: pointer" onclick=" password_visibile('eye3', 'confirm_pass');"><i class="fa-solid fa-eye" id="eye3"></i></span>
        </div>
        <div class="invalid-feedback text-left" id="err_confirm_pass"></div>
      </div>
            </div>
            <div class="card-footer text-right">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">

  $('#change_password_form').on('submit', function(e){
    e.preventDefault();
    let form = $("#change_password_form")[0];
    // Create an FormData object 
  var data = new FormData(form);
    let url = $(this).attr('action');
    $.ajax({
        type:"POST",
        url:url,
        data:data,
        enctype: 'multipart/form-data',
        processData: false,  // Important!
        contentType: false,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
              Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: response.message,
                        showConfirmButton: true,
                        timer: 1500
                      })

              setTimeout(function(){
                 window.location.href = "{{ route('login') }}";
              }, 1700); 
             
         }else{
          console.log(response);
          showValidator(response.error, 'change_password_form');
          if (response.message) {
              Swal.fire({
                          position: 'center',
                          icon: 'info',
                          title: response.message,
                          showConfirmButton: true,
                          timer: 10000
                        })
          }
          
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  });	

  function password_visibile(icon, input){
        $("#"+icon).toggleClass('fa-eye fa-eye-slash');
        $("#"+input).attr('type', function(index, attr){
        return (attr == 'text')? 'password' : 'text';
        });
    }

</script>
@endsection