@extends('Layout.app_auth')
@section('title', 'Register')
@section('css')
<style type="text/css">
.required:before {
content:" *";
color: red;
}
</style>
@endsection
@section('content')
<div class="container-fluid mt-4">
  <div class="row justify-content-center">
    <div class="col-sm-8">
      <form action="{{ route('auth.registration.add') }}" id="Registration_form" class="needs-validation">
        <div class="card">
          <div class="card-header h2">Register account</div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="required">Firstname</label>
                <input type="text" name="first_name" id="first_name" placeholder="Enter firstname here" class="mb-2 form-control" autocomplete="off">
                <div class="invalid-feedback text-left" id="err_first_name"></div>
              </div>
              <div class="col-sm-4">
                <label>Middlename</label>
                <input type="text" name="middle_name" id="middle_name" placeholder="Enter middlename here" class="mb-2 form-control" autocomplete="off">
                <div class="invalid-feedback text-left" id="err_middle_name"></div>
              </div>
              <div class="col-sm-4">
                <label class="required">Lastname</label>
                <input type="text" name="last_name" id="last_name" placeholder="Enter lastname here" class="mb-2 form-control" autocomplete="off">
                <div class="invalid-feedback text-left" id="err_last_name"></div>
              </div>
              <div class="col-sm-12">
                <label class="required">Date of Birth</label>
                <input type="date" name="bday" id="bday" class="mb-2 form-control" autocomplete="off">
                <div class="invalid-feedback text-left" id="err_bday"></div>
              </div>
              <div class="col-sm-6">
                <label class="required">Gender</label>
                <select name="gender" id="gender" class="form-control" autocomplete="off">
                  <option selected="" disabled="">Please select gender</option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                </select>
                <div class="invalid-feedback text-left" id="err_gender"></div>
              </div>
              <div class="col-sm-6">
                <label class="required">Contact</label>
                <input type="text" name="contact" id="contact" placeholder="Enter contact here" class="mb-2 form-control" autocomplete="off">
                <div class="invalid-feedback text-left" id="err_contact"></div>
              </div>
              <div class="col-sm-12">
                <label class="required">Address</label>
                <textarea class="form-control mb-2" rows="3" id="address" name="address" placeholder="Enter address here"></textarea>
                <div class="invalid-feedback text-left" id="err_address"></div>
              </div>
              <div class="col-sm-12">
                <label class="required">Email</label>
                <input type="text" name="email_address" id="email_address" class="form-control mb-2" placeholder="Enter email here">
                <div class="invalid-feedback text-left" id="err_email_address"></div>
              </div>
              <div class="col-sm-12">
                <label class="required">Password</label>
                <input type="password" name="password" id="password" class="form-control mb-2" placeholder="Enter password here">
                <div class="invalid-feedback text-left" id="err_password"></div>
              </div>
              <div class="col-sm-12">
                <label class="required">Confirm Password</label>
                <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Verify password here">
                <div class="invalid-feedback text-left" id="err_confirm_password"></div>
              </div>
            </div>
          </div>
          <div class="card-footer text-right">
            <button type="button" class="btn btn-danger px-5" onclick="clear_button()">Clear</button>
            <button type="submit" class="btn btn-success px-5">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
 
 function clear_button(){
  $('#Registration_form')[0].reset();
 }

  $('#Registration_form').on('submit', function(e){
    e.preventDefault();
    let formData = $(this).serialize();
    let url = $(this).attr('action');
    $.ajax({
        type:"post",
        url:url,
        data:formData,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
           console.log(response);
         if (response.status === true) {
              
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Account Created Successfully!',
                showConfirmButton: false,
                timer: 1500
              })
              $('#Registration_form')[0].reset();
              setTimeout(function(){
                history.back();
              }, 1000);
              
         }else{
          // console.log(response);
          showValidator(response.error, 'login_form');
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  });
	
</script>
@endsection