@extends('Layout.app_auth')
@section('title', 'Forget Password')
@section('content')

  <div class="login-logo">
    @if(empty(getSystemDetails()->logo))
    <img src="{{ asset('img/default.png') }}" class="mt-4" style="width: 150px"><br>
    @else
    <img src="{{ asset('storage/'.getSystemDetails()->logo)}}" class="mt-4" style="width: 150px"><br>
    @endif
    <a href=""><strong>{{ getSystemDetails()->system_name ?? "MalvarSystem"}}</strong></a>
  </div>


  <div class="container-fluid mt-5">
      <div class="row justify-content-center">
        <div class="col-sm-6">
          <form action="{{ route('auth.forget_pass_save') }}" id="forget_password_form" novalidate="" class="needs-validation">
          <div class="card">
            <div class="card-header h5">Forget Password</div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-2 mt-1">
                  <label>Email Address</label>
                </div>
                <div class="col-sm-10 text-left">
                  <input type="text" name="email_address" id="email_address" class="form-control" placeholder="Enter a valid email address here">
                  <div class="invalid-feedback" id="err_email_address"></div>
                </div>
              </div>
            </div>
            <div class="card-footer text-right">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">


  $('#forget_password_form').on('submit', function(e){
    e.preventDefault();
    let form = $("#forget_password_form")[0];
    // Create an FormData object 
  var data = new FormData(form);
    let url = $(this).attr('action');
    $.ajax({
        type:"POST",
        url:url,
        data:data,
        enctype: 'multipart/form-data',
        processData: false,  // Important!
        contentType: false,
        dataType:'json',
        beforeSend:function(){
        Swal.fire('Please wait...')
        Swal.showLoading();
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
              Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: response.message,
                        showConfirmButton: true,
                        timer: 5000
                      })
            $('#email_address').val('');

         }else{
          console.log(response);
          showValidator(response.error, 'forget_password_form');
          Swal.close();
          
          if (response.message) {
              Swal.fire({
                          position: 'center',
                          icon: 'info',
                          title: response.message,
                          showConfirmButton: true,
                          timer: 10000
                        })
          }
         }
        
        },
        error: function(error){
          console.log(error);
        }
      });
  });	
</script>
@endsection