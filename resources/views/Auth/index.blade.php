@extends('Layout.app_auth')
@section('title', 'Login')
@section('content')
<div class="row justify-content-center mt-1">
  <div class="login-box">
    <div class="login-logo">
      @if(empty(getSystemDetails()->logo))
      <img src="{{ asset('img/default.png') }}" class="mt-4" style="width: 150px"><br>
      @else
      <img src="{{ asset('storage/'.getSystemDetails()->logo)}}" class="mt-4" style="width: 150px"><br>
      @endif
      <a href=""><strong>{{ getSystemDetails()->system_name ?? "MalvarSystem"}}</strong></a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form action="{{ route('auth.check')}}" id="login_form" novalidate="" class="needs-validation">
          <div class="input-group mb-3">
            <input type="email" class="form-control" placeholder="Email" id="email_address" name="email_address">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
            <div class="invalid-feedback" id="err_email_address"></div>
          </div>
          <div class="input-group mb-3">
            <input type="password" class="form-control" placeholder="Password" id="password" name="password">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
            <div class="invalid-feedback" id="err_password"></div>
          </div>
          <div class="row">
            <div class="col-12">
              <p class="mb-0">
              </p>
            </div>
            <!-- /.col -->
            <div class="col-12 text-center">
              <button type="submit" class="btn btn-info btn-block">Log In</button>
            </div>
            <div class="col-12 text-center mt-1">
              <a href="{{ route('auth.forget_pass') }}">Forget password?</a>
            </div>
            <div class="col-12">
              <hr>
              <a href="{{ route('auth.registration') }}" class="btn btn-success btn-block">Create new account</a>
            </div>
          </div>
        </form>
        <div class="social-auth-links text-center">
          <!-- /.social-auth-links -->
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
  </div>
  <!-- /.login-box -->
@endsection
@section('script')
<script type="text/javascript">
  $('#login_form').on('submit', function(e){
    e.preventDefault();
    let formData = $(this).serialize();
    let url = $(this).attr('action');
    $.ajax({
        type:"post",
        url:url,
        data:formData,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
           console.log(response);
         if (response.status === true) {
              window.location = response.redirect;
         }else{
          console.log(response);
          showValidator(response.error, 'login_form');
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  });
	
</script>
@endsection