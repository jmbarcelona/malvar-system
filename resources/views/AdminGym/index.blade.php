@extends('Layout.app_admin')
@section('title', 'Gymnasium Reservation')
@section('css')
<style type="text/css">
	

</style>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row pt-3">
		<div class="col-lg-12 col-md-12 col-12">
			<div class="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
				<div class="mb-3 mb-md-0">
					<h1 class="mb-1 h2 fw-bold">Today's Gymnasium Reservation</h1>
				</div>
				<div class="d-flex">
					<button class="btn btn-info btn-shadow" onclick="add_user_accounts();"><i class="fa-solid fa-plus"></i></i>&nbsp;Add Gymnasium Reservation</button>
				</div>
			</div>
		</div>
	</div>

	<form class="needs-validation" id="add_edit_form" action="{{ route('admin.Gym_scheduling.store') }}" novalidate>
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Record Filter</h5>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse">
						<i class="fas fa-minus"></i>
						</button>
					</div>
				</div>
				
				<div class="card-body">
					<div class="row">

						<div class="position-relative mb-2 col-md-3">
							<label>Date From</label>
							<input type="date" name="filter1" id="filter1" class="form-control" placeholder="Search contact name here">
						</div>

						<div class="position-relative mb-2 col-md-3">
							<label>Date To</label>
							<input type="date" name="filter2" id="filter2" class="form-control" placeholder="Search contact name here">
						</div>

					</div>
					
				</div>
				<div class="card-footer text-right">
					<!-- <button type="button" class="btn btn-secondary" data-card-widget="collapse">
					<i class="fa fa-minus"></i> Minimize Filter
					</button> -->
					<button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
					<button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
				</div>
				
			</div>
			<div class="card">
				<div class="card-body">
					<div class="table-responsive">
					<table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;"></table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" role="dialog" id="modal_add_edit">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title h4">
						Add Gym Schedule
					</div>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12 mb-2">
							<label>Renter Name</label>
							<input type="text" id="renter" name="renter" class="form-control" placeholder="Enter renter name">
							<div class="invalid-feedback text-left" id="err_renter"></div>

						</div>

						<div class="col-sm-12">
							<input type="hidden" name="id" id="id">
							<label class="required">Start time</label><br>
							<select name="start_time" id="start_time"  class="mb-2 form-control" autocomplete="off" onchange="time_to_validate(this.value)">
								<option value="" selected="" disabled="">Select start time</option>
								<option value="6" id="6pm1">6:00 PM</option>
								<option value="7" id="7pm1">7:00 PM</option>
								<option value="8" id="8pm1">8:00 PM</option>
								<option value="9" id="9pm1">9:00 PM</option>
							</select>
							<div class="invalid-feedback text-left" id="err_start_time"></div>
						</div>
						<div class="col-sm-12">
							<label class="required">End time</label><br>
							<select name="end_time" id="end_time"  class="mb-2 form-control" autocomplete="off" onchange="amount_to_paid(this.value)">
								<option value="" selected="" disabled="">Select end time</option>
								<option value="7" id="7pm">7:00 PM</option>
								<option value="8" id="8pm">8:00 PM</option>
								<option value="9" id="9pm">9:00 PM</option>
								<option value="10" id="10pm">10:00 PM</option>
							</select>
							<div class="invalid-feedback text-left" id="err_end_time"></div>
						</div>

						<div class="col-sm-12 mb-2">
							<label class="required">Date</label><br>
							<input type="date" name="date" id="date" class="form-control">
							<div class="invalid-feedback text-left" id="err_date"></div>
						</div>

						<div class="col-sm-12 mb-2">
							<label class="required">Reason</label><br>
							<textarea name="reason" id="reason" class="form-control" rows="2" placeholder="Enter Reason"></textarea>
							<div class="invalid-feedback text-left" id="err_reason"></div>
						</div>

						<div class="col-sm-4">
							<label class="required">Amount to paid</label><br>
							<input type="text" name="to_paid" id="to_paid" class="form-control bg-light" disabled="">
						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success px-5" id="btn_submit">Submit</button>
				</div>
			</div>
		</div>
	</div>
</form>
</div>



@endsection


@section('script')
<!-- Javascript Function-->
<script type="text/javascript">

	function amount_to_paid(_this){
		if (_this == 7) {
			$('#6pm1').removeClass("d-none", "d-none");
			$('#7pm1').addClass("d-none", "d-none");
			$('#8pm1').addClass("d-none", "d-none");
			$('#9pm1').addClass("d-none", "d-none");
		}else if(_this == 8) {
			$('#6pm1').removeClass("d-none", "d-none");
			$('#7pm1').removeClass("d-none", "d-none");
			$('#8pm1').addClass("d-none", "d-none");
			$('#9pm1').addClass("d-none", "d-none");
		}else if(_this == 9){
			$('#6pm1').removeClass("d-none", "d-none");
			$('#7pm1').removeClass("d-none", "d-none");
			$('#9pm1').addClass("d-none", "d-none");
			$('#8pm1').removeClass("d-none", "d-none");
		}else{
			$('#6pm1').removeClass("d-none", "d-none");
			$('#7pm1').removeClass("d-none", "d-none");
			$('#9pm1').removeClass("d-none", "d-none");
			$('#8pm1').removeClass("d-none", "d-none");
		}
		
		var total = $('#end_time').val() - $('#start_time').val()

		$('#to_paid').val("₱"+total+","+"000")		
		
	}



	function time_to_validate(_this){
		$('#end_time').removeAttr('disabled', 'disabled');

		if ($('#end_time').val() == 10  ) {
			$('#end_time').val();
		}else if($('#end_time').val() <= $('#start_time').val()){
			$('#end_time').val('');
			$('#to_paid').val("");
		}

		if (_this == 7) {
		$('#7pm').addClass("d-none", "d-none");

		$('#8pm').removeClass("d-none", "d-none");
		$('#9pm').removeClass("d-none", "d-none");
		}else if(_this == 8) {

		$('#7pm').addClass("d-none", "d-none");
		$('#8pm').addClass("d-none", "d-none");

		$('#9pm').removeClass("d-none", "d-none");
		}else if(_this == 9) {

		$('#7pm').addClass("d-none", "d-none");
		$('#8pm').addClass("d-none", "d-none");
		$('#9pm').addClass("d-none", "d-none");

		}else if (_this == 6){
		$('#7pm').removeClass("d-none", "d-none");
		$('#8pm').removeClass("d-none", "d-none");
		$('#9pm').removeClass("d-none", "d-none");
		}else{
			$('#end_time').attr('disabled', 'disabled');
		}


		var total = $('#end_time').val() - $('#start_time').val()

		if (total >= 0) {
			$('#to_paid').val("₱"+total+","+"000")
		}
		


	}


	$('#refresh-filter').on('click', function(){
		$('#filter1').val('');
		$('#filter2').val('');
		submit_filter();
	});

	function submit_filter(){
		show_user_accounts();
	}


	show_user_accounts();
	var tbl_user_accounts;
	function show_user_accounts(){
		if (tbl_user_accounts) {
			tbl_user_accounts.destroy();
		}
		tbl_user_accounts = $('#tbl_user_accounts').DataTable({
		pageLength: 10,
		responsive: true,
		deferRender: true,
		aaSorting: [],
		language: {
		searchPlaceholder: "Renter's Name",
		"emptyTable": "No data available"
	},
	ajax: {
        url: "{{ route('admin.Gym_scheduling.list') }}",
        data: {filter1 : $('#filter1').val(), filter2 : $('#filter2').val() },
    },
		columns: [{
		className: 'text-center',
		"data": "id",
		"title": "Operations",
		"orderable": false,
		"render": function(data, type, row, meta){
      newdata = '<div class="btn-group">\
			<button type="button" class="btn btn-sm btn-info">Settings</button>\
			<button type="button" class="btn btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">\
			<span class="sr-only">Toggle Dropdown</span>\
			</button>\
			<div class="dropdown-menu p-2" role="menu">\
			<li><a class="btn bg-success btn-block btn-sm mb-1" onclick="approve('+data+')"><i class="fas fa-thumbs-up"></i> Approve</a></li>\
			<li><a class="btn bg-danger btn-block btn-sm mb-1" onclick="disapprove('+data+')"><i class="fas fa-thumbs-down"></i> Disapprove</a></li>\
			</div>\
			</div>'
		return newdata;
    }
	},{
		className: '',
		"data": "renter",
		"title": "Renter's name",
		"orderable": false,
	},{
		className: '',
		"data": "reason",
		"title": "Note",
		"searchable": false,
		"orderable": false,
		
	},{
		className: '',
		"data": "start_time",
		"title": "Start time",
		"searchable": false,
		"orderable": false,
		"render": function(data, type, row, meta){
      newdata = data+":00 PM"
		return newdata;
    }
	},{
		className: '',
		"data": "end_time",
		"title": "End time",
		"searchable": false,
		"orderable": false,
		"render": function(data, type, row, meta){
      newdata = data+":00 PM"
		return newdata;
    }
	},{
		className: '',
		"data": "amount",
		"title": "Rental Fee",
		"searchable": false,
		"orderable": false,
		"render": function(data, type, row, meta){
      newdata = "₱"+data
		return newdata;
    }
	},{
		className: '',
		"data": "date",
		"title": "Date",
		"searchable": false,
		"orderable": false,
	},{
		className: 'text-center',
		"data": "status",
		"title": "Status",
		"searchable": false,
		"orderable": false,
		"render": function(data, type, row, meta){
		if (row.deleted_at) {
				newdata = '<span class="badge badge-danger p-2">Disapproved</span>'
		}else if(data == 1){
				newdata = '<span class="badge badge-warning p-2">Pending</span>'
		}else if(data == 2){
			newdata = '<span class="badge badge-success p-2">Approved</span>'
		}


			// if (data == 1) {
     	//  		newdata = '<span class="badge badge-warning p-2">Pending</span>'
			// }else if(data == 2){
			// 	newdata = '<span class="badge badge-success p-2">Approved</span>'
			// }else if(row.deleted_at){
			// 	newdata = '<span class="badge badge-danger p-2">Disapproved</span>'
			// }
		return newdata;
    }
	}
	]
	});
	}

	function approve(id){
		Swal.fire({
			  title: 'Are you sure?',
			  text: "This request will be approve!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, approve it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$.ajax({
		    type:"post",
		    url:"{{ route('admin.Gym_scheduling.approve') }}"+"/"+id,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     	Swal.fire(
			      'Success!',
			      response.message,
			      'success'
			    )
		       
		     }else{
		     	Swal.fire({
		                position: 'center',
		                icon: 'error',
		                title: response.message,
		                showConfirmButton: false,
		                timer: 4500
		              })
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    
			  }
			})
	}

	function disapprove(id){
		Swal.fire({
			  title: 'Are you sure?',
			  text: "This request will be disapprove!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, disapprove it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$.ajax({
		    type:"post",
		    url:"{{ route('admin.Gym_scheduling.decline') }}"+"/"+id,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     	Swal.fire(
			      'Success!',
			      'This request has been disapproved.',
			      'success'
			    )
		       
		     }else{
		     	Swal.fire({
		                position: 'center',
		                icon: 'error',
		                title: response.message,
		                showConfirmButton: false,
		                timer: 4500
		              })
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    
			  }
			})
	}

	function delete_s_equipment(id){
		Swal.fire({
			  title: 'Are you sure?',
			  text: "You won't be able to revert this!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$.ajax({
		    type:"delete",
		    url:"{{ route('admin.Gym_scheduling.destroy') }}"+'/'+id,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		     	show_user_accounts();
		     
		       
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });

			    Swal.fire(
			      'Deleted!',
			      'Your record has been deleted.',
			      'success'
			    )
			  }
			})

		
	}

	$("#add_edit_form").on('submit', function(e){
		var mydata = $(this).serialize();
		let url = $(this).attr('action');
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"post",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit').prop('disabled', true);
					$('#btn_submit').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					$('#6pm1').removeClass("d-none", "d-none");
					$('#7pm1').removeClass("d-none", "d-none");
					$('#9pm1').removeClass("d-none", "d-none");
					$('#8pm1').removeClass("d-none", "d-none");
					Swal.fire({
		                position: 'center',
		                icon: 'success',
		                title: 'Request submitted Successfully!',
		                showConfirmButton: false,
		                timer: 1500
		              })
					
					showValidator(response.error,'add_edit_form');
					show_user_accounts();
					$('#modal_add_edit').modal('hide');
				}else{
					if (response.message) {
						Swal.fire({
		                position: 'center',
		                icon: 'error',
		                title: response.message,
		                showConfirmButton: false,
		                timer: 5500
		              })
					}
					
					//<!-- your error message or action here! -->
					showValidator(response.error,'add_edit_form');
				}
				$('#btn_submit').prop('disabled', false);
				$('#btn_submit').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_user_accounts(_this){
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this user_accounts?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"delete",
				url:"",
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					
					show_user_accounts();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function add_user_accounts(){
		$('#6pm1').removeClass("d-none", "d-none");
			$('#7pm1').removeClass("d-none", "d-none");
			$('#9pm1').removeClass("d-none", "d-none");
			$('#8pm1').removeClass("d-none", "d-none");
		$('#add_edit_form').trigger("reset");
		$('#modal_add_edit').modal('show');
	}

	
</script>
@endsection
