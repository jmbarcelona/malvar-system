@extends('Layout.app_admin')
@section('title', 'Barangay Announcement')
@section('css')
<style type="text/css">
</style>
@endsection
@section('content')
<div class="container-fluid">
  <div class="row pt-3">
    <div class="col-lg-12 col-md-12 col-12">
      <div class="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
        <div class="mb-3 mb-md-0">
          <h1 class="mb-1 h2 fw-bold">Barangay Announcement</h1>
        </div>
        <div class="d-flex">
          <button class="btn btn-info btn-shadow" onclick="add_user_accounts();"><i class="fa-solid fa-plus"></i>&nbsp;Add Barangay Announcement</button>
        </div>
      </div>
    </div>
  </div>
  <form class="needs-validation" id="announcement_add_edit_form" action="{{ route('admin.announcement.store') }}" novalidate>
    <div class="container-fluid">
      <!-- <div class="card">
        <div class="card-header">
          <h5 class="card-title">Record Filter</h5>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="position-relative mb-2 col-md-4">
              <label>Status Filter</label>
              <select name="filter" id="filter" class="form-control">
                <option value="" selected="" disabled="">Please Select status</option>
                <option value="1">Hidden</option>
                <option value="2">Showing</option>
              </select>
            </div>
          </div>
        </div>
        <div class="card-footer text-right">
          <button type="button" class="btn btn-secondary" data-card-widget="collapse">
          <i class="fa fa-minus"></i> Minimize Filter
          </button>
          <button type="button" id="refresh-filter" class="btn btn-success btn-md"><i class="fa fa-sync-alt"></i> Refresh Filter</button>
          <button type="button" class="btn btn-info btn-md" onclick="submit_filter()"><i class="fa fa-search"></i> Run Filter</button>
        </div>
      </div> -->
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
          <table class="table table-bordered table-hove" id="tbl_user_accounts" style="width: 100%;"></table>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" role="dialog" id="modal_add_edit">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title h4">
            Add announcement
          </div>
          <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <input type="hidden" name="id" id="id">
              <label class="required">Announcement Name</label><br>
              <input type="text" name="announcement_name" id="announcement_name" placeholder="Enter announcement name" class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_announcement_name"></div>
            </div>
            <div class="col-sm-12">
              <label class="required">Date</label><br>
              <input type="date" name="date" id="date"  class="mb-2 form-control" autocomplete="off">
              <div class="invalid-feedback text-left" id="err_date"></div>
            </div>
            <div class="col-sm-12">
              <label class="required">Content</label><br>
              <textarea class="form-control" id="description" name="description" placeholder="Enter announcement content" rows="3"></textarea>
              <div class="invalid-feedback text-left" id="err_description"></div>
            </div>
            <div class="col-sm-12 mt-2 text-center">
              <img src="{{ asset('img/default.png') }}" class="mt-4" style="width: 400px;" id="img_shower"><br>
            </div>
            <div class="col-sm-12 mt-2 text-center">
              <input type="file" name="img" id="img" placeholder="Enter contact number" class="mb-2" autocomplete="off" hidden>
              <button type="button" class="btn btn-info col-sm-12" onclick='$("#img").trigger("click")'>+ Upload Image</button>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger px-5" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-success px-5" id="btn_submit">Submit</button>
        </div>
      </div>
    </div>
  </div>
</form>



<div class="modal fade" role="dialog" id="modal_view">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title h4">
          View announcement
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <label class="required">Announcement Name</label><br>
            <input readonly="" type="text"  id="announcement_name_view" class="mb-2 form-control" autocomplete="off" style="background-color: white">
            <div class="invalid-feedback text-left" id="err_announcement_name"></div>
          </div>
          <div class="col-sm-12">
            <label class="required">Date</label><br>
            <input readonly="" type="date"  id="date_view"  class="mb-2 form-control" autocomplete="off" style="background-color: white">
            <div class="invalid-feedback text-left" id="err_date"></div>
          </div>
          <div class="col-sm-12">
            <label class="required">Description</label><br>
            <textarea readonly="" class="form-control text-lg" id="description_view" style="background-color: white" rows="8"></textarea>
            <div class="invalid-feedback text-left" id="err_description"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger px-5" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
</div>



@endsection


@section('script')
<!-- Javascript Function-->
<script type="text/javascript">


  $('#refresh-filter').on('click', function(){
    $('#filter').val('');
    submit_filter();
  });

  function submit_filter(){
    show_user_accounts();
  }


  show_user_accounts();
  var tbl_user_accounts;
  function show_user_accounts(){
    if (tbl_user_accounts) {
      tbl_user_accounts.destroy();
    }
    tbl_user_accounts = $('#tbl_user_accounts').DataTable({
    pageLength: 10,
    responsive: true,
    deferRender: true,
    aaSorting: [],
    language: {
    "emptyTable": "No data available",
    searchPlaceholder: "announcement name"
  },
  ajax: {
        url: "{{ route('admin.announcement.list') }}",
        data: {filter : $('#filter').val()},
    },
    columns: [{
    className: '',
    "data": "img",
    "title": "",
    "orderable": false,
    "width": 1,
    "render": function(data, type, row, meta){
      if (data) {
        newdata = '<img src="http://localhost/malvar-system/public/storage/'+data+'" class="" style="width: 60px; height: 60px">'
      }else{
        newdata = '<img src="{{ asset('img/default.png') }}" class="" style="width: 60px; height: 60px">'
      }
      
    return newdata;
    }
  },{
    className: '',
    "data": "a_name",
    "title": "Announcement Name",
    "orderable": false,
  },{
    className: '',
    "data": "date.",
    "title": "Date",
    "orderable": false,
  },{
    className: 'text-center',
    "width": "200px",
    "data": "id",
    "title": "Operations",
    "orderable": false,
    "render": function(data, type, row, meta){
      newdata = '<div class="btn-group">\
    <button type="button" class="btn btn-sm btn-info">Settings</button>\
    <button type="button" class="btn btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">\
    <span class="sr-only">Toggle Dropdown</span>\
    </button>\
    <div class="dropdown-menu p-2" role="menu">\
    <li><a class="btn bg-info btn-block btn-sm mb-1" onclick="full_view('+data+')"> View</a></li>\
    <li><a class="btn bg-warning btn-block btn-sm mb-1" onclick="edit_s_equipment('+data+')"><i class="fa-solid fa-pen-to-square"></i></i> Edit</a></li>\
    <li><a class="btn bg-danger btn-block btn-sm mb-1" onclick="delete_s_equipment('+data+')"><i class="fa-solid fa-trash-can"></i></i> Delete</a></li>\
    </div>\
    </div>'
    return newdata;
    }
  }
  ]
  });
  }



  function hide_announcement(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "This announcement will be hidden!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, hide it!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
        type:"post",
        url:"{{ route('admin.announcement.hide') }}"+"/"+id,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
          
            show_user_accounts();
           
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });

          Swal.fire(
            'Success!',
            'Announcement has been hidden.',
            'success'
          )
        }
      })
  }

  function show_announcement(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "This announcement will be shown!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, show it!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
        type:"post",
        url:"{{ route('admin.announcement.show') }}"+"/"+id,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
          show_user_accounts();
         
           
         }else{
          
          console.log(response);
          Swal.fire(
            'Success!',
            response.message,
            'success'
          )
         }
        },
        error: function(error){
          console.log(error);
        }
      });
          Swal.fire(
            'Success!',
            'Announcement has been shown.',
            'success'
          )
        }
      })
  }


  function full_view(id){
    $.ajax({
          type:"get",
          url:"{{ route('admin.announcement.find') }}"+'/'+id,
          data:{},
          dataType:'json',
          beforeSend:function(){
          },
          success:function(response){
           if (response.status == true) {
              $('#announcement_name_view').val(response.data.a_name);
              $('#description_view').val(response.data.description);
              $('#date_view').val(response.data.date);
              $('.modal-title').html('View announcement')
              $('#modal_view').modal('show');
           }else{
            console.log(response);
           }
          },
          error: function(error){
            console.log(error);
          }
        });   
  }


  function edit_s_equipment(id){
    $.ajax({
            type:"get",
            url:"{{ route('admin.announcement.find') }}"+'/'+id,
            data:{},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
             if (response.status == true) {
                $('#announcement_name').val(response.data.a_name);
                $('#description').val(response.data.description);
                $('#date').val(response.data.date);
                $('#id').val(response.data.id);
                $('.modal-title').html('Edit announcement')
                if (response.data.img) {
                $("#img_shower").attr("src", "http://localhost/malvar-system/public/storage/"+response.data.img+"")
                }
                $('#modal_add_edit').modal('show');
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          });   
  }


  function delete_s_equipment(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
        type:"delete",
        url:"{{ route('admin.announcement.destroy') }}"+'/'+id,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
          show_user_accounts();
         
           
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });

          Swal.fire(
            'Deleted!',
            'Announcement has been deleted.',
            'success'
          )
        }
      })

    
  }

  $("#announcement_add_edit_form").on('submit', function(e){
    e.preventDefault(e);
    let form = $("#announcement_add_edit_form")[0];
    var data = new FormData(form);
    let url = $(this).attr('action');
    e.stopPropagation();

    $.ajax({
      type:"post",
      url:url,
      data:data,
      cache:false,
      enctype: 'multipart/form-data',
      processData: false,  // Important!
      contentType: false,
      beforeSend:function(){
          //<!-- your before success function -->
          $('#btn_submit').prop('disabled', true);
          $('#btn_submit').text('Please wait...');
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Announcement Inserted Successfully!',
                    showConfirmButton: false,
                    timer: 1500
                  })
          
          showValidator(response.error,'announcement_add_edit_form');
          show_user_accounts();
          $('#modal_add_edit').modal('hide');
        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'announcement_add_edit_form');
        }
        $('#btn_submit').prop('disabled', false);
        $('#btn_submit').text('Save');
      },
      error:function(error){
        console.log(error)
      }
    });
  });


  function add_user_accounts(){
    $('#announcement_add_edit_form').trigger("reset");
    $('.modal-title').html('Add announcement');
    $("#img_shower").attr("src","{{ asset('img/default.png') }}" )
    $('#modal_add_edit').modal('show');
  }

</script>
<script>
      var selDiv = "";
      var storedFiles = [];
      $(document).ready(function () {
        $("#img").on("change", handleFileSelect);
        selDiv = $("#img_shower");
      });

      function handleFileSelect(e) {
        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        filesArr.forEach(function (f) {
          if (!f.type.match("image.*")) {
            return;
          }
          storedFiles.push(f);

          var reader = new FileReader();
          reader.onload = function (e) {
             $("#img_shower").attr('src', e.target.result);
             $("#img_shower").attr('data-file', f.name);
          };
          reader.readAsDataURL(f);
        });
      }
    </script>
@endsection
