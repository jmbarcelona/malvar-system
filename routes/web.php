<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\AdminProfileController;
use App\Http\Controllers\CedulaRequestController;
use App\Http\Controllers\UserProfileController;
use App\Http\Controllers\SportEquipmentController;
use App\Http\Controllers\BarangayEquipmentController;
use App\Http\Controllers\AdminUserManagementController;
use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\AdminContactController;
use App\Http\Controllers\UserSportsEquipmentController;
use App\Http\Controllers\UserBrgyEquipmentController;
use App\Http\Controllers\AdminRequestSport;
use App\Http\Controllers\AdminRequestBrgy;
use App\Http\Controllers\CourtRequestController;
use App\Http\Controllers\SystemController;
use App\Http\Controllers\BrgyClearanceController;
use App\Http\Controllers\BrgyIndigencyController;
use App\Http\Controllers\UserBrgyClearanceController;
use App\Http\Controllers\UserBrgyIndigencyController;
use App\Http\Controllers\AdminGymController;
use App\Http\Controllers\ReportsController;
use App\Http\Controllers\VerifyAccountController;
use App\Http\Controllers\AdminVerifyController;


// Login
Route::get('/', [AuthController::class, 'index'])->name('login');
Route::post('/login', [AuthController::class, 'check_login'])->name('auth.check');
Route::get('/logout', [AuthController::class, 'logoutUser'])->name('auth.logout');
Route::get('/registration', [AuthController::class, 'registration'])->name('auth.registration');
Route::post('/registration/add', [AuthController::class, 'registrationAdd'])->name('auth.registration.add');
Route::get('/forget_pass', [AuthController::class, 'forgetPass'])->name('auth.forget_pass');
Route::post('/forget_pass/save', [AuthController::class, 'forgetPassSave'])->name('auth.forget_pass_save');
Route::get('/change/password/{id?}', [AuthController::class, 'changePass'])->name('auth.change_password');
Route::post('/change/password/save/{id?}', [AuthController::class, 'changePassSave'])->name('auth.change_password_save');
Route::post('/reset/password/save/{id?}', [AuthController::class, 'resetPassSave'])->name('auth.reset_password_save');


// admin modules
Route::group(['middleware' => ['auth', 'AuthAdmin']], function(){
Route::group(['prefix' => 'admin', 'as' => 'admin.'], function(){

// admin Dashboard
Route::get('/dashboard', [DashboardController::class, 'index'])->name('index');

//admin Verify
Route::get('admin-verify/', [AdminVerifyController::class, 'index'])->name('verify.index');
Route::get('admin-verify/list', [AdminVerifyController::class, 'list'])->name('verify.list');
Route::get('admin-verify/find/{id?}', [AdminVerifyController::class, 'find'])->name('verify.find');
Route::post('admin-verify/approve/{id?}', [AdminVerifyController::class, 'approve'])->name('verify.approve');
Route::post('admin-verify/disapprove/{id?}', [AdminVerifyController::class, 'disapprove'])->name('verify.disapprove');



//admin profile
Route::get('profile', [AdminProfileController::class, 'index'])->name('profile.index');
Route::post('profile/edit', [AdminProfileController::class, 'edit'])->name('profile.edit');
Route::get('profile/find/{id?}', [AdminProfileController::class, 'find'])->name('profile.find');
Route::get('profile/change-password', [AdminProfileController::class, 'changePassword'])->name('profile.change.password');
Route::post('profile/upload', [AdminProfileController::class, 'upload'])->name('profile.upload');

//AdminUserInformationController
Route::get('UserInformation', [AdminUserInformationController::class, 'index'])->name('UserInformation.index');
Route::get('UserInformation/list', [AdminUserInformationController::class, 'list'])->name('UserInformation.list');

//sports_equipment
Route::get('sports/equipments', [SportEquipmentController::class, 'index'])->name('sports.index');
Route::get('sports/store', [SportEquipmentController::class, 'store'])->name('sports.store');
Route::get('sports/list', [SportEquipmentController::class, 'list'])->name('sports.list');
Route::get('sports/find/{id?}', [SportEquipmentController::class, 'find'])->name('sports.find');
Route::post('sports/destroy/{id?}', [SportEquipmentController::class, 'destroy'])->name('sports.destroy');
Route::get('sports/select', [SportEquipmentController::class, 'select'])->name('sports.select');
Route::get('sports/qnty/{id?}', [SportEquipmentController::class, 'qnty'])->name('sports.qnty');
Route::post('sports/stock-in', [SportEquipmentController::class, 'stockIn'])->name('sports.stockIn');
Route::post('sports/stock-out', [SportEquipmentController::class, 'stockOut'])->name('sports.stockOut');

//barangay_equipment
Route::get('barangay/equipments', [BarangayEquipmentController::class, 'index'])->name('barangay.index');
Route::get('barangay/store', [BarangayEquipmentController::class, 'store'])->name('barangay.store');
Route::get('barangay/list', [BarangayEquipmentController::class, 'list'])->name('barangay.list');
Route::get('barangay/find/{id?}', [BarangayEquipmentController::class, 'find'])->name('barangay.find');
Route::post('barangay/destroy/{id?}', [BarangayEquipmentController::class, 'destroy'])->name('barangay.destroy');
Route::get('barangay/select', [BarangayEquipmentController::class, 'select'])->name('barangay.select');
Route::get('barangay/qnty/{id?}', [BarangayEquipmentController::class, 'qnty'])->name('barangay.qnty');
Route::post('barangay/stock-in', [BarangayEquipmentController::class, 'stockIn'])->name('barangay.stockIn');
Route::post('barangay/stock-out', [BarangayEquipmentController::class, 'stockOut'])->name('barangay.stockOut');

//user_management
Route::get('user-management/', [AdminUserManagementController::class, 'index'])->name('users.index');
Route::get('user-management/store', [AdminUserManagementController::class, 'store'])->name('users.store');
Route::get('user-management/list', [AdminUserManagementController::class, 'list'])->name('users.list');
Route::get('user-management/find/{id?}', [AdminUserManagementController::class, 'find'])->name('users.find');
Route::delete('user-management/destroy/{id?}', [AdminUserManagementController::class, 'destroy'])->name('users.destroy');

//announcement
Route::get('announcement/', [AnnouncementController::class, 'index'])->name('announcement.index');
Route::post('announcement/store', [AnnouncementController::class, 'store'])->name('announcement.store');
Route::get('announcement/list', [AnnouncementController::class, 'list'])->name('announcement.list');
Route::get('announcement/find/{id?}', [AnnouncementController::class, 'find'])->name('announcement.find');
Route::delete('announcement/destroy/{id?}', [AnnouncementController::class, 'destroy'])->name('announcement.destroy');
Route::post('announcement/show/{id?}', [AnnouncementController::class, 'show'])->name('announcement.show');
Route::post('announcement/hide/{id?}', [AnnouncementController::class, 'hide'])->name('announcement.hide');


//contact
Route::get('contact/', [AdminContactController::class, 'index'])->name('contact.index');
Route::post('contact/store', [AdminContactController::class, 'store'])->name('contact.store');
Route::get('contact/list', [AdminContactController::class, 'list'])->name('contact.list');
Route::get('contact/find/{id?}', [AdminContactController::class, 'find'])->name('contact.find');
Route::delete('contact/destroy/{id?}', [AdminContactController::class, 'destroy'])->name('contact.destroy');


//sport equipment request
Route::get('request-sport/', [AdminRequestSport::class, 'index'])->name('request-sport.index');
Route::get('request-sport/store', [AdminRequestSport::class, 'store'])->name('request-sport.store');
Route::get('request-sport/list', [AdminRequestSport::class, 'list'])->name('request-sport.list');
Route::post('request-sport/approave', [AdminRequestSport::class, 'approave'])->name('request-sport.approave');
Route::post('request-sport/decline', [AdminRequestSport::class, 'decline'])->name('request-sport.decline');
Route::post('request-sport/return', [AdminRequestSport::class, 'return'])->name('request-sport.return');
Route::get('request-sport/add', [AdminRequestSport::class, 'add'])->name('request-sport.add');
Route::get('sports/qnty/{id?}', [AdminRequestSport::class, 'qnty'])->name('sports.qnty');
Route::post('sports/store-request/', [AdminRequestSport::class, 'storeRequest'])->name('sports.store-request');


//barangay equipment request
Route::get('request-barangay/', [AdminRequestBrgy::class, 'index'])->name('request-barangay.index');
Route::get('request-barangay/store', [AdminRequestBrgy::class, 'store'])->name('request-barangay.store');
Route::get('request-barangay/list', [AdminRequestBrgy::class, 'list'])->name('request-barangay.list');
Route::post('request-barangay/approave', [AdminRequestBrgy::class, 'approave'])->name('request-barangay.approave');
Route::post('request-barangay/decline', [AdminRequestBrgy::class, 'decline'])->name('request-barangay.decline');
Route::post('request-barangay/return', [AdminRequestBrgy::class, 'return'])->name('request-barangay.return');
Route::get('request-barangay/add', [AdminRequestBrgy::class, 'add'])->name('request-barangay.add');
Route::get('barangay/qnty/{id?}', [AdminRequestBrgy::class, 'qnty'])->name('barangay.qnty');
Route::post('barangay/store-request/', [AdminRequestBrgy::class, 'storeRequest'])->name('barangay.store-request');


//system management
Route::get('system', [SystemController::class, 'index'])->name('system.index');
Route::post('system/edit', [SystemController::class, 'edit'])->name('system.edit');

//barangay indigency
Route::get('barangay-indigency/', [BrgyIndigencyController::class, 'index'])->name('barangay_indigency.index');
Route::get('barangay-indigency/store', [BrgyIndigencyController::class, 'store'])->name('barangay_indigency.store');
Route::get('barangay-indigency/list', [BrgyIndigencyController::class, 'list'])->name('barangay_indigency.list');
Route::get('barangay-indigency/find/{id?}', [BrgyIndigencyController::class, 'find'])->name('barangay_indigency.find');
Route::post('barangay-indigency/approve/{id?}', [BrgyIndigencyController::class, 'approve'])->name('barangay_indigency.approve');
Route::post('barangay-indigency/disapprove/{id?}', [BrgyIndigencyController::class, 'disapprove'])->name('barangay_indigency.disapprove');

//barangay clearance
Route::get('barangay-clearance/', [BrgyClearanceController::class, 'index'])->name('barangay_clearance.index');
Route::post('barangay-clearance/store', [BrgyClearanceController::class, 'store'])->name('barangay_clearance.store');
Route::get('barangay-clearance/list', [BrgyClearanceController::class, 'list'])->name('barangay_clearance.list');
Route::get('barangay-clearance/find/{id?}', [BrgyClearanceController::class, 'find'])->name('barangay_clearance.find');
Route::post('barangay-clearance/approve/{id?}', [BrgyClearanceController::class, 'approve'])->name('barangay_clearance.approve');
Route::post('barangay-clearance/disapprove/{id?}', [BrgyClearanceController::class, 'disapprove'])->name('barangay_clearance.disapprove');

//Gym Sheduling
Route::get('Gym-scheduling/', [AdminGymController::class, 'index'])->name('Gym_scheduling.index');
Route::post('Gym-scheduling/store', [AdminGymController::class, 'store'])->name('Gym_scheduling.store');
Route::get('Gym-scheduling/list', [AdminGymController::class, 'list'])->name('Gym_scheduling.list');
Route::get('Gym-scheduling/find/{id?}', [AdminGymController::class, 'find'])->name('Gym_scheduling.find');
Route::delete('Gym-scheduling/destroy/{id?}', [AdminGymController::class, 'destroy'])->name('Gym_scheduling.destroy');
Route::post('Gym-scheduling/approve/{id?}', [AdminGymController::class, 'approve'])->name('Gym_scheduling.approve');
Route::post('Gym-scheduling/decline/{id?}', [AdminGymController::class, 'decline'])->name('Gym_scheduling.decline');


//Gym Sheduling
Route::get('reports/barangay-equpment', [ReportsController::class, 'barangay_equpment'])->name('barangay_equpment.report');
Route::get('reports/barangay-equpment/list', [ReportsController::class, 'barangay_equpment_list'])->name('barangay_equpment.report.list');

//reports
Route::get('reports/sports-equpment', [ReportsController::class, 'sports_equpment'])->name('sports_equpment.report');
Route::get('reports/sports-equpment/list', [ReportsController::class, 'sports_equpment_list'])->name('sports_equpment.report.list');

Route::get('reports/barangay-clearance', [ReportsController::class, 'barangay_clearance'])->name('barangay_clearance.report');
Route::get('reports/barangay-clearance/list', [ReportsController::class, 'barangay_clearance_list'])->name('barangay_clearance.report.list');

Route::get('reports/barangay-indigency', [ReportsController::class, 'barangay_indigency'])->name('barangay_indigency.report');
Route::get('reports/barangay-indigency/list', [ReportsController::class, 'barangay_indigency_list'])->name('barangay_indigency.report.list');



//download pdf
Route::get('download/barangay-clearance/{id?}', [ReportsController::class, 'printClearance'])->name('download.clearance');
Route::get('download/barangay-indigency/{id?}', [ReportsController::class, 'printIndigency'])->name('download.indigency');


	});
});












// User account modules
Route::group(['middleware' => ['auth', 'AuthUser']], function(){
	Route::group(['prefix' => 'user', 'as' => 'user.'], function(){
		Route::get('/dashboard', [AccountController::class, 'index'])->name('index');

		//cedula request
		Route::get('cedula/request', [CedulaRequestController::class, 'index'])->name('cedula.index');
		Route::get('cedula/store', [CedulaRequestController::class, 'store'])->name('cedula.store');
		Route::get('cedula/list', [CedulaRequestController::class, 'list'])->name('cedula.list');
		Route::delete('cedula/destroy/{id?}', [CedulaRequestController::class, 'destroy'])->name('cedula.destroy');

		//user profile settings
		Route::get('profile', [UserProfileController::class, 'index'])->name('profile.index');
		Route::post('profile/edit', [UserProfileController::class, 'edit'])->name('profile.edit');
		Route::get('profile/find/{id?}', [UserProfileController::class, 'find'])->name('profile.find');
		Route::get('profile/change-password', [UserProfileController::class, 'changePassword'])->name('profile.change.password');
		Route::post('profile/upload', [AdminProfileController::class, 'upload'])->name('profile.upload');


		//request sports equipment
		Route::get('sports', [UserSportsEquipmentController::class, 'index'])->name('sports.index');
		Route::get('sports/view', [UserSportsEquipmentController::class, 'requestView'])->name('sports.requestView');
		Route::get('sports/qnty/{id?}', [UserSportsEquipmentController::class, 'qnty'])->name('sports.qnty');
		Route::post('sports/store/', [UserSportsEquipmentController::class, 'store'])->name('sports.store');

		//request barangay equipment
		Route::get('barangay', [UserBrgyEquipmentController::class, 'index'])->name('barangay.index');
		Route::get('barangay/view', [UserBrgyEquipmentController::class, 'requestView'])->name('barangay.requestView');
		Route::get('barangay/qnty/{id?}', [UserBrgyEquipmentController::class, 'qnty'])->name('barangay.qnty');
		Route::post('barangay/store/', [UserBrgyEquipmentController::class, 'store'])->name('barangay.store');

		//request court
		Route::get('court', [CourtRequestController::class, 'index'])->name('court.index');
		Route::post('Gym-scheduling/store', [CourtRequestController::class, 'store'])->name('Gym_scheduling.store');
		Route::get('Gym-scheduling/request-view', [CourtRequestController::class, 'requestView'])->name('Gym_scheduling.request_view');


		//anouncement
		Route::get('announcement/', [AnnouncementController::class, 'userIndex'])->name('announcement.index');

		//anouncement
		Route::get('contacts/', [AdminContactController::class, 'UserContacts'])->name('contacts.index');


		Route::get('barangay-indigency/', [UserBrgyIndigencyController::class, 'index'])->name('barangay_indigency.index');
		Route::post('barangay-indigency/store', [UserBrgyIndigencyController::class, 'store'])->name('barangay_indigency.store');
		Route::get('barangay-indigency/request', [UserBrgyIndigencyController::class, 'request'])->name('barangay_indigency.request');
		Route::get('barangay-indigency/find/{id?}', [UserBrgyIndigencyController::class, 'find'])->name('barangay_indigency.find');

		Route::get('barangay-clearance/', [UserBrgyClearanceController::class, 'index'])->name('barangay_clearance.index');
		Route::post('barangay-clearance/store', [UserBrgyClearanceController::class, 'store'])->name('barangay_clearance.store');
		Route::get('barangay-clearance/request', [UserBrgyClearanceController::class, 'request'])->name('barangay_clearance.request');
		Route::get('barangay-clearance/find/{id?}', [UserBrgyClearanceController::class, 'find'])->name('barangay_clearance.find');


		Route::get('user/verify-account/', [VerifyAccountController::class, 'index'])->name('user_verify_account.index');
		Route::post('user/verify-account/store', [VerifyAccountController::class, 'store'])->name('user_verify_account.store');


		
	});
});
