<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BarangayClearance;
use Validator;
use Auth;

class UserBrgyClearanceController extends Controller
{
    public function index(){
    	return view('UserBrgyClearance.index');
    }

    public function store(Request $request){
    	$validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'years_of_residency' => 'required',
            'civil_status' => 'required',
            'purok' => 'required',
            'date' => 'required',
            'purpose' => 'required',
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{        
        	if (!empty($request->get('id'))) {
            $clearance = BarangayClearance::where('id',$request->get('id'))->first();
            $clearance->ctc_no = $request->get('ctc_no');
            $clearance->first_name = ucfirst($request->get('first_name'));
            $clearance->middle_name = ucfirst($request->get('middle_name'));
            $clearance->last_name = ucfirst($request->get('last_name'));
            $clearance->resident_from = $request->get('years_of_residency');
            $clearance->civil_status = $request->get('civil_status');
            $clearance->address = $request->get('purok');
            $clearance->purpose = $request->get('purpose');
            $clearance->date = $request->get('date');
            $clearance->user_id = Auth::user()->id;
            $clearance->save();
            }else{

            $count = BarangayClearance::where('user_id', Auth::user()->id)->where('date', today())->count();

            if ($count > 19) {
                return response()->json(['status' => false, 'message' => 'Barangay Clearance Request Already Reach The Limit, Try Again Tomorrow!']);
            }else{
                $clearance = new BarangayClearance;
                $clearance->ctc_no = $request->get('ctc_no');
                $clearance->first_name = ucfirst($request->get('first_name'));
                $clearance->middle_name = ucfirst($request->get('middle_name'));
                $clearance->last_name = ucfirst($request->get('last_name'));
                $clearance->resident_from = $request->get('years_of_residency');
                $clearance->civil_status = $request->get('civil_status');
                $clearance->address = $request->get('purok');
                $clearance->purpose = $request->get('purpose');
                $clearance->date = $request->get('date');
                $clearance->user_id = Auth::user()->id;
                $clearance->save();
                return response()->json(['status' => true]);    
            }
            }
            
        }
    }

    public function request(){
        $clearance = BarangayClearance::where('user_id', Auth::user()->id)->orderBy('date', 'desc')->get();
        return view('UserBrgyClearance.request', compact('clearance'));
    }
    
    public function find($id){ 
        $data = BarangayClearance::where('id', $id)->first();
        return response()->json(['status' => true, 'data' => $data]);
    }
}
