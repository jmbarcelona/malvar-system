<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use Validator;


class AdminContactController extends Controller
{
     public function index(){
    	return view('AdminContact.index');
    }

     public function store(Request $request){
    	$validator = Validator::make($request->all(), [
            'contact_name' => 'required',
            'contact_number' => 'required|min:11',  
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	if (!empty($request->get('id'))) {
        	$contact = Contact::where('id',$request->get('id'))->first();
        	$contact->c_name = $request->get('contact_name');
        	$contact->c_no = $request->get('contact_number');
            if (!empty($request->file('img'))) { $contact->img = $request->file('img')->store('contact_img','public'); }
        	$contact->save();
        	}else{
        	$contact = new Contact;
            if (!empty($request->file('img'))) { $contact->img = $request->file('img')->store('contact_img','public'); }
        	$contact->c_name = $request->get('contact_name');
        	$contact->c_no = $request->get('contact_number');
        	$contact->save();
        	}
        	return response()->json(['status' => true]);
        }
    }



    public function list(Request $request){
        if (!empty($request->get('filter'))) {
        $Contact = Contact::where('c_name','LIKE','%'.$request->get('filter').'%')
        ->orderBy('id', 'desc')
        ->get();
        }else{
        $Contact = Contact::orderBy('id', 'desc')->get();
        }
    	return response()->json(['status' => true, 'data' => $Contact]);
    }

    public function find($id){
		$user = Contact::where('id', $id)->first();
		return response()->json(['status' => true, 'data' => $user]);
	}

    public function destroy($id){
        $Contact = Contact::where('id', $id)->delete();
        return response()->json(['status' => true]);
    }
    
    public function UserContacts(){
      $contact =  Contact::get();
        return view('UserContacts.index', compact('contact'));
    }
}
