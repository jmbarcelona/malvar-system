<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BarangayClearance;
use Validator;
use Auth;



class BrgyClearanceController extends Controller
{
    public function index(){
    	return view('AdminBrgyClearance.index');
    }

    public function store(Request $request){
    	$validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'years_of_residency' => 'required',
            'civil_status' => 'required',
            'purok' => 'required',
            'date' => 'required',
            'purpose' => 'required',
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	if (!empty($request->get('id'))) {
        	$clearance = BarangayClearance::where('id',$request->get('id'))->first();
        	$clearance->ctc_no = $request->get('ctc_no');
        	$clearance->first_name = ucfirst($request->get('first_name'));
        	$clearance->middle_name = ucfirst($request->get('middle_name'));
        	$clearance->last_name = ucfirst($request->get('last_name'));
        	$clearance->resident_from = $request->get('years_of_residency');
        	$clearance->civil_status = $request->get('civil_status');
        	$clearance->address = $request->get('purok');
        	$clearance->purpose = $request->get('purpose');
        	$clearance->date = $request->get('date');
        	$clearance->user_id = $clearance->user_id;
        	$clearance->save();
        	}else{
        	$clearance = new BarangayClearance;
        	$clearance->ctc_no = $request->get('ctc_no');
        	$clearance->first_name = ucfirst($request->get('first_name'));
        	$clearance->middle_name = ucfirst($request->get('middle_name'));
        	$clearance->last_name = ucfirst($request->get('last_name'));
        	$clearance->resident_from = $request->get('years_of_residency');
        	$clearance->civil_status = $request->get('civil_status');
        	$clearance->address = $request->get('purok');
        	$clearance->purpose = $request->get('purpose');
        	$clearance->date = $request->get('date');
        	$clearance->user_id = Auth::user()->id;
            $clearance->status = 2;
        	$clearance->save();
        	}
        	return response()->json(['status' => true]);
        }
    }



    public function list(Request $request){
        if (!empty($request->get('filter1')) || !empty($request->get('filter2'))) {
        $Contact = BarangayClearance::with('users')
        ->whereBetween('date', [$request->get('filter1'), $request->get('filter2')])
        ->orderBy('date', 'desc')->orderBy('status', 'desc')
        ->orderBy('id', 'desc')
        ->get();
        }else{
        $Contact = BarangayClearance::with('users')->where('date', today())
        ->orderBy('date', 'desc')
        ->orderBy('status', 'desc')
        ->orderBy('id', 'desc')
        ->get();
        }
    	return response()->json(['status' => true, 'data' => $Contact]);
    }

    public function find($id){ 
		$data = BarangayClearance::where('id', $id)->first();
		return response()->json(['status' => true, 'data' => $data]);
	}

	public function approve($id){
	$clearance = BarangayClearance::where('id', $id)->first();
	$clearance->status = 2;
	$clearance->save();
	return response()->json(['status' => true]);
	}

    public function disapprove($id){
    $clearance = BarangayClearance::where('id', $id)->first();
    $clearance->status = 3;
    $clearance->save();
    return response()->json(['status' => true]);
    }
}
