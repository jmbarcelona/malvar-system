<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\BarangayEquipmentRent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;


class DashboardController extends Controller
{
    public function index(Request $request){
    	return view('Admin.index');
    }
}