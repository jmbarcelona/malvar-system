<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gym;
use Validator;
use Auth;

class AdminGymController extends Controller
{
    public function index(){
    	 $reserve = Gym::where('date', today())->where('status', 2)->first();
 
    	
    	 return view('AdminGym.index');

    }


     public function store(Request $request){
    	$validator = Validator::make($request->all(), [
            'start_time' => 'required',
            'end_time' => 'required',
            'date' => 'required',
            'renter' => 'required',
            'reason' => 'required',
            
            
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	if (!empty($request->get('id'))) {
        	$Gym = Gym::where('id',$request->get('id'))->first();
        	
            $hasFile = Gym::whereNull('deleted_at')->where('end_time', '>',$request->get('start_time'))->first();
            $hasFile2 = Gym::whereNull('deleted_at')->where('start_time', '<',$request->get('end_time'))->first();
            if ($hasFile == null || $hasFile2 == null) {
                $Gym = new Gym;
                $Gym->start_time = $request->get('start_time');
                $Gym->end_time = $request->get('end_time');
                $Gym->reason = $request->get('reason');
                $Gym->date = $request->get('date');
                $Gym->renter = ucfirst($request->get('renter'));
                $Gym->amount = $request->get('end_time') - $request->get('start_time').',000';
                $Gym->save();
            }else{
                return response()->json(['status' => false, 'message' => 'This time schedule already reserved, please check reserved table']);
            }
        	}else{

            $hasFile = Gym::where('status', 2)->whereNull('deleted_at')->where('date', '=', $request->get('date'))->where('end_time', '>',$request->get('start_time'))->first();
            $hasFile2 = Gym::where('status', 2)->whereNull('deleted_at')->where('date', '=', $request->get('date'))->where('start_time', '<',$request->get('end_time'))->first();
            if ($hasFile == null || $hasFile2 == null) {
                $Gym = new Gym;
                $Gym->start_time = $request->get('start_time');
                $Gym->end_time = $request->get('end_time');
                $Gym->reason = $request->get('reason');
                $Gym->date = $request->get('date');
                $Gym->amount = $request->get('end_time') - $request->get('start_time').',000';
                $Gym->user_id = Auth::user()->id;
                 $Gym->renter = ucfirst($request->get('renter'));
                $Gym->status = 2;
                $Gym->save();
            }else{
                return response()->json(['status' => false, 'message' => 'This time schedule already reserved, please check reserved table']);
            }

        	
        	}
        	return response()->json(['status' => true]);
        }
    }



    public function list(Request $request){
        if (!empty($request->get('filter1')) && !empty($request->get('filter2')) ) {
        $Gym = Gym::with('users')
        ->whereBetween('date', [$request->get('filter1'), $request->get('filter2')])
        ->orderBy('created_at', 'desc')
        ->get();
        }elseif (!empty($request->get('filter1'))) {
            $Gym = Gym::with('users')
        ->where('date', '=', $request->get('filter1'))
        ->orderBy('created_at', 'desc')
        ->get();
        }else{
        $Gym = Gym::with('users')->where('date', today())->orderBy('created_at', 'desc')->get();
        }
    	return response()->json(['status' => true, 'data' => $Gym]);
    }

    public function find($id){
		$user = Gym::where('id', $id)->first();
		return response()->json(['status' => true, 'data' => $user]);
	}

    

    public function approve($id){

    	$Gym = Gym::where('id', $id)->first();
        $hasFile = Gym::where('status', 2)->whereNull('deleted_at')->where('date', '=', $Gym->date)->where('end_time', '>',$Gym->start_time)->first();
            $hasFile2 = Gym::where('status', 2)->whereNull('deleted_at')->where('date', '=', $Gym->date)->where('start_time', '<',$Gym->end_time)->first();

        if ($hasFile == null || $hasFile2 == null) {
            $Gym->status = 2;
            $Gym->deleted_at = null;
            $Gym->save();
            return response()->json(['status' => true, 'message' => 'Request approve successfully!']);
        }else{
            return response()->json(['status' => false, 'message' => 'This time schedule already reserved!']);
        }

    }

    public function decline($id){
    	$Gym = Gym::where('id', $id)->first();
        $Gym->deleted_at = now();
        $Gym->save();
        return response()->json(['status' => true]);
    }
}
