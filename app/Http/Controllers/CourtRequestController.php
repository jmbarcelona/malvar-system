<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gym;
use Validator;
use Auth;


class CourtRequestController extends Controller
{
    public function index(){
    	$gym_today = Gym::with('users')->where('status', 2)->whereNull('deleted_at')->where('date', today())->get();
    	return view('UserRequestCourt.index', compact('gym_today'));
    }

    public function store(Request $request){
    	$validator = Validator::make($request->all(), [
            'start_time' => 'required',
            'end_time' => 'required',
            'date' => 'required',
            'reason' => 'required',
            
            
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	if (!empty($request->get('id'))) {
        	$Gym = Gym::where('id',$request->get('id'))->first();
        	
            $hasFile = Gym::where('date', '=', $request->get('date'))->where('end_time', '>',$request->get('start_time'))->first();
            $hasFile2 = Gym::where('date', '=', $request->get('date'))->where('start_time', '<',$request->get('end_time'))->first();
            if ($hasFile == null || $hasFile2 == null) {
                $Gym = new Gym;
                $Gym->start_time = $request->get('start_time');
                $Gym->end_time = $request->get('end_time');
                $Gym->reason = $request->get('reason');
                $Gym->date = $request->get('date');
                $Gym->amount = $request->get('end_time') - $request->get('start_time').',000';
                $Gym->save();
            }else{
                return response()->json(['status' => false, 'message' => 'This time schedule already reserved, please check reserved table']);
            }
        	}else{

            $hasFile = Gym::where('status', 2)->whereNull('deleted_at')->where('date', '=', $request->get('date'))->where('end_time', '>',$request->get('start_time'))->first();
            $hasFile2 = Gym::where('status', 2)->whereNull('deleted_at')->where('date', '=', $request->get('date'))->where('start_time', '<',$request->get('end_time'))->first();
            if ($hasFile == null || $hasFile2 == null) {
                $Gym = new Gym;
                $Gym->start_time = $request->get('start_time');
                $Gym->end_time = $request->get('end_time');
                $Gym->reason = $request->get('reason');
                $Gym->date = $request->get('date');
                $Gym->amount = $request->get('end_time') - $request->get('start_time').',000';
                $Gym->user_id = Auth::user()->id;
                $Gym->renter = Auth::user()->first_name.' '.Auth::user()->last_name;
                $Gym->status = 1;
                $Gym->save();
            }else{
                return response()->json(['status' => false, 'message' => 'This time schedule already reserved, please check reserved table']);
            }

        	
        	}
        	return response()->json(['status' => true]);
        }
    }

    public function requestView(){
    	$gymRecord = Gym::with('users')->where('user_id', Auth::user()->id)->orderBy('date', 'desc')->get();
    	return view('UserRequestCourt.requestView', compact('gymRecord'));
   }
}
