<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\System;
use Validator;

class SystemController extends Controller
{
    public function index(){
    $user = User::where('type', 2)->first();
    $system = System::first();
    return view('AdminSystemManagement.index', compact('user', 'system'));

    }

    public function edit(Request $request){
    	
    	

    	 $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'address' => 'required',
            'gender' => 'required',
            'contact' => 'required',
            'email' => 'required',

        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        if (!empty($request->get('id'))) {
            $user = User::where('id', $request->get('id'))->first();
            $user->address = $request->get('address');
            $user->first_name = $request->get('full_name');
            $user->gender = $request->get('gender');
            $user->contact = $request->get('contact');
            $user->email_address = $request->get('email');
            $user->password = 123456;
            $user->save();


            if (!empty($request->get('system_id'))) {
                $system = System::where('id', $request->get('system_id'))->first();
                if (!empty($request->file('img'))) { $system->logo = $request->file('img')->store('system_logo','public');}
                $system->system_name = $request->get('system_name');
                $system->save();
            }else{
                $system = new System;
                if (!empty($request->file('img'))) { $system->logo = $request->file('img')->store('system_logo','public');}
                $system->system_name = $request->get('system_name');
                $system->save();

            }
            
                
        }else{
            $user = new User;
            $user->address = $request->get('address');
            $user->first_name = $request->get('full_name');
            $user->gender = $request->get('gender');
            $user->contact = $request->get('contact');
            $user->email_address = $request->get('email');
            $user->password = 123456;
            $user->type = 2;
            $user->save();

            if (!empty($request->get('system_id'))) {
                $system = System::where('id', $request->get('system_id'))->first();
                if (!empty($request->file('img'))) { $system->logo = $request->file('img')->store('system_logo','public');}
                $system->system_name = $request->get('system_name');
                $system->save();
            }else{
                $system = new System;
                if (!empty($request->file('img'))) { $system->logo = $request->file('img')->store('system_logo','public');}
                $system->system_name = $request->get('system_name');
                $system->save();

            }

        	}
        	return response()->json(['status' => true, 'message' => 'Systems changes successfully applied!']);


        }
    }
}
