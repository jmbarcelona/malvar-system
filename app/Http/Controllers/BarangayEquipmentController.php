<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\BarangayEquipment;
use App\Models\BarangayReport;



class BarangayEquipmentController extends Controller
{
    public function index(){
    	return view('AdminBrgyEquipment.index');
    }

     public function store(Request $request){
    	$validator = Validator::make($request->all(), [
            'equipment_name' => 'required',
            'quantity' => 'required|numeric|min:1',
            'description' => 'required|min:3|max:50',

        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	if (!empty($request->get('id'))) {
        	$SportsEquipment = BarangayEquipment::where('id',$request->get('id'))->first();
        	$SportsEquipment->e_name = ucfirst(strtolower($request->get('equipment_name')));
            $SportsEquipment->qnty = $request->get('quantity');
        	$SportsEquipment->description = $request->get('description');
        	$SportsEquipment->save();
        	}else{
        	$SportsEquipment = new BarangayEquipment;
        	$SportsEquipment->e_name = ucfirst(strtolower($request->get('equipment_name')));
        	$SportsEquipment->qnty = $request->get('quantity');
            $SportsEquipment->description = $request->get('description');
        	$SportsEquipment->save();
        	}
        	return response()->json(['status' => true]);
        }
    }



    public function list(Request $request){
        if (!empty($request->get('filter'))) {
        $BarangayEquipment = BarangayEquipment::whereNull('deleted_at')->where('e_name','LIKE','%'.$request->get('filter').'%')
        ->orderBy('id', 'desc')
        ->get();
        }else{
        $BarangayEquipment = BarangayEquipment::whereNull('deleted_at')->orderBy('id', 'desc')->get();
        }
    	return response()->json(['status' => true, 'data' => $BarangayEquipment]);
    }

    public function find($id){
		$user = BarangayEquipment::where('id', $id)->first();
		return response()->json(['status' => true, 'data' => $user]);
	}

    public function destroy($id){
        $BarangayEquipment = BarangayEquipment::where('id', $id)->first();
        $BarangayEquipment->deleted_at = now();
        $BarangayEquipment->save();
        return response()->json(['status' => true]);
    }


    public function select(){
       $BarangayEquipment = BarangayEquipment::whereNull('deleted_at')->get();
       return response()->json(['status' => true, 'data' => $BarangayEquipment]);
    }

    public function qnty($id){
        $BarangayEquipment = BarangayEquipment::where('id', $id)->first();
       return response()->json(['status' => true, 'data' => $BarangayEquipment]);
    }

    public function stockIn(Request $request){
        $validator = Validator::make($request->all(), [
            'selection_for_stock_in' => 'required',
            'add_for_stock_in' => 'required|numeric|min:1',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            $BarangayEquipment = BarangayEquipment::where('id', $request->get('selection_for_stock_in'))->first();
            $totalBarangayEquipment = $BarangayEquipment->qnty + $request->get('add_for_stock_in');
            $BarangayEquipment->qnty = $totalBarangayEquipment;
            $BarangayEquipment->save();

                 $report = new BarangayReport;
                 $report->barangay_equipment_id = $request->get('selection_for_stock_in');
                 $report->report_name = "Stock in";
                 $report->quantity = $request->get('add_for_stock_in');
                 $report->reason = $request->get('reason');
                 $report->datetime = now();
                 $report->date = now();
                 $report->save();

            return response()->json(['status' => true]);
        }
    }

     public function stockOut(Request $request){
        $validator = Validator::make($request->all(), [
            'selection_for_stock_out' => 'required',
            'minus_for_stock_in' => 'required|numeric|min:1',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            $BarangayEquipment = BarangayEquipment::where('id', $request->get('selection_for_stock_out'))->first();
            $totalBarangayEquipment = $BarangayEquipment->qnty - $request->get('minus_for_stock_in');
            if ($request->get('minus_for_stock_in') > $BarangayEquipment->qnty) {
                return response()->json(['status' => false, 'message' => 'Stock out failed, stock out quantity can not be greater than current stock!']);
                
            }else{
                $BarangayEquipment->qnty = $totalBarangayEquipment;
                $BarangayEquipment->save();

                 $report = new BarangayReport;
                 $report->barangay_equipment_id = $request->get('selection_for_stock_out');
                 $report->report_name = "Stock out";
                 $report->quantity = $request->get('minus_for_stock_in');
                 $report->reason = $request->get('reason');
                 $report->datetime = now();
                 $report->date = now();
                 $report->save();
                return response()->json(['status' => true, 'message' => 'Equipment Stock out Successfully!']);
            }
            

            
        }
    }
}
