<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\SportEquipmentRent;
use App\Models\SportEquipment;
use App\Models\SportReport;
use App\Models\User;

class AdminRequestSport extends Controller
{
     public function index(){
    	return view('AdminRequestSport.index');
    }

     public function store(Request $request){
    	$validator = Validator::make($request->all(), [
            'equipment_name' => 'required',
            'quantity' => 'required',
            
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	if (!empty($request->get('id'))) {
        	$SportsEquipment = SportEquipment::where('id',$request->get('id'))->first();
        	$SportsEquipment->e_name = ucfirst(strtolower($request->get('equipment_name')));
        	$SportsEquipment->qnty = $request->get('quantity');
        	$SportsEquipment->save();
        	}else{
        	$SportsEquipment = new SportEquipment;
        	$SportsEquipment->e_name = ucfirst(strtolower($request->get('equipment_name')));
        	$SportsEquipment->qnty = $request->get('quantity');
        	$SportsEquipment->save();
        	}
        	return response()->json(['status' => true]);
        }
    }



    public function list(Request $request){
       if (!empty($request->get('filter1')) && !empty($request->get('filter2')) && empty($request->get('status_filter'))) {
        
        $SportEquipment = SportEquipmentRent::with('users')->with('SportEquipment')->whereBetween('date', [$request->get('filter1') , $request->get('filter2')] )->orderBy('date', 'desc')->orderBy('status', 'asc')->get();

        }elseif(!empty($request->get('filter1')) && empty($request->get('filter2')) && empty($request->get('status_filter'))){

          $SportEquipment = SportEquipmentRent::with('users')->with('SportEquipment')->where('date', '=', $request->get('filter1') )->orderBy('date', 'desc')->orderBy('status', 'asc')->get();

        }elseif (!empty($request->get('filter1')) && !empty($request->get('filter2')) && !empty($request->get('status_filter')) ) {

        $SportEquipment = SportEquipmentRent::with('users')->with('SportEquipment')->where('status', '=', $request->get('status_filter'))->whereBetween('date', [$request->get('filter1') , $request->get('filter2')] )->orderBy('date', 'desc')->orderBy('status', 'asc')->get();    

        }elseif (!empty($request->get('status_filter'))) {
          $SportEquipment = SportEquipmentRent::with('users')->with('SportEquipment')->where('date', today())->where('status', '=', $request->get('status_filter') )->orderBy('date', 'desc')->orderBy('status', 'asc')->get();
        }else{

        $SportEquipment = SportEquipmentRent::with('users')->with('SportEquipment')->where('date', today())->orderBy('date', 'desc')->orderBy('status', 'asc')->get();

        }
    	return response()->json(['status' => true, 'data' => $SportEquipment]);
    }

   
   public function approave(Request $request){
       $rentApproave = SportEquipmentRent::where('id', $request->get('rent_id'))->first();
       $minusStock = SportEquipment::where('id', $request->get('equipment_id'))->first();
       if ($minusStock->qnty <  $rentApproave->qnty) {
          return response()->json(['status' => false, 'message' => 'Not enough stock for this requested equipment, Check the current stock please!']);
       }else{
       $rentApproave->status = 1;
       $rentApproave->save();
       $minusThis = $minusStock->qnty - $rentApproave->qnty;
       $minusStock->qnty = $minusThis;
       $minusStock->save();

         $report = new SportReport;
         $report->sport_equipment_id = $request->get('equipment_id');
         $report->report_name = "Stock out / Requested";
         $report->quantity =  $rentApproave->qnty;
         $report->reason = "Requested by user";
         $report->datetime = now();
         $report->date = now();
         $report->save();

       return response()->json(['status' => true]);
       }       
   }

   public function decline(Request $request){
       $rentApproave = SportEquipmentRent::where('id', $request->get('rent_id'))->first();
       
       $rentApproave->status = 2;
       $rentApproave->save();
       return response()->json(['status' => true]);
   }

   public function return(Request $request){
       $rentApproave = SportEquipmentRent::where('id', $request->get('rent_id'))->first();
       $minusStock = SportEquipment::where('id', $request->get('equipment_id'))->first();
       $rentApproave->status = 3;
       $rentApproave->save();
       $minusThis = $minusStock->qnty + $rentApproave->qnty;
       $minusStock->qnty = $minusThis;
       $minusStock->save();


     $report = new SportReport;
     $report->sport_equipment_id = $request->get('equipment_id');
     $report->report_name = "Stock in / Return";
     $report->quantity =  $rentApproave->qnty;
     $report->reason = "Requested by user";
     $report->datetime = now();
     $report->date = now();
     $report->save();
       return response()->json(['status' => true]);
             
   }

   public function add(){
    $SportEquipment = SportEquipment::whereNull('deleted_at')->get();
    return view('AdminRequestSport.addRequestSport', compact('SportEquipment'));
   }

     public function qnty($id){
        $SportEquipment = SportEquipment::where('id', $id)->first();
        return response()->json(['status' => true, 'data' => $SportEquipment]);
    }

    public function storeRequest(Request $request){

        $validator = Validator::make($request->all(), [
            'sports_equipments' => 'required',
            'request_quantity' => 'required',
            'date' => 'required',
            'reason' => 'required',
            'borrower' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{

            if ($request->get('request_quantity') > $request->get('current_quantity')) {
                return response()->json(['status' => false, 'message' => 'Not enough stock for this equipment!']);
            }else{
            $rent = new SportEquipmentRent;
            $rent->user_id = Auth::user()->id;
            $rent->sport_equipment_id = $request->get('sports_equipments');
            $rent->qnty = $request->get('request_quantity');
            $rent->date = $request->get('date');
            $rent->e_name = $request->get('e_name');
            $rent->reason = $request->get('reason');
            $rent->renter = $request->get('borrower');
            $rent->status = 1;
            $rent->save();

            $equipment = SportEquipment::where('id', $request->get('sports_equipments'))->first();
            $total_qnty = $equipment->qnty - $request->get('request_quantity');
            $equipment->qnty = $total_qnty;
            $equipment->save();

            return response()->json(['status' => true, 'message' => 'Equipment requested successfully!']);
            }
            
        }
    }

}
