<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\SportEquipment;
use App\Models\SportEquipmentRent;

class UserSportsEquipmentController extends Controller
{
    public function index(){
    	$SportEquipment = SportEquipment::whereNull('deleted_at')->get();
    	return view('UserSportEquipment.index', compact('SportEquipment'));
    }

    public function qnty($id){
    	$SportEquipment = SportEquipment::where('id', $id)->first();
    	return response()->json(['status' => true, 'data' => $SportEquipment]);
    }

    public function store(Request $request){

    	$validator = Validator::make($request->all(), [
            'sports_equipments' => 'required',
            'request_quantity' => 'required',
            'date' => 'required',
            'reason' => 'required',
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{

        	if ($request->get('request_quantity') > $request->get('current_quantity')) {
        		return response()->json(['status' => false, 'message' => 'Not enough stock for this equipment!']);
        	}else{

            $count = SportEquipmentRent::where('date', today())->where('user_id', Auth::user()->id)->count();

            if ($count > 19) {
                return response()->json(['status' => false, 'message' => 'Equipment Request Already Reach The Limit, Try Again Tomorrow!']);
            }else{
                $rent = new SportEquipmentRent;
                $rent->user_id = Auth::user()->id;
                $rent->sport_equipment_id = $request->get('sports_equipments');
                $rent->qnty = $request->get('request_quantity');
                $rent->date = $request->get('date');
                $rent->e_name = $request->get('e_name');
                $rent->reason = $request->get('reason');
                $rent->renter = Auth::user()->first_name.' '.Auth::user()->last_name;
                $rent->status = 4;
                $rent->save();
            }

			
        	return response()->json(['status' => true, 'message' => 'Equipment requested successfully!']);
        	}
        	
        }
    }
    public function requestView(){
    	$request = SportEquipmentRent::where('user_id', Auth::user()->id)->orderBy('date', 'desc')->get();
    	return view('UserSportEquipment.viewRequest', compact('request'));

    }
}
