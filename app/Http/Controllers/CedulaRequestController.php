<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\Cedula;


class CedulaRequestController extends Controller
{
    public function index(){
    	return view('userCedulaRequest.index');
    }

    public function store(Request $request){
    	$validator = Validator::make($request->all(), [
            'last_name' => 'required',
            'first_name' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'citizenship' => 'required',
            'bplace' => 'required',
            'bday' => 'required',
            'civil_status' => 'required',
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        		$cedula = new Cedula;
                $cedula->last_name = $request->get('last_name');
                $cedula->first_name = $request->get('first_name');
                $cedula->middle_name = $request->get('middle_name');
                $cedula->address = $request->get('address');
                $cedula->gender = $request->get('gender');
                $cedula->citizenship = $request->get('citizenship');
                $cedula->bday = $request->get('bday');
                $cedula->bplace = $request->get('bplace');
                $cedula->civil_status = $request->get('civil_status');
                $cedula->height = $request->get('height');
                $cedula->weight = $request->get('weight');
                $cedula->date_requested = now();
                $cedula->user_id = Auth::user()->id;
                if($cedula->save()){
                    return response()->json(['status' => true, 'message' => 'Users saved successfully!']);
                }
        }
    }

    public function list(Request $request){
        if (!empty($request->get('filter'))) {
        $cedula = Cedula::where('user_id', Auth::user()->id)
        ->where('last_name','LIKE','%'.$request->get('filter').'%')
        ->orWhere('first_name','LIKE','%'.$request->get('filter').'%')
        ->orWhere('middle_name','LIKE','%'.$request->get('filter').'%')->orderBy('id', 'desc')->get();
        }else{
        $cedula = Cedula::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
        }
    	return response()->json(['status' => true, 'data' => $cedula]);
    }

    public function destroy($id){
        $cedula = Cedula::where('id', $id)->delete();
        return response()->json(['status' => true]);
    }
}
