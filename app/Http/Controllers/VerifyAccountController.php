<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Validator;

class VerifyAccountController extends Controller
{
    public function index(){
        return view('UserVerify.index');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'proof_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            
            $user = User::find(Auth::user()->id);
            $user->status = 2;
            $user->proof_img = $request->file('proof_img')->store('proof_img','public');
            $user->save();
            return response()->json(['status' => true, 'message' => 'Account Updated Successfully!']);

            }

            }
    }

