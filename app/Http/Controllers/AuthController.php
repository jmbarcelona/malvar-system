<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\System;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgetPassword;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{


    public function index(){
    	return view('Auth.index');
    }

    public function check_login(Request $request){
    	$email_address = $request->get('email_address');
    	$password = $request->get('password');
    	$validator = Validator::make($request->all(), [
			'email_address' => 'required|email',
			'password' => 'required:min:7'
		]);
		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			$redirect_url = '';
			$credentials = ['email_address' => $request->get('email_address'), 'password' => $request->get('password'), 'deleted_at' => null];
			if (Auth::attempt($credentials)) {
				 /*If username and password is correct it will start and set the session or also known as Auth*/
				 $request->session()->regenerate();
				 $user = Auth::user(); /*$_SESSION['auth']*/
				 if ($user->type === 3) {
				 	$redirect_url = route('user.index');
				 }else{
				 	$redirect_url = route('admin.index');
				 }
				 return response()->json(['status' => true, 'redirect' => $redirect_url]);
			}else{
				$validator->errors()->add('password','Invalid Account!');
	            return response()->json(['status' => false, 'error' => $validator->errors()]);	
			}
		}
    }

    public function logoutUser(){
		Session::flush();
    	Auth::logout();
		return redirect(route('login'));
	}

	public function registration(){
		return view('Auth.register');
	}

	public function registrationAdd(Request $request){
		$validator = Validator::make($request->all(), [
			'first_name' => 'required',
			'last_name' => 'required',
			'gender' => 'required',
			'contact' => 'required|min:11|max:11',
			'address' => 'required',
			'email_address' => 'required|email',
			'password' => 'required|min:7',
			'confirm_password' => 'required|min:7',
			'bday' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if ($request->get('password') != $request->get('confirm_password')) {
				return response()->json(['status' => false, 'error' => $validator->errors()->add('confirm_password', 'Passwords did not match!')]);
			}else{
				$data = request()->all();
				User::create($data);
				return response()->json(['status' => true, 'message' => 'Account Made Successfully!']);
			}
		}
	}

	public function forgetPass(){
		return view('Auth.forgetPassword');
	}

	public function forgetPassSave(Request $request){
		$validator = Validator::make($request->all(), [
			'email_address' => 'required|email',
		]);
		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			$user = User::where('email_address', $request->get('email_address'));
			$get_name = $user->first();

			if ($user->count() > 0) {
				
				$system = System::first();

				$data = [
	                'subject' => 'Forget Password',
	                'name' => $get_name->first_name,
	                'system' => $system->system_name,
	                'email' => $request->get('email_address'),
	                'link' => route('auth.change_password'),
	                'id' => $get_name->id,
	                'view' => 'Email.forgetPassword',
	            ];

	            $sentMail = Mail::to($request->get('email_address'))->send(new ForgetPassword($data));

	            if ($sentMail){
	                return response()->json(['status' => true, 'message' => 'Request Sent, Please check your email!']);
	            }
			}else{
				return response()->json(['status' => false, 'message' => 'Invalid Email Address!']);
			}
		
			

		}
	}

	public function changePass(){
		return view('Auth.changePassword');
	}

	public function changePassSave(Request $request, $id){
		$old_pass = $request->get('old_pass');
		$new_pass = $request->get('new_pass');
		$confirm_pass = $request->get('confirm_pass');
	$validator = Validator::make($request->all(), [
            'old_pass' => 'required|min:7',
			'new_pass' => 'required|min:7',
			'confirm_pass' => 'required|min:7',
			
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        $user = User::where('id' , $id)->first();
		if (Hash::check($old_pass, $user->password)) {
			if ($new_pass === $confirm_pass) {
			$user = User::where('id', $id)->first();
			$user->password = $new_pass;
			if ($user->save()) {
				return response()->json(['status' => true, 'message' => "Password updated successfully"]);
			}
			}else{
				return response()->json(['status' => false, 'message' => "New passwords do not match"]);
			}
		}else{
			return response()->json(['status' => false, 'message' => 'Old password do not match']);
		}
        		
        }

	}

	function resetPassSave(Request $request, $id){
	$new_pass = $request->get('new_pass');
	$confirm_pass = $request->get('confirm_pass');

	$validator = Validator::make($request->all(), [
			'new_pass' => 'required|min:7',
			'confirm_pass' => 'required|min:7',
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	$user = User::where('id' , $id)->first();

			if ($new_pass === $confirm_pass) {

			$user = User::where('id', $id)->first();

			$user->password = $new_pass;

			if ($user->save()) {

				return response()->json(['status' => true, 'message' => "Password updated successfully"]);
			}

			}else{

				return response()->json(['status' => false, 'message' => "New passwords do not match"]);
			}
        }
	}

}
