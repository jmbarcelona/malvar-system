<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;


class AdminVerifyController extends Controller
{
    public function index(){
        return view('AdminVerify.index');
    }

    public function list(Request $request){
        if (!empty($request->get('status_filter'))) {
        
        $User = User::whereNull('deleted_at')->where('type', 3)->where('status','!=', 1)->where('status', $request->get('status_filter'))->orderBy('status', 'asc')->orderBy('id', 'desc')->get();
        }else{
        $User = User::whereNull('deleted_at')->where('type', 3)->where('status','!=', 1)->orderBy('id', 'desc')->get();
        }
        return response()->json(['status' => true, 'data' => $User]);
    }

    public function find(Request $request, $id){
        $img = User::find($id);
        return response()->json(['status' => true, 'data' => $img]);
    }

    public function approve(Request $request, $id){
        $img = User::find($id);
        $img->status = 3;
        $img->save();
        return response()->json(['status' => true]);
    }

    public function disapprove(Request $request, $id){
        $img = User::find($id);
        $img->status = 4;
        $img->save();
        return response()->json(['status' => true]);
    }
}
