<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\User;


class AdminUserManagementController extends Controller
{
     public function index(){
    	return view('AdminUserManagement.index');
    }

     public function store(Request $request){
     if (!empty($request->get('id'))) {
     	$validator = Validator::make($request->all(), [
            'first_name' => 'required',
			'last_name' => 'required',
			'gender' => 'required',
			'contact' => 'required|min:11',
			'address' => 'required',
			'bday' => 'required'
            
        ]);
     }else{
     	$validator = Validator::make($request->all(), [
            'first_name' => 'required',
			'last_name' => 'required',
			'gender' => 'required',
			'contact' => 'required|min:11|max:11',
			'address' => 'required',
			'email_address' => 'required|email',
			'password' => 'required|min:7',
			'confirm_password' => 'required|min:7 ',
			'bday' => 'required'
            
        ]);
     }
    	
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	if (!empty($request->get('id'))) {
        		if ($request->get('password') != $request->get('confirm_password')) {
				return response()->json(['status' => false, 'error' => $validator->errors()->add('confirm_password', 'Passwords did not match!')]);
			}else{
				$user = User::where('id',$request->get('id'))->first();
        	$user->first_name = $request->get('first_name');
        	$user->middle_name = $request->get('middle_name');
        	$user->last_name = $request->get('last_name');
        	$user->bday = $request->get('bday');
        	$user->address = $request->get('address');
        	$user->contact = $request->get('contact');
        	$user->gender = $request->get('gender');
        	$user->save();
        	return response()->json(['status' => true, 'message' => 'Account Updated Successfully!']);
			}
        	}else{

        	if ($request->get('password') != $request->get('confirm_password')) {
				return response()->json(['status' => false, 'error' => $validator->errors()->add('confirm_password', 'Passwords did not match!')]);
			}else{
				$user = request()->all();
				User::create($user);
        	return response()->json(['status' => true, 'message' => 'Account Created Successfully!']);

			}

        	}
        	
        }
    }



    public function list(Request $request){
        if (!empty($request->get('filter'))) {
        $User = User::whereNull('deleted_at')->where('type', 3)
        ->where('first_name','LIKE','%'.$request->get('filter').'%')
        ->orWhere('last_name','LIKE','%'.$request->get('filter').'%')
        ->where('type', 3)
        ->orWhere('address','LIKE','%'.$request->get('filter').'%')
        ->where('type', 3)
        ->orderBy('id', 'desc')
        ->get();
        }else{
        $User = User::whereNull('deleted_at')->where('type', 3)->orderBy('id', 'desc')->get();
        }
    	return response()->json(['status' => true, 'data' => $User]);
    }

    public function find($id){
		$user = User::where('id', $id)->first();
		return response()->json(['status' => true, 'data' => $user]);
	}

    public function destroy($id){
        $user = User::where('id', $id)->first();
        $user->deleted_at = now();
        $user->save();
        return response()->json(['status' => true]);
    }
}
