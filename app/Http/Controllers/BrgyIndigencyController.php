<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BarangayIndigency;
use Validator;
use Auth;



class BrgyIndigencyController extends Controller
{
    public function index(){
    	return view('AdminBrgyIndigency.index');
    }

    public function store(Request $request){
    	$validator = Validator::make($request->all(), [
            'ctc_no' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'age' => 'required',
            'civil_status' => 'required',
            'address' => 'required',
            'purpose' => 'required',
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	if (!empty($request->get('id'))) {
        	$indigency = BarangayIndigency::find($request->get('id'));
            $indigency->ctc_no = $request->get('ctc_no');
            $indigency->first_name = ucfirst($request->get('first_name'));
            $indigency->middle_name = ucfirst($request->get('middle_name'));
            $indigency->last_name = ucfirst($request->get('last_name'));
            $indigency->age = $request->get('age');
            $indigency->civil_status = $request->get('civil_status');
            $indigency->address = $request->get('address');
            $indigency->purpose = $request->get('purpose');
            $indigency->user_id = $indigency->user_id;
            
            $indigency->save();
        	}else{
        	$indigency = new BarangayIndigency;
            $indigency->ctc_no = $request->get('ctc_no');
            $indigency->first_name = ucfirst($request->get('first_name'));
            $indigency->middle_name = ucfirst($request->get('middle_name'));
            $indigency->last_name = ucfirst($request->get('last_name'));
            $indigency->age = $request->get('age');
            $indigency->civil_status = $request->get('civil_status');
            $indigency->address = $request->get('address');
            $indigency->purpose = $request->get('purpose');
            $indigency->date = now();
            $indigency->user_id = $request->get('id');
            $indigency->status = 2;
            $indigency->save();
        	}
        	return response()->json(['status' => true]);
        }
    }



    public function list(Request $request){
        if (!empty($request->get('filter1')) || !empty($request->get('filter2'))) {
        $Contact = BarangayIndigency::with('users')->whereBetween('date', [$request->get('filter1'), $request->get('filter2')])
        ->orderBy('date', 'desc')->orderBy('status', 'asc')
        ->get();
        }else{
        $Contact = BarangayIndigency::with('users')->where('date', today())->orderBy('date', 'desc')->orderBy('status', 'asc')->get();
        }
    	return response()->json(['status' => true, 'data' => $Contact]);
    }

    public function find($id){ 
		$data = BarangayIndigency::where('id', $id)->first();
		return response()->json(['status' => true, 'data' => $data]);
	}

    public function approve($id){
    $indigency = BarangayIndigency::where('id', $id)->first();
    $indigency->status = 2;
    $indigency->save();
    return response()->json(['status' => true]);
    }

    public function disapprove($id){
    $indigency = BarangayIndigency::where('id', $id)->first();
    $indigency->status = 3;
    $indigency->save();
    return response()->json(['status' => true]);
    }
}
