<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\BarangayEquipmentRent;
use App\Models\BarangayEquipment;
use App\Models\User;
use App\Models\SportReport;
use App\Models\BarangayReport;
use App\Models\BarangayClearance;
use App\Models\BarangayIndigency;
use Barryvdh\DomPDF\Facade\Pdf;
use App;


class ReportsController extends Controller
{
    public function barangay_equpment(){

			return view('AdminReports.barangay');
    }

    public function barangay_equpment_list(Request $request){
        if (!empty($request->get('filter1')) && !empty($request->get('filter1'))  ) {
        	$barangayReport = BarangayReport::join('barangay_equipment', 'barangay_reports.barangay_equipment_id', 'barangay_equipment.id')
        	->whereBetween('date', [$request->get('filter1'), $request->get('filter2') ])
      		->select('barangay_reports.*', 'barangay_equipment.e_name as e_name', 'barangay_equipment.qnty as total_qty')
            ->orderBy('datetime', 'desc')
            ->get();
        }else{

        	$barangayReport = BarangayReport::join('barangay_equipment', 'barangay_reports.barangay_equipment_id', 'barangay_equipment.id')
      		->select('barangay_reports.*', 'barangay_equipment.e_name as e_name', 'barangay_equipment.qnty as total_qty')
            ->orderBy('datetime', 'desc')
            ->get();

        }        
    	return response()->json(['status' => true, 'data' => $barangayReport]);
    }

    public function sports_equpment(){

    		return view('AdminReports.sport');
    }


    public function sports_equpment_list(Request $request){
        if (!empty($request->get('filter1')) && !empty($request->get('filter1'))  ) {
        	$SportReport = SportReport::join('sport_equipment', 'sport_reports.sport_equipment_id', 'sport_equipment.id')
        	->whereBetween('date', [$request->get('filter1'), $request->get('filter2') ])
      		->select('sport_reports.*', 'sport_equipment.e_name as e_name', 'sport_equipment.qnty as total_qty')
            ->orderBy('datetime', 'desc')
            ->get();
        }else{

        	$SportReport = SportReport::join('sport_equipment', 'sport_reports.sport_equipment_id', 'sport_equipment.id')
      		->select('sport_reports.*', 'sport_equipment.e_name as e_name', 'sport_equipment.qnty as total_qty')
            ->orderBy('datetime', 'desc')
            ->get();

        }        
    	return response()->json(['status' => true, 'data' => $SportReport]);
    }


    public function barangay_clearance(){

    		return view('AdminReports.clearance');
    }

    public function barangay_indigency(){

    		return view('AdminReports.indigency');
    }

    public function printClearance($id){

    $clearance = BarangayClearance::findOrFail($id);
    $captain = User::where('type', 2)->firstOrFail();
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadView('PDF.report_sample', compact('clearance', 'captain'));
    return $pdf->stream();

    // $pdf = PDF::loadView('PDF.report_sample')->setOptions(['defaultFont' => 'sans-serif']);
    // return $pdf->download('report_sample.pdf');
    }



    public function printIndigency($id){

    $indigency = BarangayIndigency::findOrFail($id);
    $captain = User::where('type', 2)->firstOrFail();
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadView('PDF.indigency', compact('indigency', 'captain'));
    return $pdf->stream();

    // $pdf = PDF::loadView('PDF.report_sample')->setOptions(['defaultFont' => 'sans-serif']);
    // return $pdf->download('report_sample.pdf');
    }

    public function barangay_clearance_list(Request $request){

        if (!empty($request->get('filter1')) || !empty($request->get('filter2'))) {
        $clearance = BarangayClearance::with('users')
        ->where('status', 2)
        ->whereBetween('date', [$request->get('filter1'), $request->get('filter2')])
        ->orderBy('date', 'desc')
        ->get();
        }else{
        $clearance = BarangayClearance::with('users')
        ->where('status', 2)
        ->orderBy('date', 'desc')
        ->get();

    }
    return response()->json(['status' => true, 'data' => $clearance]);


   }

   public function barangay_indigency_list(Request $request){
          if (!empty($request->get('filter1')) || !empty($request->get('filter2'))) {
        $indigency = BarangayIndigency::with('users')
        ->whereBetween('date', [$request->get('filter1'), $request->get('filter2')])
        ->orderBy('date', 'desc')->orderBy('status', 'asc')
        ->get();
        }else{
        $indigency = BarangayIndigency::with('users')
        ->where('status', 2)
        ->orderBy('date', 'desc')
        ->get();
        }
        return response()->json(['status' => true, 'data' => $indigency]);
   }
}




