<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\SportEquipment;
use App\Models\SportReport;


class SportEquipmentController extends Controller
{
    public function index(){
    	return view('AdminSportsEquipment.index');
    }

     public function store(Request $request){
    	$validator = Validator::make($request->all(), [
            'equipment_name' => 'required',
            'quantity' => 'required|numeric|min:1',
            'description' => 'required|min:3|max:50',
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	if (!empty($request->get('id'))) {

        	$SportsEquipment = SportEquipment::where('id',$request->get('id'))->first();
        	$SportsEquipment->e_name = ucfirst(strtolower($request->get('equipment_name')));
            $SportsEquipment->qnty = $request->get('quantity');
        	$SportsEquipment->description = $request->get('description');
        	$SportsEquipment->save();

          

        	}else{

        	$SportsEquipment = new SportEquipment;
        	$SportsEquipment->e_name = ucfirst(strtolower($request->get('equipment_name')));
        	$SportsEquipment->qnty = $request->get('quantity');
            $SportsEquipment->description = $request->get('description');
        	$SportsEquipment->save();
        	}
        	return response()->json(['status' => true]);
        }
    }



    public function list(Request $request){
        if (!empty($request->get('filter'))) {
        $SportEquipment = SportEquipment::whereNull('deleted_at')->where('e_name','LIKE','%'.$request->get('filter').'%')
        ->orderBy('id', 'desc')
        ->get();
        }else{
        $SportEquipment = SportEquipment::whereNull('deleted_at')->orderBy('id', 'desc')->get();
        }
    	return response()->json(['status' => true, 'data' => $SportEquipment]);
    }

    public function find($id){
		$user = SportEquipment::where('id', $id)->first();
		return response()->json(['status' => true, 'data' => $user]);
	}

    public function destroy($id){
        $SportEquipment = SportEquipment::where('id', $id)->first();
        $SportEquipment->deleted_at = now();
        $SportEquipment->save();
        return response()->json(['status' => true]);
    }

    public function select(){
       $SportEquipment = SportEquipment::whereNull('deleted_at')->get();
       return response()->json(['status' => true, 'data' => $SportEquipment]);
    }

    public function qnty($id){
        $SportEquipment = SportEquipment::where('id', $id)->first();
       return response()->json(['status' => true, 'data' => $SportEquipment]);
    }

    public function stockIn(Request $request){
        $validator = Validator::make($request->all(), [
            'selection_for_stock_in' => 'required',
            'add_for_stock_in' => 'required|numeric|min:1',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            $SportEquipment = SportEquipment::where('id', $request->get('selection_for_stock_in'))->first();
            $totalSportEquipment = $SportEquipment->qnty + $request->get('add_for_stock_in');
            $SportEquipment->qnty = $totalSportEquipment;
            $SportEquipment->save();

             $report = new SportReport;
             $report->sport_equipment_id = $request->get('selection_for_stock_in');
             $report->report_name = "Stock in";
             $report->quantity = $request->get('add_for_stock_in');
             $report->reason = $request->get('reason');
             $report->datetime = now();
             $report->date = now();
             $report->save();

            return response()->json(['status' => true]);
        }
    }

     public function stockOut(Request $request){
        $validator = Validator::make($request->all(), [
            'selection_for_stock_out' => 'required',
            'minus_for_stock_in' => 'required|numeric|min:1',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            $SportEquipment = SportEquipment::where('id', $request->get('selection_for_stock_out'))->first();
            $totalSportEquipment = $SportEquipment->qnty - $request->get('minus_for_stock_in');
            if ($request->get('minus_for_stock_in') > $SportEquipment->qnty) {
                 return response()->json(['status' => false, 'message' => 'Stock out failed, stock out quantity can not be greater than current stock!']);
            }else{
                $SportEquipment->qnty = $totalSportEquipment;
                $SportEquipment->save();

             $report = new SportReport;
             $report->sport_equipment_id = $request->get('selection_for_stock_out');
             $report->report_name = "Stock out";
             $report->quantity = $request->get('minus_for_stock_in');
             $report->reason = $request->get('reason');
             $report->datetime = now();
             $report->date = now();
             $report->save();
                return response()->json(['status' => true, 'message' => 'Equipment Stock out Successfully!']);
            }

            
        }
    }
}
