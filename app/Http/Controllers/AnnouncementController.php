<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Announcement;
use Validator;

class AnnouncementController extends Controller
{
    public function index(){
    	$announcement = Announcement::first();
    	return view('AdminAnnouncement.index', compact('announcement'));
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'announcement_name' => 'required',
            'date' => 'required',
            'description' => 'required',



        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
            if (!empty($request->get('id'))) {
            $Announcement = Announcement::where('id',$request->get('id'))->first();
            $Announcement->a_name = ucfirst(strtolower($request->get('announcement_name')));
            $Announcement->description = $request->get('description');
            $Announcement->date = $request->get('date');
            if (!empty($request->file('img'))) { $Announcement->img = $request->file('img')->store('announcement_img','public'); }
            $Announcement->save();
            }else{
            $Announcement = new Announcement;
            $Announcement->a_name = ucfirst(strtolower($request->get('announcement_name')));
            $Announcement->description = $request->get('description');
            $Announcement->date = $request->get('date');
            if (!empty($request->file('img'))) { $Announcement->img = $request->file('img')->store('announcement_img','public'); }
            $Announcement->save();
            }
            return response()->json(['status' => true]);
        }
    }

    public function list(Request $request){
        
        $Announcement = Announcement::orderBy('date', 'desc')->get();
        
        return response()->json(['status' => true, 'data' => $Announcement]);
    }

    public function find($id){
        $user = Announcement::where('id', $id)->first();
        return response()->json(['status' => true, 'data' => $user]);
    }

    public function destroy($id){
        $Announcement = Announcement::where('id', $id)->delete();
        return response()->json(['status' => true]);
    }

    public function userIndex(){
      $announcement =  Announcement::orderBy('date', 'desc')->get();
        return view('UserAnnouncement.index', compact('announcement'));
    }

    public function show($id){

       $announcement = Announcement::where('id', $id)->first();
       $announcement->status = 2;
       $announcement->save();
       return response()->json(['status' => true]);

    }

    public function hide($id){
       $announcement = Announcement::where('id', $id)->first();
       $announcement->status = 1;
       $announcement->save();
       return response()->json(['status' => true]);

    }


}
