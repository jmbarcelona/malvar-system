<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Auth;
use Illuminate\Support\Facades\Hash;


class AdminProfileController extends Controller
{
    public function index(){
    	return view('AdminProfile.index');
    }
    public function find($id){
		$user = User::where('id', $id)->first();
		return response()->json(['status' => true, 'data' => $user]);
	}

	public function edit(Request $request){
		$validator = Validator::make($request->all(), [
            'first_name' => 'required',
			'last_name' => 'required',
			'gender' => 'required',
			'contact' => 'required',
			'address' => 'required',
			'email_address' => 'required|email',
			'bday' => 'required'
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	$user = User::where('id', Auth::user()->id )->first();
        	$user->first_name = $request->get('first_name');
        	$user->middle_name = $request->get('middle_name');
        	$user->last_name = $request->get('last_name');
        	$user->gender = $request->get('gender');
        	$user->contact = $request->get('contact');
        	$user->address = $request->get('address');
        	$user->bday = $request->get('bday');
        	$user->email_address = $request->get('email_address');
        	$user->save();
       		return response()->json(['status' => true]);
        		
        }
	}

	public function changePassword(Request $request){
		$old_pass = $request->get('old_pass');
		$new_pass = $request->get('new_pass');
		$confirm_pass = $request->get('confirm_pass');
	$validator = Validator::make($request->all(), [
            'old_pass' => 'required',
			'new_pass' => 'required|min:7',
			'confirm_pass' => 'required|min:7',
			
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        $user = User::where('id' , $request->get('user_id2'))->first();
		if (Hash::check($old_pass, $user->password)) {
			if ($new_pass === $confirm_pass) {
			$user = User::where('id', $request->get('user_id2'))->first();
			$user->password = $new_pass;
			if ($user->save()) {
				return response()->json(['status' => true, 'message' => "Password updated successfully"]);
			}
			}else{
				return response()->json(['status' => false, 'message' => "New passwords do not match"]);
			}
		}else{
			return response()->json(['status' => false, 'message' => 'Old password do not match']);
		}
        		
        }

	}

	public function upload(Request $request){

		if ($request->hasFile('img')) {
			    $user = User::where('id', Auth::user()->id)->first();
			   	$user->user_img = $request->file('img')->store('admin_img','public');
			   	$user->save();
			   	return response()->json(['status' => true, 'message' => 'Profile Image Uploaded Successfully']);
			}else{
			   	return response()->json(['status' => false, 'message' => 'Please select an image first before uploading!']);

			}
	
	}
}
