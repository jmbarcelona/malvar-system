<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\BarangayEquipmentRent;
use App\Models\BarangayEquipment;
use App\Models\User;
use App\Models\BarangayReport;


class AdminRequestBrgy extends Controller
{
    public function index(){
    	return view('AdminRequestBrgy.index');
    }

     public function store(Request $request){
    	$validator = Validator::make($request->all(), [
            'equipment_name' => 'required',
            'quantity' => 'required',
            
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	if (!empty($request->get('id'))) {
        	$SportsEquipment = BarangayEquipment::where('id',$request->get('id'))->first();
        	$SportsEquipment->e_name = ucfirst(strtolower($request->get('equipment_name')));
        	$SportsEquipment->qnty = $request->get('quantity');
        	$SportsEquipment->save();
        	}else{
        	$SportsEquipment = new BarangayEquipment;
        	$SportsEquipment->e_name = ucfirst(strtolower($request->get('equipment_name')));
        	$SportsEquipment->qnty = $request->get('quantity');
        	$SportsEquipment->save();
        	}
        	return response()->json(['status' => true]);
        }
    }



    public function list(Request $request){
        if (!empty($request->get('filter1')) && !empty($request->get('filter2')) && empty($request->get('status_filter'))) {
        
        $BarangayEquipment = BarangayEquipmentRent::with('users')->with('BarangayEquipment')->whereBetween('date', [$request->get('filter1') , $request->get('filter2')] )->orderBy('date', 'desc')->orderBy('status', 'asc')->get();

        }elseif(!empty($request->get('filter1')) && empty($request->get('filter2')) && empty($request->get('status_filter'))){

          $BarangayEquipment = BarangayEquipmentRent::with('users')->with('BarangayEquipment')->where('date', '=', $request->get('filter1') )->orderBy('date', 'desc')->orderBy('status', 'asc')->get();

        }elseif (!empty($request->get('filter1')) && !empty($request->get('filter2')) && !empty($request->get('status_filter')) ) {

        $BarangayEquipment = BarangayEquipmentRent::with('users')->with('BarangayEquipment')->where('status', '=', $request->get('status_filter'))->whereBetween('date', [$request->get('filter1') , $request->get('filter2')] )->orderBy('date', 'desc')->orderBy('status', 'asc')->get();    

        }elseif (!empty($request->get('status_filter'))) {
          $BarangayEquipment = BarangayEquipmentRent::with('users')->with('BarangayEquipment')->where('date', today())->where('status', '=', $request->get('status_filter') )->orderBy('date', 'desc')->orderBy('status', 'asc')->get();
        }else{

        $BarangayEquipment = BarangayEquipmentRent::with('users')->with('BarangayEquipment')->where('date', today())->orderBy('date', 'desc')->orderBy('status', 'asc')->get();

        }
    	return response()->json(['status' => true, 'data' => $BarangayEquipment]);
    }

   
   public function approave(Request $request){
       $rentApproave = BarangayEquipmentRent::where('id', $request->get('rent_id'))->first();
       $minusStock = BarangayEquipment::where('id', $request->get('equipment_id'))->first();
       if ($minusStock->qnty <  $rentApproave->qnty) {
          return response()->json(['status' => false, 'message' => 'Not enough stock for this requested equipment, Check the current stock please!']);
       }else{
       $rentApproave->status = 1;
       $rentApproave->save();
       $minusThis = $minusStock->qnty - $rentApproave->qnty;
       $minusStock->qnty = $minusThis;
       $minusStock->save();


         $report = new BarangayReport;
         $report->barangay_equipment_id = $request->get('equipment_id');
         $report->report_name = "Stock out / Requested";
         $report->quantity =  $rentApproave->qnty;
         $report->reason = "Requested by user";
         $report->datetime = now();
         $report->date = now();
         $report->save();



       return response()->json(['status' => true]);
       }       
   }

   public function decline(Request $request){
       $rentApproave = BarangayEquipmentRent::where('id', $request->get('rent_id'))->first();
       
       $rentApproave->status = 2;
       $rentApproave->save();
       return response()->json(['status' => true]);
   }

   public function return(Request $request){
       $rentApproave = BarangayEquipmentRent::where('id', $request->get('rent_id'))->first();
       $minusStock = BarangayEquipment::where('id', $request->get('equipment_id'))->first();
       $rentApproave->status = 3;
       $rentApproave->save();
       $minusThis = $minusStock->qnty + $rentApproave->qnty;
       $minusStock->qnty = $minusThis;
       $minusStock->save();

         $report = new BarangayReport;
         $report->barangay_equipment_id = $request->get('equipment_id');
         $report->report_name = "Stock in / Return";
         $report->quantity =  $rentApproave->qnty;
         $report->reason = "Requested by user";
         $report->datetime = now();
         $report->date = now();
         $report->save();


       return response()->json(['status' => true]);
             
   }

   public function add(){
    $Barangay = BarangayEquipment::whereNull('deleted_at')->get();
    return view('AdminRequestBrgy.addRequestBrgy', compact('Barangay'));
   }

     public function qnty($id){
        $BarangayEquipment = BarangayEquipment::where('id', $id)->first();
        return response()->json(['status' => true, 'data' => $BarangayEquipment]);
    }

    public function storeRequest(Request $request){

        $validator = Validator::make($request->all(), [
            'barangay_equipments' => 'required',
            'request_quantity' => 'required',
            'date' => 'required',
            'reason' => 'required',
            'borrower' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{

            if ($request->get('request_quantity') > $request->get('current_quantity')) {
                return response()->json(['status' => false, 'message' => 'Not enough stock for this equipment!']);
            }else{
            $rent = new BarangayEquipmentRent;
            $rent->user_id = Auth::user()->id;
            $rent->barangay_equipment_id = $request->get('barangay_equipments');
            $rent->qnty = $request->get('request_quantity');
            $rent->date = $request->get('date');
            $rent->e_name = $request->get('e_name');
            $rent->reason = $request->get('reason');
            $rent->renter = $request->get('borrower');
            $rent->status = 1;
            $rent->save();

            $equipment = BarangayEquipment::where('id', $request->get('barangay_equipments'))->first();
            $total_qnty = $equipment->qnty - $request->get('request_quantity');
            $equipment->qnty = $total_qnty;
            $equipment->save();
            return response()->json(['status' => true, 'message' => 'Equipment requested successfully!']);
            }
            
        }
    }
}
