<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BarangayEquipment;
use App\Models\BarangayEquipmentRent;
use Auth;
use Validator;




class UserBrgyEquipmentController extends Controller
{
     public function index(){
    	$BarangayEquipment = BarangayEquipment::whereNull('deleted_at')->get();
    	return view('UserBrgyEquipment.index', compact('BarangayEquipment'));
    }

    public function qnty($id){
    	$BarangayEquipment = BarangayEquipment::where('id', $id)->first();
    	return response()->json(['status' => true, 'data' => $BarangayEquipment]);
    }

    public function store(Request $request){

    	$validator = Validator::make($request->all(), [
            'barangay_equipments' => 'required',
            'request_quantity' => 'required',
            'date' => 'required',
            'reason' => 'required',
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{

        	if ($request->get('request_quantity') > $request->get('current_quantity')) {
        		return response()->json(['status' => false, 'message' => 'Not enough stock for this equipment!']);
        	}else{

            $count = BarangayEquipmentRent::where('user_id', Auth::user()->id)->where('date', today())->count();
            if ($count > 19) {
               return response()->json(['status' => false, 'message' => 'Equipment Request Already Reach The Limit, Try Again Tomorrow!']);
            }else{
                $rent = new BarangayEquipmentRent;
                $rent->user_id = Auth::user()->id;
                $rent->barangay_equipment_id = $request->get('barangay_equipments');
                $rent->qnty = $request->get('request_quantity');
                $rent->date = $request->get('date');
                $rent->e_name = $request->get('e_name');
                $rent->reason = $request->get('reason');
                $rent->renter = Auth::user()->first_name.' '.Auth::user()->last_name;
                $rent->status = 4;
                $rent->save();
                return response()->json(['status' => true, 'message' => 'Equipment requested successfully!']);
            }
			
        	}
        	
        }
    }
    public function requestView(){
    	$request = BarangayEquipmentRent::where('user_id', Auth::user()->id)->orderBy('date', 'desc')->get();
    	return view('UserBrgyEquipment.viewRequest', compact('request'));

    }
}
