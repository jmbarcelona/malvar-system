<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BarangayIndigency;
use Validator;
use Auth;

class UserBrgyIndigencyController extends Controller
{
    public function index(){
    	return view('UserBrgyIndigency.index');
    }

    public function store(Request $request){
    	$validator = Validator::make($request->all(), [
            'ctc_no' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'age' => 'required',
            'civil_status' => 'required',
            'address' => 'required',
            'purpose' => 'required',
        ]);
 		if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{

            $count = BarangayIndigency::where('date', today())->where('user_id', Auth::user()->id)->count();
            if ($count > 19) {
                    
                    return response()->json(['status' => false, 'message' => 'Barangay Indigency Request Already Reach The Limit, Try Again Tomorrow!']);

            }else{

                $indigency = new BarangayIndigency;
                $indigency->ctc_no = $request->get('ctc_no');
                $indigency->first_name = ucfirst($request->get('first_name'));
                $indigency->middle_name = ucfirst($request->get('middle_name'));
                $indigency->last_name = ucfirst($request->get('last_name'));
                $indigency->age = $request->get('age');
                $indigency->civil_status = $request->get('civil_status');
                $indigency->address = $request->get('address');
                $indigency->purpose = $request->get('purpose');
                $indigency->date = now();
                $indigency->user_id = Auth::user()->id;
                $indigency->save();
                return response()->json(['status' => true]);
            }
        }
    }

    public function request(){
    	$indigency = BarangayIndigency::where('user_id', Auth::user()->id)->get();
    	return view('UserBrgyIndigency.request', compact('indigency'));
    }
    
    public function find($id){ 
		$data = BarangayIndigency::where('id', $id)->first();
		return response()->json(['status' => true, 'data' => $data]);
	}
}
