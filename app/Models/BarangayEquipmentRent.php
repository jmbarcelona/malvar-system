<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BarangayEquipmentRent extends Model
{
    use HasFactory;
    protected $table = 'barangay_equipment_rent';
    protected $fillable =
    [
    	'user_id',
    	'barangay_equipment_id',
    	'date',
        'qnty',
    	'reason',
        'e_name',
    	'status',
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id' , 'id');
    }

    public function BarangayEquipment()
    {
        return $this->belongsTo(BarangayEquipment::class, 'barangay_equipment_id' , 'id');
    }
}
