<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gym extends Model
{
    use HasFactory;
    protected $table = 'gym';
    protected $fillable= 
    [
    	'user_id',
    	'start_time',
    	'end_time',
    	'date',
        'status',
        'reason',
        'amount',
    	'renter',
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id' , 'id');
    }
}
