<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BarangayReport extends Model
{
    use HasFactory;

    protected $table = 'barangay_reports';

    protected $fillable = [
    	'barangay_equipment_id',
    	'datetime',
    	'report_name',
    	'quantity',
    ];

    // protected $appends = ['date_toformat'];

    // function getDateToformatAttribute(){
    // 	return date('F j, Y, g:i a', strtotime($this->datetime));
    // }

    function getDatetimeAttribute($value){
    	return date('F j, Y', strtotime($value));
    }

    public function BarangayEquipment()
    {
        return $this->belongsTo(BarangayEquipment::class, 'sport_equipment_id' , 'id');
    }

}
