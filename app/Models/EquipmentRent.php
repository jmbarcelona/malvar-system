<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EquipmentRent extends Model
{
    use HasFactory;
    protected $table = 'equipment_rent';
    protected $fillable = 
    [
        'user_id',
    	'equipment_id',
    	'e_name',
    	'qnty',
    	'date',
    	'description',
    ];
}
