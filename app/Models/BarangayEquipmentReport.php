<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BarangayEquipmentReport extends Model
{
    use HasFactory;

      protected $table = 'barangay_sport_reports';

    protected $fillable = [
    	'barangay_equipment_id',
    	'datetime',
    	'report_name',
    	'quantity',
    ];
}
