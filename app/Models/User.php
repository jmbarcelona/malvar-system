<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $table = 'users';
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'type',
        'address',
        'contact',
        'gender',
        'email_address',
        'password',
        'bday',
        'user_img'
    ];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }

    public function SportEquipmentRent()
    {
        return $this->hasMany(SportEquipmentRent::class, 'user_id');
    }

    public function BarangayEquipmentRent()
    {
        return $this->hasMany(BarangayEquipmentRent::class, 'user_id');
    }

    public function BarangayIndigency()
    {
        return $this->hasMany(BarangayIndigency::class, 'user_id');
    }

     public function BarangayClearance()
    {
        return $this->hasMany(BarangayClearance::class, 'user_id');
    }

     public function Gym()
    {
        return $this->hasMany(Gym::class, 'user_id');
    }
}
