<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cedula extends Model
{
    use HasFactory;

    protected $table = 'cedula';

    protected $fillables = 
    [
    	'user_id',
    	'last_name',
    	'first_name',
    	'middle_name',
    	'suffix_name',
    	'gender',
    	'address',
    	'citizenship',
    	'bday',
    	'bplace',
    	'civil_status',
    	'height',
    	'weight',

    ];
}
