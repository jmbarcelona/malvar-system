<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SportReport extends Model
{
    use HasFactory;

    protected $table = 'sport_reports';

    protected $fillable = [
    	'sport_equipment_id',
    	'datetime',
    	'report_name',
    	'quantity',
    ];

    // protected $appends = ['date_toformat'];

    // function getDateToformatAttribute(){
    // 	return date('F j, Y, g:i a', strtotime($this->datetime));
    // }

    function getDatetimeAttribute($value){
    	return date('F j, Y', strtotime($value));
    }

    public function SportEquipment()
    {
        return $this->belongsTo(SportEquipment::class, 'sport_equipment_id' , 'id');
    }

}
