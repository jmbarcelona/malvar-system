<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class SportEquipmentRent extends Model
{
    use HasFactory;
    protected $table = 'sport_equipment_rent';
    protected $fillable =
    [
    	'user_id',
    	'sport_equipment_id',
    	'date',
        'qnty',
    	'reason',
        'e_name',
    	'status',
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id' , 'id');
    }

    public function SportEquipment()
    {
        return $this->belongsTo(SportEquipment::class, 'sport_equipment_id' , 'id');
    }




}
