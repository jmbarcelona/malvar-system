<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BarangayClearance extends Model
{
    use HasFactory;

    protected $table = 'barangay_clearance';
    protected $fillable = [
    	'user_id',
    	'ctc_no',
    	'first_name',
    	'middle_name',
    	'last_name',
    	'resident_from',
    	'address',
    	'civil_status',
    	'purpose',
        'status',
    	'date',
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id' , 'id');
    }

    // function getDateAttribute($value){
    //     return date('F j, Y', strtotime($value));
    // }

    // function getResidentfromAttribute($value){
    //     return date('F j, Y', strtotime($value));
    // }
    // function getResident_fromAttribute($value){
    //     return date('F j, Y', strtotime($value));
    // }
}
