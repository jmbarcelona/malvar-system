<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BarangayIndigency extends Model
{
    use HasFactory;


    protected $table = 'barangay_indigency';
    protected $fillable = [
    	'user_id',
    	'ctc_no',
    	'first_name',
    	'middle_name',
    	'last_name',
    	'age',
    	'address',
    	'civil_status',
    	'purpose',
        'status',
    	'date',
    ];

     public function users()
    {
        return $this->belongsTo(User::class, 'user_id' , 'id');
    }

    // function getDateAttribute($value){
    //     return date('F j, Y', strtotime($value));
    // }
}
