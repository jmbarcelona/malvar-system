<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BarangayEquipment extends Model
{
    use HasFactory;
    protected $table = 'barangay_equipment';
    protected $fillable =
    [
    	'e_name',
        'qnty',
    	'description',
    ];

     public function BarangayEquipmentRent()
    {
        return $this->hasMany(BarangayEquipmentRent::class, 'barangay_equipment_id');
    }

    public function BarangayReport()
    {
        return $this->hasMany(BarangayReport::class, 'barangay_equipment_id');
    }
}
