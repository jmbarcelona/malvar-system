<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SportEquipment extends Model
{
    use HasFactory;
    protected $table = 'sport_equipment';
    protected $fillable =
    [
    	'e_name',
    	'qnty',
        'description',
    ];

     public function SportEquipmentRent()
    {
        return $this->hasMany(SportEquipmentRent::class, 'sport_equipment_id');
    }

    public function SportReport()
    {
        return $this->hasMany(SportReport::class, 'sport_equipment_id');
    }
}
