<?php 

use App\Models\User;
use App\Models\System;
use App\Models\Gym;
use App\Models\Announcement;
use App\Models\BarangayEquipmentRent;
use App\Models\BarangayClearance;
use App\Models\BarangayIndigency;

use App\Models\SportEquipmentRent;
use Illuminate\Support\Facades\Auth;

function getSystemDetails(){
	$system = System::first();
	 return $system;
}

function totalUser(){
	return User::where('type', 3)->count();
}

function totalAnnouncement(){
	return Announcement::count();
}

function BarangayEquipmentRent(){
	$today = date("Y-m-d");
	return BarangayEquipmentRent::where('date', $today)->count();
}

function SportEquipmentRent(){
	$today = date("Y-m-d");
	return SportEquipmentRent::where('date', $today)->count();
}

function Gymcount(){
	$today = date("Y-m-d");
	return Gym::where('date', $today)->count();
}

function UserGymcount(){
	$today = date("Y-m-d");
	return Gym::where('user_id', Auth::user()->id)->where('date', $today)->count();
}

function UserBrgyRent(){
	$today = date("Y-m-d");
	return BarangayEquipmentRent::where('user_id', Auth::user()->id)->where('date', $today)->count();
}

function UserSportRent(){
	$today = date("Y-m-d");
	return SportEquipmentRent::where('user_id', Auth::user()->id)->where('date', $today)->count();
}

function UserBarangayIndigency(){
	$today = date("Y-m-d");
	return BarangayIndigency::where('user_id', Auth::user()->id)->where('date', $today)->count();
}

function UserBarangayClearance(){
	$today = date("Y-m-d");
	return BarangayClearance::where('user_id', Auth::user()->id)->where('date', $today)->count();
}

function BarangayIndigency(){
	$today = date("Y-m-d");
	return BarangayIndigency::where('date', $today)->count();
}

function BarangayClearance(){
	$today = date("Y-m-d");
	return BarangayClearance::where('date', $today)->count();
}



